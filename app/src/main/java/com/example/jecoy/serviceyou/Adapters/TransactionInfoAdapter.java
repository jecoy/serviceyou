package com.example.jecoy.serviceyou.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.jecoy.serviceyou.Class.TransactionInfo;
import com.example.jecoy.serviceyou.R;

import java.util.List;

import static com.example.jecoy.serviceyou.R.drawable.popupcancelled;

public class TransactionInfoAdapter extends RecyclerView.Adapter<TransactionInfoAdapter.RecyclerViewAdapter> {
    List<TransactionInfo> transactionInfos;
    Context context;
    transactionInfoClickListener transactionInfoClickListener;

    public TransactionInfoAdapter(List<TransactionInfo> transactionInfos, Context context, TransactionInfoAdapter.transactionInfoClickListener transactionInfoClickListener) {
        this.transactionInfos = transactionInfos;
        this.context = context;
        this.transactionInfoClickListener = transactionInfoClickListener;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listoftransactions,viewGroup,false);
        return new RecyclerViewAdapter(view,transactionInfoClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter recyclerViewAdapter, int i) {
        TransactionInfo transactionInfo = transactionInfos.get(i);
        recyclerViewAdapter.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation));
        recyclerViewAdapter.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));
        recyclerViewAdapter.transactionID.setText(transactionInfo.getTi_id()+"");
        recyclerViewAdapter.transactionDate.setText(transactionInfo.getTask_date());
        recyclerViewAdapter.transactionSubservice.setText(transactionInfo.getSubservice());

        if(transactionInfo.getTask_status().equals("Cancelled")){
            recyclerViewAdapter.container.setBackgroundResource(R.drawable.popupcancelled);
        }
        else if(transactionInfo.getTask_status().equals("Completed")){
            recyclerViewAdapter.container.setBackgroundResource(R.drawable.popupcompleted);
        }
        else if(transactionInfo.getTask_status().equals("Rescheduled")){
            recyclerViewAdapter.container.setBackgroundResource(R.drawable.popuprescheduled);
        }
        else{
            recyclerViewAdapter.container.setBackgroundResource(R.drawable.popupopen);
        }
    }
    @Override
    public int getItemCount() {
        return transactionInfos.size();
    }

    public class RecyclerViewAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView transactionID,transactionSubservice,transactionDate;
        transactionInfoClickListener transactionInfoClickListener;
        RelativeLayout container;

        public RecyclerViewAdapter(@NonNull View itemView,transactionInfoClickListener transactionInfoClickListener) {
            super(itemView);
            transactionID = itemView.findViewById(R.id.transactionID);
            transactionSubservice = itemView.findViewById(R.id.transactionSubservice);
            transactionDate = itemView.findViewById(R.id.transactionDate);
            container = itemView.findViewById(R.id.container);
            this.transactionInfoClickListener = transactionInfoClickListener;
            container.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            transactionInfoClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public interface transactionInfoClickListener{
        void onItemClick(View view,int position);
    }
}
