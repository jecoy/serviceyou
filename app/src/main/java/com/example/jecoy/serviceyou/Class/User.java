package com.example.jecoy.serviceyou.Class;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("user_fname")
    private String user_fname;
    @SerializedName("user_lname")
    private String user_lname;
    @SerializedName("user_add")
    private String user_add;
    @SerializedName("user_number")
    private String user_number;
    @SerializedName("user_email")
    private String user_email;
    @SerializedName("user_pass")
    private String user_pass;
    @SerializedName("user_pic")
    private String user_pic;
    @SerializedName("user_idPic")
    private String user_idPic;
    @SerializedName("user_badge")
    private String user_badge;
    @SerializedName("user_points")
    private float user_points;
    @SerializedName("user_role")
    private String user_role;
    @SerializedName("user_status")
    private String user_status;
    @SerializedName("user_isActive")
    private Boolean user_isActive;
    @SerializedName("verification_code")
    private String verification_code;
    @SerializedName("vc_status")
    private String 	vc_status;


    @SerializedName("value")
    private String value;
    @SerializedName("message")
    private String massage;
    @SerializedName("service_quickpitch")
    private String service_quickpitch;
    @SerializedName("rating")
    private float rating;

    @SerializedName("user_description")
    private String user_description;
    @SerializedName("user_workDescription")
    private String user_workDescription;
    @SerializedName("hms_location")
    private String hms_location;
    @SerializedName("hms_radius")
    private String hms_radius;
    @SerializedName("hm_efficiency")
    private double hm_efficiency;
    @SerializedName("hms_latitude")
    private double hms_latitude;
    @SerializedName("hms_longitude")
    private double hms_longitude;

    public int getUser_id() {
        return user_id;
    }

    public String getUser_fname() {
        return user_fname;
    }

    public String getUser_lname() {
        return user_lname;
    }

    public String getUser_add() {
        return user_add;
    }

    public String getUser_number() {
        return user_number;
    }

    public String getUser_email() {
        return user_email;
    }

    public String getUser_pass() {
        return user_pass;
    }

    public String getUser_pic() {
        return user_pic;
    }

    public String getUser_idPic() {
        return user_idPic;
    }

    public String getUser_badge() {
        return user_badge;
    }

    public float getUser_points() {
        return user_points;
    }

    public String getUser_role() {
        return user_role;
    }

    public String getUser_status() {
        return user_status;
    }

    public Boolean getUser_isActive() {
        return user_isActive;
    }

    public String getVerification_code() {
        return verification_code;
    }

    public String getVc_status() {
        return vc_status;
    }

    public String getValue() {
        return value;
    }

    public String getMassage() {
        return massage;
    }

    public String getService_quickpitch() {
        return service_quickpitch;
    }

    public float getRating() {
        return rating;
    }

    public String getUser_description() {
        return user_description;
    }

    public String getUser_workDescription() {
        return user_workDescription;
    }

    public String getHms_location() {
        return hms_location;
    }

    public String getHms_radius() {
        return hms_radius;
    }

    public double getHm_efficiency() {
        return hm_efficiency;
    }

    public double getHms_latitude() {
        return hms_latitude;
    }

    public double getHms_longitude() {
        return hms_longitude;
    }
}
