package com.example.jecoy.serviceyou.Class;

public class LoginUser {
    private int user_id;
    private String user_email, user_fname, user_lname,user_role,user_status,user_pic,user_address,user_number,user_description,user_workDescription,hm_efficieny,user_badge,user_idPic;

    public LoginUser(int user_id, String user_email, String user_fname, String user_lname, String user_role, String user_status, String user_pic, String user_address, String user_number, String user_description, String user_workDescription, String hm_efficieny, String user_badge, String user_idPic) {
        this.user_id = user_id;
        this.user_email = user_email;
        this.user_fname = user_fname;
        this.user_lname = user_lname;
        this.user_role = user_role;
        this.user_status = user_status;
        this.user_pic = user_pic;
        this.user_address = user_address;
        this.user_number = user_number;
        this.user_description = user_description;
        this.user_workDescription = user_workDescription;
        this.hm_efficieny = hm_efficieny;
        this.user_badge = user_badge;
        this.user_idPic = user_idPic;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getUser_email() {
        return user_email;
    }

    public String getUser_fname() {
        return user_fname;
    }

    public String getUser_lname() {
        return user_lname;
    }

    public String getUser_role() {
        return user_role;
    }

    public String getUser_status() {
        return user_status;
    }

    public String getUser_pic() {
        return user_pic;
    }

    public String getUser_address() {
        return user_address;
    }

    public String getUser_number() {
        return user_number;
    }

    public String getUser_description() {
        return user_description;
    }

    public String getUser_workDescription() {
        return user_workDescription;
    }

    public String getHm_efficieny() {
        return hm_efficieny;
    }

    public String getUser_badge() {
        return user_badge;
    }

    public String getUser_idPic() {
        return user_idPic;
    }
}
