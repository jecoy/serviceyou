package com.example.jecoy.serviceyou.Fragments;

import android.Manifest;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.jecoy.serviceyou.Activities.HomeActivity;
import com.example.jecoy.serviceyou.Adapters.HandymanDocumentsAdapter;
import com.example.jecoy.serviceyou.Adapters.MessagesAdapter;
import com.example.jecoy.serviceyou.Adapters.PromoAdapter;
import com.example.jecoy.serviceyou.Adapters.ReasonsAdapter;
import com.example.jecoy.serviceyou.Adapters.ServiceAdapter;
import com.example.jecoy.serviceyou.Adapters.SubServiceAdapter;
import com.example.jecoy.serviceyou.AlarmReceiver.AlarmReceiver;
import com.example.jecoy.serviceyou.Api.ApiInterface;
import com.example.jecoy.serviceyou.Api.RetrofitClient;
import com.example.jecoy.serviceyou.Class.Conversations;
import com.example.jecoy.serviceyou.Class.HandymanDocs;
import com.example.jecoy.serviceyou.Class.LoginUser;
import com.example.jecoy.serviceyou.Class.Messages;
import com.example.jecoy.serviceyou.Class.Promos;
import com.example.jecoy.serviceyou.Class.RateAndReview;
import com.example.jecoy.serviceyou.Class.Reasons;
import com.example.jecoy.serviceyou.Class.Services;
import com.example.jecoy.serviceyou.Class.SubServices;
import com.example.jecoy.serviceyou.Class.TransactionInfo;
import com.example.jecoy.serviceyou.Class.User;
import com.example.jecoy.serviceyou.GoogleDirection.DirectionJSONParser;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Storage.SharedPrefManager;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import afu.org.checkerframework.checker.igj.qual.I;
import de.hdodenhof.circleimageview.CircleImageView;
import pl.bclogic.pulsator4droid.library.PulsatorLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import org.json.JSONObject;
import org.w3c.dom.Text;

import static android.content.Context.ALARM_SERVICE;

public class MapFragment extends Fragment implements OnMapReadyCallback{
    /*AutocompleteSupportFragment autocompleteFragment;
    List<Place.Field> placeFields = Arrays.asList(Place.Field.ID,
            Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
    EditText searchPlaceEditText;*/
    int warranty_id;

    MessagesAdapter messagesAdapter;
    MessagesAdapter.messageClickListener messageClickListener;
    List<Messages> messagesList;
    CardView send;
    EditText message;
    RecyclerView recyclerViewMessages;
    Dialog C_M_P;

    View v;
    GoogleMap mMap;
    String apikey = "AIzaSyCIg3G0tM1xxwxlJd4adGDIRk0PEiB-Zss";
    String currentDateTime,displayedCurrentTime,taskLocation,taskInstruction,_subservice,taskPromo,user_roleFromActivity;
    Switch availabilitySwitch;
    LoginUser user;
    ApiInterface apiInterface;
    MarkerOptions origin,destination;
    ArrayList markerPoints= new ArrayList();
    private Polyline currentPolyline;

    private List<Services> servicesList;
    private List<SubServices> subServicesList;
    private List<Promos> promosList;
    private List<Reasons> reasonsList;
    ReasonsAdapter.reasonsClickListener reasonsClickListener;
    ReasonsAdapter reasonsAdapter;
    PromoAdapter.promosClickListener promosClickListener;
    PromoAdapter promoAdapter;
    ServiceAdapter.serviceClickListener serviceClickListener;
    ServiceAdapter serviceAdapter;
    SubServiceAdapter.subServiceClickListener subServiceClickListener;
    SubServiceAdapter subServiceAdapter;
    private RecyclerView recyclerViewforSubservices,recyclerViewforServices,recyclerViewforReasons,recyclerViewforPromos;

    private FusedLocationProviderClient fusedLocationProviderClient;
    private static final float DEFAULT_ZOOM = 14f;
    FloatingActionButton myLocBtn,messageBtn;

    Dialog B_O_D, F_I_P, W_F_H, H_P_I,L_I_P,W_F_T,T_I_D,L_O_R,C_C_P,C_R_D,C_R_F_H,C_H_A,C_S_C,C_S_P,R_R_P;
    int sub_id, serv_id, user_idFromActivity,ti_id,promo_id,reason_id,counter = 21,i = 11,rt_id,transaction_id,conversation_id;
    double lati, longi,sub_rate,_latitude,_longitude,valueResult,km,meter;
    CardView cardViewBookNow, cardViewBookLater, cardViewSendInfo, cardViewCancel,cardViewCancelTransaction,cardViewYes,cardViewNo,cardViewReschedule;
    TextView handymanDocs,closeHandymanPopUp,promoCode,closeTxtView, transactionDate, transactionLocation,handyman_name,handyman_add,handyman_quickpitch,handyman_bio,handyman_workinfo,rescheduledDateTxtView;
    RatingBar handyman_rating;
    EditText transactionInstruction;
    CircleImageView handyman_profile;
    ImageView sylogo;
    PulsatorLayout pulsator;
    ProgressDialog progressDialog,progressDialog2;
    private Handler mHandler = new Handler();
    long alarmStartTime;
    String rescheduledDate,rt_proposed_date,original_date,convo_partner_email;
    DatePickerDialog datePickerDialog;

    //FOR RETRIEVED TRANSACTION INFO.
    int _ti_id,_service_id,_subservice_id,_customer_id,_handyman_id,_conversation_id;
    String _task_date,_task_location,_task_instruction,_task_status,_subserviceFromTI,_task_type;

    //FOR RETRIEVED PROFILE INFO.
    String _handymanName,_handymanAddress,_handymanQuickPitch,_handymanBio,_handymanWorkInfo,_handymanPic,_handymanEmail,_handymanNumber;
    float _handymanRate;

    //FOR RETRIEVED TRANSACTION INFO DIALOG
    TextView _transactionID,_transactionDate,_transactionService,_transactionLocation,_transactionInstruction,counterTextView,close;
    CardView _cardViewAccept;

    //FOR THE HANDYMAN TOOLS AND DISTANCE INFORMATION OF CUSTOMER
    RelativeLayout distanceInKmRelativeLayout,hmTools,handyman_info_layout_parent,handyman_info_layout;
    TextView textViewArrived,textViewDistance,distanceLabel;

    boolean isStopped=true,isAcquired,handymanProfileLayoutVisibility,hasArrived=false,isBanned = false,isResumed=false,profileInfoisOpen=false;
    RelativeLayout changeStatus;
    String customer_name,customer_location;
    Marker currentLocationMarker,customerLocationMarker;
    double kmInDec;

    //FOR HANDYMAN INFO IN USER
    CardView cardViewCancelHandymanAcquired,cardViewMessageHandymanAcquired;
    TextView handyman_acquired_name,handyman_acquired_email,handyman_acquired_number,handyman_acquired_address;
    CircleImageView handyman_acquired_picture;

    //FOR CONFIRMATION OF RESCHEDULE TRANSACTION FOR HANDYMAN
    CardView cardViewConfirmResched,cardViewCancelResched;
    TextView originalDateTxtView,proposedDateTxtView;

    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    String actualDate;
    RecyclerView recyclerViewHandymanDocs;
    private List<HandymanDocs> hmDocsList;
    HandymanDocumentsAdapter handymanDocumentsAdapter;
    HandymanDocumentsAdapter.hmDocsListener hmDocsListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        user = SharedPrefManager.getInstance(this.getActivity().getApplicationContext()).getUser();
        user_idFromActivity = user.getUser_id();
        user_roleFromActivity = user.getUser_role();
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        if(this.getArguments()!=null){
            warranty_id = this.getArguments().getInt("warranty_id");
        }


        if (user_roleFromActivity.equals("Handyman")) {
            v = inflater.inflate(R.layout.fragment_map_handyman, container, false);
        }
        else {
            v = inflater.inflate(R.layout.fragment_map, container, false);
            getServices();
            serviceClickListener = ((view, position) -> {
                int id = servicesList.get(position).getService_id();
                getSubService("selecSub", id);
            });
            subServiceClickListener = new SubServiceAdapter.subServiceClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    sub_id = subServicesList.get(position).getSubservice_id();
                    serv_id = subServicesList.get(position).getService_id();
                    _subservice = subServicesList.get(position).getSubservice();
                    sub_rate = subServicesList.get(position).getSubservice_rate();
                    _task_type = "OnTheSpot";
                    showFinalInstruction();
                    //checkHandymanService("checkHmService",sub_id,user_idFromActivity,serv_id);
                }
            };
        }

        return v;
    }

    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                               float velocityY) {
            try {
                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH){
                    return false;
                }
                // right to left swipe
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
                        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    if(profileInfoisOpen){
                        Toast.makeText(getActivity().getApplicationContext(),"LEFT",Toast.LENGTH_LONG).show();
                    }
                }
                // left to right swipe
                else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
                        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    if(profileInfoisOpen){
                        Toast.makeText(getActivity().getApplicationContext(),"RIGHT",Toast.LENGTH_LONG).show();
                    }
                }
            } catch (Exception e) {

            }
            return false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        checkTransaction(user_idFromActivity,user_roleFromActivity);
    }

    @Override
    public void onStop() {
        super.onStop();
        if(isResumed){
            autoUpdateDistanceStop("");
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        updateBanned("checkHandyman",user_idFromActivity);
    }

    private void checkTransaction(int user_id, String user_role) {
        Call<TransactionInfo> call = apiInterface.checkTransaction("checkTransaction",user_id,user_role);
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                if(response.body()!=null){
                    String value = response.body().getValue();
                    if(value.equals("1")){
                        String status = response.body().getTask_status();
                        if(user_role.equals("Handyman")){
                            if(!status.equals("Completed")&&!status.equals("Cancelled")&&!status.equals("Rescheduled")&&!status.equals("Reserved")&&!status.equals("Pending")){
                                isResumed = true;
                                if(status.equals("Service in Progress")){
                                    hasArrived=true;
                                }
                                else{
                                    hasArrived=false;
                                }

                                availabilitySwitch.setVisibility(View.INVISIBLE);
                                _ti_id = response.body().getTi_id();
                                _service_id = response.body().getService_id();
                                _subservice_id  = response.body().getSubservice_id();
                                _customer_id = response.body().getCustomer_id();
                                _subserviceFromTI = response.body().getSubservice();
                                _task_date = response.body().getTask_date();
                                _task_location = response.body().getTask_location();
                                _latitude = response.body().getLatitude();
                                _longitude = response.body().getLongitude();
                                _task_instruction = response.body().getTask_instruction();
                                _task_status = response.body().getTask_status();
                                autoUpdateDistanceStart();
                                customer_name = response.body().getUser_fname()+" "+response.body().getUser_lname();
                                convo_partner_email = response.body().getUser_email();
                                conversation_id = response.body().getConversation_id();

                                moveCamera2(new LatLng(_latitude,_longitude),DEFAULT_ZOOM,"Customer's name: "+customer_name,"Location: "+_task_location);
                            }
                            else{

                            }
                        }
                        else{
                            if(!status.equals("Completed")&&!status.equals("Cancelled")&&!status.equals("Reserved")&&!status.equals("Pending")){
                                isResumed = true;
                                _ti_id = response.body().getTi_id();
                                ti_id = _ti_id;
                                _service_id = response.body().getService_id();
                                _subservice_id  = response.body().getSubservice_id();
                                _handyman_id = response.body().getHandyman_id();
                                conversation_id = response.body().getConversation_id();
                                fetchHandymanProfile("fetchHandymanProfle",_handyman_id);

                            }
                        }
                    }
                    else{
                        return;
                    }
                }
                else{
                    return;
                }
            }

            @Override
            public void onFailure(Call<TransactionInfo> call, Throwable t) {
                getDeviceLocation();
            }
        });
    }

    //ON VIEW CREATED
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        progressDialog = new ProgressDialog(this.getActivity());
        progressDialog2 = new ProgressDialog(this.getActivity());
        isAcquired = false;
        handymanProfileLayoutVisibility = false;
        recyclerViewforSubservices = v.findViewById(R.id.recyclerViewSubService);
        recyclerViewforServices= v.findViewById(R.id.recyclerViewServices);

        Places.initialize(this.getActivity().getApplicationContext(), apikey);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment == null) {
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            mapFragment = SupportMapFragment.newInstance();
            ft.replace(R.id.map, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);

        myLocBtn = v.findViewById(R.id.myLocBtn);
        myLocBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDeviceLocation();
            }
        });

        L_O_R = new Dialog(getActivity());
        C_C_P = new Dialog(getActivity());
        C_S_P = new Dialog(getActivity());
        R_R_P = new Dialog(getActivity());
        C_M_P = new Dialog(getActivity());
        if(user_roleFromActivity.equals("Customer")){
            //DIALOG THAT ARE FOR USERS//
            //BOOKING OPTIONS DIALOG
            B_O_D = new Dialog(getActivity());
            //FINAL INSTRUCTION DIALOG
            F_I_P = new Dialog(getActivity());
            //WAITING FOR HANDYMAN DIALOG
            W_F_H = new Dialog(getActivity());
            //HANDYMAN PROFILE INFORMATION DIALOG
            H_P_I = new Dialog(getActivity());
            //LIST OF PROMOS DIALOG
            L_I_P = new Dialog(getActivity());
            //CONFIRM RESCHEDULE DATE FOR USER
            C_R_D = new Dialog(getActivity());
            //CONFIRM HANDYMAN ARRIVAL FOR USER
            C_H_A = new Dialog(getActivity());
            //CONFIRM SERVICE COMPLETION FOR USER
            C_S_C = new Dialog(getActivity());
        }
        else{
            //WAITING FOR TRANSACTION DIALOG
            W_F_T = new Dialog(getActivity());
            //TRANSACTION INSTRUCTION DIALOG
            T_I_D = new Dialog(getActivity());
            //CONFIRM RESCHEDULE FOR HANDYMAN DIALOG
            C_R_F_H = new Dialog(getActivity());
            availabilitySwitch = v.findViewById(R.id.availabilitySwitch);
            changeStatus = v.findViewById(R.id.changeStatus);

            availabilitySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked==true){
                        checkHandymanPoints("checkHandymanPointsForAvailability");
                    }
                    else {
                        return;
                    }
                }
            });
        }
    }
    //CHECK IF THE HANDYMAN HAS SUFFICIENT POINTS TO ACCEPT A SERVICE
    private void checkHandymanPoints(String key) {
        Call<User> call = apiInterface.checkHandymanPoints(key,user_idFromActivity);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.body()!=null){
                    String value = response.body().getValue();
                    if(value.equals("1")){
                        showWaitingDialogForHM();
                    }
                    else if(value.equals("2")){
                        Toast.makeText(getActivity().getApplicationContext(),"Insufficient points, please load atleast 100 points to receive service.",Toast.LENGTH_LONG).show();
                        availabilitySwitch.setChecked(false);
                    }
                }
                else{
                    Toast.makeText(getActivity().getApplicationContext(),"ERROR RESPONSE BODY IS NULL.",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });

    }
    //WAITING DIALOG FOR FETCHING AVAILABLE SERVICES
    private void showWaitingDialogForHM() {
        W_F_T.setContentView(R.layout.pendingservicepopup);
        W_F_T.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        sylogo = W_F_T.findViewById(R.id.sylogo);
        pulsator = W_F_T.findViewById(R.id.pulsator);
        cardViewCancelTransaction = W_F_T.findViewById(R.id.cardViewCancelTransaction);
        W_F_T.show();
        pulsator.start();
        W_F_T.setCancelable(false);
        W_F_T.setCanceledOnTouchOutside(false);
        waitingForTransactionStart();

        cardViewCancelTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStopped = true;
                W_F_T.dismiss();
                pulsator.stop();
                availabilitySwitch.setChecked(false);
                mHandler.removeCallbacks(transactionWaitingRunnable);
            }
        });
    }

    //START OF THE HANDLER THAT FETCHES AVAILABLE TRANSACTION
    private void waitingForTransactionStart() {
        isStopped = false;
        transactionWaitingRunnable.run();
    }
    //START OF THE LOOP THAT FETCHES AVAILABLE TRANSACTION
    public void waitingForTransactionStop(){
        W_F_T.dismiss();
        pulsator.stop();
        mHandler.removeCallbacks(transactionWaitingRunnable);
        showTransactionInfo();
    }
    //STOP THE LOOP AND HANDLER FOR IT HAS FETCHED A TRANSACTION
    public void waitingForTransactionStartLoop(){
        mHandler.removeCallbacks(transactionWaitingRunnable);
        transactionWaitingRunnable.run();
    }
    //RUNNABLE FOR FETCHING A TRANSACTION FOR HANDYMAN
    private Runnable transactionWaitingRunnable = new Runnable() {
        @Override
        public void run() {
            Call<TransactionInfo> call = apiInterface.checkAvaialableTransaction("checkAvaialableTransaction",user_idFromActivity);
            call.enqueue(new Callback<TransactionInfo>() {
                @Override
                public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                    if(response.body()==null){
                        waitingForTransactionStartLoop();
                    }
                    else{
                        String value = response.body().getValue();
                        if(value.equals("0"))
                        {
                            waitingForTransactionStartLoop();
                        }
                        else
                        {
                            if(!isStopped){
                                _ti_id = response.body().getTi_id();
                                _service_id = response.body().getService_id();
                                _subservice_id  = response.body().getSubservice_id();
                                _customer_id = response.body().getCustomer_id();
                                _subserviceFromTI = response.body().getSubservice();
                                _task_date = response.body().getTask_date();
                                _task_location = response.body().getTask_location();
                                _latitude = response.body().getLatitude();
                                _longitude = response.body().getLongitude();
                                _task_instruction = response.body().getTask_instruction();
                                _task_status = response.body().getTask_status();
                                waitingForTransactionStop();
                            }
                            else{
                                return;
                            }
                        }
                    }

                }
                @Override
                public void onFailure(Call<TransactionInfo> call, Throwable t) {
                    Toast.makeText(getActivity(), "rp :"+
                                    t.getMessage().toString(),
                            Toast.LENGTH_SHORT).show();
                    waitingForTransactionStop();
                }
            });
            mHandler.postDelayed(this, 1000);
        }
    };

    //SHOW THE FETCH TRANSACTION INFORMATION
    private void showTransactionInfo() {
        T_I_D.setContentView(R.layout.serviceinfopopup);
        T_I_D.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        initializeTransactionInfoDialog(T_I_D);
        initializeTransactionInfoValues();
        T_I_D.show();
        T_I_D.setCancelable(false);
        T_I_D.setCanceledOnTouchOutside(false);
        trapTransactionInfoStart();

        _cardViewAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trapTransactionInfoLooperStop();
                isStopped = true;
                availabilitySwitch.setChecked(false);
                availabilitySwitch.setVisibility(View.INVISIBLE);
                counter=21;
                i = 11;
                acceptTransaction("handymanAcceptTransaction");
                autoUpdateDistanceStart();
            }
        });
    }
    //INITIALIZING VALUES OF FETCHED TRANSACTION TO THE DIALOG
    private void initializeTransactionInfoValues() {
        _transactionID.setText(_ti_id+"");
        _transactionDate.setText(_task_date);
        _transactionService.setText(_subserviceFromTI);
        _transactionLocation.setText(_task_location);
        _transactionInstruction.setText(_task_instruction);
    }
    //INITIALIZING DIALOG ELEMENTS
    private void initializeTransactionInfoDialog(Dialog t_i_d) {
        _transactionID = T_I_D.findViewById(R.id.transactionID);
        _transactionDate = T_I_D.findViewById(R.id.transactionDate);
        _transactionService = T_I_D.findViewById(R.id.transactionService);
        _transactionLocation = T_I_D.findViewById(R.id.transactionLocation);
        _transactionInstruction = T_I_D.findViewById(R.id.serviceInstruction);
        _cardViewAccept = T_I_D.findViewById(R.id.cardViewAccept);
        counterTextView = T_I_D.findViewById(R.id.counterTextView);
    }
    //ACCEPT THE TRANSACTION THAT IS FETCH
    private void acceptTransaction(String key) {
        int handyman_id = user_idFromActivity;
        Call<TransactionInfo> call = apiInterface.handymanAcceptTransaction(key,_ti_id,handyman_id,_customer_id);
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                String value = response.body().getValue();
                if(value.equals("1")){
                    if(response.body().getTask_type().equals("OnTheSpot")){
                        isAcquired = true;
                        customer_name = response.body().getUser_fname()+" "+response.body().getUser_lname();
                        convo_partner_email = response.body().getUser_email();
                        customer_location = response.body().getTask_location();
                        conversation_id = response.body().getConversation_id();
                        moveCamera2(new LatLng(_latitude,_longitude),DEFAULT_ZOOM,"Customer's name: "+customer_name,"Location: "+customer_location);
                    }
                    else{
                        progressDialog = ProgressDialog.show(getActivity(), "THANK YOU!", "THE SERVICE IS ALREADY RESERVED FOR YOU.", true);
                        progressDialog.show();
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();
                            }
                        }, 5000);
                    }
                }
                else{
                    Toast.makeText(getActivity().getApplicationContext(),"ERROR"+response.body().getMessage(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<TransactionInfo> call, Throwable t) {
                Toast.makeText(getActivity(), "ACCEPTANCE ERROR : rp - "+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }

    //TRANSACTION TRAPPINGS START
    private void trapTransactionInfoStart() {
        transactionInfoTrappings.run();
    }
    //TRANSACTION TRAPPINGS LOOPER
    private void trapTransactionInfoLooperStart() {
        counter--;
        if(counter>0){
            if(counter==20||counter==18||counter==16||counter==14||counter==12||counter==10||counter==8||counter==6||counter==4||counter==2)
            {
                i--;
                counterTextView.setText("You have "+i+" seconds to accept the service.");
                mHandler.removeCallbacks(transactionInfoTrappings);
                transactionInfoTrappings.run();
            }
        }
        else{
            isBanned = true;
            trapTransactionInfoLooperStop();
        }
    }
    //TRANSACTION LOOPER STOP
    public void trapTransactionInfoLooperStop(){
        T_I_D.dismiss();
        counter=21;
        i = 11;
        availabilitySwitch.setChecked(false);
        mHandler.removeCallbacks(transactionInfoTrappings);
        if(isBanned){
            showBanner();
        }
    }

    private void showBanner() {
        updateBanned("banHandyman",user_idFromActivity);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                updateBanned("unbanHandyman",user_idFromActivity);
            }
        }, 60000);

    }


    private void updateBanned(String key, int user_id) {
        Call<User> call = apiInterface.updateBanned(key,user_id);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.body()!=null){
                    String value = response.body().getValue();
                    if(value.equals("200:UserIsUnbanned")){
                        return;
                    }
                    else if(value.equals("404:UserIsBanned")){
                        showBanner();
                    }
                    else if(value.equals("200:Banned")){
                        T_I_D.setContentView(R.layout.handymanbannedpopup);
                        T_I_D.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        TextView counter = T_I_D.findViewById(R.id.counterTextView);
                        T_I_D.show();
                        T_I_D.setCancelable(false);
                        T_I_D.setCanceledOnTouchOutside(false);
                    }
                    else if(value.equals("200:Unbanned")){
                        T_I_D.dismiss();
                        isBanned = false;
                    }
                    else{
                        return;
                    }
                }
                else {
                    return;
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getActivity(), "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    //THE TRANSACTION LOOPER FOR FETCHING WETHER THE TRANSACTION IS ACCEPT BY OTHERS,CANCELLED BY THE USER,
    //AND COUNTER FOR THE HANDYMAN TO ACCEPT THE TRANSACTION
    private Runnable transactionInfoTrappings = new Runnable() {
        @Override
        public void run() {
            Call<TransactionInfo> call = apiInterface.checkTransactionAvailability("checkTransactionAvailability",_ti_id,user_idFromActivity);
            call.enqueue(new Callback<TransactionInfo>() {
                @Override
                public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                    if(response.body()!=null){
                        String value = response.body().getValue();
                        if(value.equals("1")){
                            trapTransactionInfoLooperStart();
                        }

                        else if(value.equals("2")){
                            T_I_D.dismiss();
                            counter=21;
                            i = 11;
                            mHandler.removeCallbacks(transactionInfoTrappings);
                            showWaitingDialogForHM();
                        }

                        else if(value.equals("3")){
                            T_I_D.dismiss();
                            counter=21;
                            i = 11;
                            mHandler.removeCallbacks(transactionInfoTrappings);
                            showWaitingDialogForHM();
                        }
                        else {
                            Toast.makeText(getActivity(), "ERROR :"+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        return;
                    }
                }
                @Override
                public void onFailure(Call<TransactionInfo> call, Throwable t) {
                    Toast.makeText(getActivity(), "rp :"+
                                    t.getMessage().toString(),
                            Toast.LENGTH_SHORT).show();
                }
            });
            mHandler.postDelayed(this, 2000);
        }
    };

    //FOR AUTO UPDATING DISTANCE FROM HANDYMAN TO USER STARTER
    private void autoUpdateDistanceStart() {
        autoUpdateDistanceRunnable.run();
    }
    //FOR AUTO UPDATING DISTANCE FROM HANDYMAN TO USER LOOPER
    public void autoUpdateDistanceStartLoop(){
        mHandler.removeCallbacks(autoUpdateDistanceRunnable);
        autoUpdateDistanceRunnable.run();
    }
    //FOR AUTO UPDATING DISTANCE FROM HANDYMAN TO USER STOPPER
    public void autoUpdateDistanceStop(String value){
        mHandler.removeCallbacks(autoUpdateDistanceRunnable);
        if(value.equals("2")){
            ((HomeActivity)getActivity()).unLockNavigationDrawer();
            Toast.makeText(getActivity(),"Unfortunately the transaction is cancelled by the user.",Toast.LENGTH_LONG).show();
            isAcquired = false;
            customerLocationMarker.setVisible(false);
            messageBtn.setVisibility(View.INVISIBLE);
            distanceInKmRelativeLayout.setVisibility(View.INVISIBLE);
            availabilitySwitch.setVisibility(View.VISIBLE);
        }
        else if(value.equals("3")){
            showRescheduleConfirmationForHM();
        }
        else{

        }
    }
    //FOR AUTO UPDATING DISTANCE FROM HANDYMAN TO USER
    private Runnable autoUpdateDistanceRunnable = new Runnable() {
        @Override
        public void run() {
            Call<TransactionInfo> call = apiInterface.checkTransactionAvailability("checkTransactionAvailabilityforHM",_ti_id,user_idFromActivity);
            call.enqueue(new Callback<TransactionInfo>() {
                @Override
                public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                    if(response.body()!=null){
                        String value = response.body().getValue();
                        if(value.equals("1")){
                            if(!hasArrived){
                                autoUpdateDistanceStartLoop();
                            }
                            else{
                                mHandler.removeCallbacks(autoUpdateDistanceRunnable);
                            }
                        }
                        else if(value.equals("2")){
                            autoUpdateDistanceStop(value);
                        }
                        else if(value.equals("3")){
                            rt_id = response.body().getRt_id();
                            rt_proposed_date = response.body().getRt_proposed_date();
                            transaction_id = response.body().getTransaction_id();
                            original_date = response.body().getRt_date();
                            autoUpdateDistanceStop(value);
                        }
                    }
                    else{
                        Toast.makeText(getActivity(),"ERROR",Toast.LENGTH_LONG);
                    }
                }
                @Override
                public void onFailure(Call<TransactionInfo> call, Throwable t) {
                    Toast.makeText(getActivity(), "rp :"+
                                    t.getMessage().toString(),
                            Toast.LENGTH_SHORT).show();
                }
            });
            mHandler.postDelayed(this, 10000);
        }
    };

    //POPUP FOR HANDYMAN RESCHEDULING CONFIRMATION OR CANCELLATION OF SERVICE
    private void showRescheduleConfirmationForHM() {
        C_R_F_H.setContentView(R.layout.confirmrescheduledatepopupforhm);
        C_R_F_H.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        C_R_F_H.show();
        C_R_F_H.setCancelable(false);
        C_R_F_H.setCanceledOnTouchOutside(false);

        cardViewConfirmResched = C_R_F_H.findViewById(R.id.cardViewConfirmResched);
        cardViewCancelResched = C_R_F_H.findViewById(R.id.cardViewCancelResched);
        originalDateTxtView = C_R_F_H.findViewById(R.id.originalDateTxtView);
        proposedDateTxtView = C_R_F_H.findViewById(R.id.proposedDateTxtView);
        originalDateTxtView.setText(original_date);
        proposedDateTxtView.setText(rt_proposed_date);

        cardViewConfirmResched.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                C_R_F_H.dismiss();
                updateRescheduledStatus();
                messageBtn.setVisibility(View.INVISIBLE);
                distanceInKmRelativeLayout.setVisibility(View.INVISIBLE);
                availabilitySwitch.setVisibility(View.VISIBLE);
            }
        });
        cardViewCancelResched.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmCancelPopUpForTransactionFromHandyman();
            }
        });
    }
    //POPUP FOR HANDYMAN FOR CONFIRMATION OF CANCELLING THE RESCHEDULED SERVICE
    private void showConfirmCancelPopUpForTransactionFromHandyman() {
        C_C_P.setContentView(R.layout.confirmcancellationpendingtransaction);
        C_C_P.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        cardViewYes = C_C_P.findViewById(R.id.cardViewYes);
        cardViewNo = C_C_P.findViewById(R.id.cardViewNo);
        C_C_P.show();
        C_C_P.setCancelable(false);
        C_C_P.setCanceledOnTouchOutside(false);
        cardViewNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                C_C_P.dismiss();
            }
        });
        cardViewYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteRescheduledStatus();
            }
        });
    }
    //DELETE RESCHEDULED SERVICE AND INSERT CANCELLED TRANSAC
    private void deleteRescheduledStatus() {
        Call<TransactionInfo> call = apiInterface.deleteRescheduledStatus("deleteRescheduledStatus",rt_id,_ti_id,user_idFromActivity,transaction_id);
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                if(response.body()!=null)
                {
                    String value = response.body().getValue();
                    if(value.equals("1")){
                        isAcquired = false;
                        customerLocationMarker.setVisible(false);
                        messageBtn.setVisibility(View.INVISIBLE);
                        distanceInKmRelativeLayout.setVisibility(View.INVISIBLE);
                        C_C_P.dismiss();
                        C_R_F_H.dismiss();
                        availabilitySwitch.setVisibility(View.VISIBLE);
                        getDeviceLocation();
                    }
                    else{
                        return;
                    }
                }
                else
                {
                    Toast.makeText(getActivity(),"ERROR NULL RESPONSE BODY",Toast.LENGTH_LONG).show();
                    return;
                }
            }
            @Override
            public void onFailure(Call<TransactionInfo> call, Throwable t) {
                Toast.makeText(getActivity(), "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    //UPDATE RESCHEDULED STATUS AFTER THE HANDYMA ACCEPTED THE RESCHEDULED SERVICE
    private void updateRescheduledStatus() {
        Call<TransactionInfo> call = apiInterface.updateRescheduledStatus("updateRescheduledStatus",rt_id);
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                if(response.body()!=null)
                {
                    String value = response.body().getValue();
                    if(value.equals("1")){
                       // sortDate(rt_proposed_date);
                        ((HomeActivity)getActivity()).unLockNavigationDrawer();
                        C_C_P.dismiss();
                        C_R_F_H.dismiss();
                    }
                    else{
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
            @Override
            public void onFailure(Call<TransactionInfo> call, Throwable t) {
                Toast.makeText(getActivity(), "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    //SORT DATE THAT IS FETCH FROM THE RESCHEDULED SERVICE FOR THE HANDYMAN ALARM
   /* private void sortDate(String rt_proposed_date) {
        String givenDateString = rt_proposed_date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Date mDate = sdf.parse(givenDateString);
            alarmStartTime = mDate.getTime();
            setUpAlarm();
            isAcquired = false;
            customerLocationMarker.setVisible(false);
            distanceInKmRelativeLayout.setVisibility(View.INVISIBLE);
            getDeviceLocation();
            availabilitySwitch.setVisibility(View.VISIBLE);

        }
        catch (ParseException e) {
            e.printStackTrace();
        }
    }*/

    //POPUP DIALOG FOR BOOKING OPTIONS
    private void showBookingOptDialog() {
        B_O_D.setContentView(R.layout.bookingoptionspopup);
        cardViewBookLater = B_O_D.findViewById(R.id.cardViewBookLater);
        cardViewBookNow = B_O_D.findViewById(R.id.cardViewBookNow);
        closeTxtView = B_O_D.findViewById(R.id.closeTxtView);
        B_O_D.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        B_O_D.show();
        B_O_D.setCancelable(false);
        B_O_D.setCanceledOnTouchOutside(false);
        cardViewBookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                B_O_D.dismiss();
                _task_type = "OnTheSpot";
               showFinalInstruction();
            }
        });
        cardViewBookLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _task_type = "ForLater";
                B_O_D.dismiss();
                showCalendar();
            }
        });

        closeTxtView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                B_O_D.dismiss();
            }
        });
    }

    private void showCalendar() {

        Calendar calendar = Calendar.getInstance();
        final int year,month,dayOfMonth,hourOfDay,minute;
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);

        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                rescheduledDate = year+"-"+(month+1)+"-"+dayOfMonth;

                new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if(hourOfDay>12){
                            hourOfDay = hourOfDay - 12;
                            rescheduledDate = year+"-"+(month+1)+"-"+dayOfMonth+" "+hourOfDay+":"+minute+":00 PM";
                            hourOfDay = hourOfDay + 12;
                            actualDate = year+"-"+(month+1)+"-"+dayOfMonth+" "+hourOfDay+":"+minute+":00";
                        }
                        else{
                            rescheduledDate = year+"-"+(month+1)+"-"+dayOfMonth+" "+hourOfDay+":"+minute+":00 AM";
                            actualDate = year+"-"+(month+1)+"-"+dayOfMonth+" "+hourOfDay+":"+minute+":00";
                        }

                        Calendar startTime = Calendar.getInstance();
                        startTime.set(Calendar.YEAR, year);
                        startTime.set(Calendar.MONTH, month);
                        startTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        startTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        startTime.set(Calendar.MINUTE, minute);
                        startTime.set(Calendar.SECOND, 0);

                        alarmStartTime = startTime.getTimeInMillis();
                        confirmScheduleDatePopUp(rescheduledDate,actualDate);
                    }
                }, hourOfDay,minute, false).show();
            }
        },year,month,dayOfMonth);

        datePickerDialog.show();
    }

    private void confirmScheduleDatePopUp(String date,String actualDate) {
        boolean havePassed = false;
        long currentInMilli,endInMilli,difference;
        double convertedDifference;
        Date currentTimeDate,endTimeDate=null;
        currentDateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").format(Calendar.getInstance().getTime());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a",Locale.US);
        try {
            currentTimeDate = sdf.parse(currentDateTime);
            endTimeDate = sdf.parse(date);
            currentInMilli = currentTimeDate.getTime();
            endInMilli = endTimeDate.getTime();
            difference = endInMilli - currentInMilli;
            convertedDifference = ((difference / (1000*60*60)) % 24);
            if(convertedDifference<=1.59) {
                havePassed = false;
            } else {
                havePassed = true;
            }
        } catch (ParseException ignored) {

        }
        sdf.applyPattern("MMMM dd yyyy hh:mm aa");
        if(havePassed){
            C_R_D.setContentView(R.layout.confirmrescheduleddatepopup);
            C_R_D.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            cardViewYes = C_R_D.findViewById(R.id.cardViewYes);
            cardViewNo = C_R_D.findViewById(R.id.cardViewNo);
            rescheduledDateTxtView = C_R_D.findViewById(R.id.rescheduledDateTxtView);
            rescheduledDateTxtView.setText(sdf.format(endTimeDate));
            C_R_D.show();
            C_R_D.setCancelable(false);
            C_R_D.setCanceledOnTouchOutside(false);
            cardViewYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    C_R_D.dismiss();
                    C_C_P.dismiss();
                    showFinalInstructionForBookLater(date,actualDate);
                }
            });
            cardViewNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    C_R_D.dismiss();
                    C_C_P.dismiss();
                }
            });
        }
        else{
            progressDialog2 = ProgressDialog.show(getActivity(), "", "You need to choose 2 hours ahead. Thank you.", true);
            progressDialog2.show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    progressDialog2.dismiss();
                    showCalendar();
                }
            }, 5000);
        }

    }

    private void showFinalInstructionForBookLater(String date,String actualDate) {
        F_I_P.setContentView(R.layout.serviceinstructionpopup);
        cardViewSendInfo = F_I_P.findViewById(R.id.cardViewSendInfo);
        cardViewCancel = F_I_P.findViewById(R.id.cardViewCancel);
        transactionDate = F_I_P.findViewById(R.id.transactionDate);
        transactionLocation = F_I_P.findViewById(R.id.transactionLocation);
        transactionInstruction = F_I_P.findViewById(R.id.transactionInstruction);
        promoCode = F_I_P.findViewById(R.id.promoTxtView);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        Date mDate = null;
        try {
            mDate = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sdf.applyPattern("MMMM dd yyyy hh:mm aa");

        currentDateTime = actualDate;

        transactionDate.setText(sdf.format(mDate));
        transactionLocation.setText(getCompleteAddressString(lati,longi));


        F_I_P.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        F_I_P.show();
        F_I_P.setCancelable(false);
        F_I_P.setCanceledOnTouchOutside(false);

        cardViewSendInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWaitingDialog();
                insertTransactionInfo("insertTransactionInfo");
            }
        });

        cardViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                F_I_P.dismiss();
            }
        });

        promoCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPromoList();
            }
        });

        promosClickListener = new PromoAdapter.promosClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                promo_id = promosList.get(position).getPromo_id();
                promoCode.setText(promosList.get(position).getPromo_code());
                L_I_P.dismiss();
            }
        };
    }

    //POPUP DIALOG FOR FINAL INSTRUCTIONS
    private void showFinalInstruction() {
        F_I_P.setContentView(R.layout.serviceinstructionpopup);
        cardViewSendInfo = F_I_P.findViewById(R.id.cardViewSendInfo);
        cardViewCancel = F_I_P.findViewById(R.id.cardViewCancel);
        transactionDate = F_I_P.findViewById(R.id.transactionDate);
        transactionLocation = F_I_P.findViewById(R.id.transactionLocation);
        transactionInstruction = F_I_P.findViewById(R.id.transactionInstruction);
        promoCode = F_I_P.findViewById(R.id.promoTxtView);

        currentDateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa").format(Calendar.getInstance().getTime());
        displayedCurrentTime = new SimpleDateFormat("MMMM/dd/yyyy hh:mm aa").format(Calendar.getInstance().getTime());

        transactionDate.setText(displayedCurrentTime);
        transactionLocation.setText(getCompleteAddressString(lati,longi));


        F_I_P.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        F_I_P.show();
        F_I_P.setCancelable(false);
        F_I_P.setCanceledOnTouchOutside(false);

        cardViewSendInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(transactionInstruction.getText().toString().trim()!=""||transactionInstruction.getText().toString().trim()!=null){
                    showWaitingDialog();
                    insertTransactionInfo("insertTransactionInfo");
                }
                else{
                    Toast.makeText(getActivity().getApplicationContext(),"Please enter a brief instruction.",Toast.LENGTH_LONG).show();
                }
            }
        });

        cardViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                F_I_P.dismiss();
            }
        });

        promoCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPromoList();
            }
        });

        promosClickListener = new PromoAdapter.promosClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                promo_id = promosList.get(position).getPromo_id();
                promoCode.setText(promosList.get(position).getPromo_code());
                L_I_P.dismiss();
            }
        };
    }

    //SHOWING AVAILABLE PROMOS THAT THE USER CAN USE
    private void showPromoList() {
        String key = "checkAvailablePromo";
        L_I_P.setContentView(R.layout.listofpromospopup);
        L_I_P.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        recyclerViewforPromos = L_I_P.findViewById(R.id.recyclerviewPromo);
        L_I_P.show();

        Call<List<Promos>> call = apiInterface.checkAvailablePromos(key,user_idFromActivity);
        call.enqueue(new Callback<List<Promos>>() {
            @Override
            public void onResponse(Call<List<Promos>> call, Response<List<Promos>> response) {
                promosList = response.body();
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerViewforPromos.setLayoutManager(layoutManager);
                promoAdapter = new PromoAdapter(promosList,getActivity(), promosClickListener);
                recyclerViewforPromos.setAdapter(promoAdapter);
                promoAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Promos>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }
    //INSERT TRANSACTION INFORMATION IN DATABASES
    private void insertTransactionInfo(String key) {
        taskLocation = transactionLocation.getText().toString().trim();
        taskInstruction = transactionInstruction.getText().toString().trim();
        taskPromo = promoCode.getText().toString().trim();
        Call<TransactionInfo> call = apiInterface.insertTransactionInfo(key,user_idFromActivity,serv_id,sub_id,currentDateTime,taskLocation,taskInstruction,lati,longi,promo_id,_task_type);
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                if(response.body()!=null){
                    String value = response.body().getValue();
                    String message = response.body().getMessage();
                    ti_id = response.body().getTi_id();
                    if(value.equals("1")){
                        Toast.makeText(getActivity().getApplicationContext(),"Successfuly booked.",Toast.LENGTH_SHORT).show();
                    }
                    else{
                        W_F_H.dismiss();
                        pulsator.stop();
                    }
                }
                else{
                    Toast.makeText(getActivity().getApplicationContext(), "ERROR RESPONSE BODY IS NULL",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TransactionInfo> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    //SHOW WAITING DIALOG FOR USER WHILE FETCHING HANDYMAN PROFILE
    private void showWaitingDialog() {
        W_F_H.setContentView(R.layout.pendingservicepopup);
        W_F_H.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        sylogo = W_F_H.findViewById(R.id.sylogo);
        pulsator = W_F_H.findViewById(R.id.pulsator);
        cardViewCancelTransaction = W_F_H.findViewById(R.id.cardViewCancelTransaction);
        W_F_H.show();
        pulsator.start();
        W_F_H.setCancelable(false);
        W_F_H.setCanceledOnTouchOutside(false);
        transactionInfoHandlerStart();
        cardViewCancelTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmCancelPopUpForTransaction();
            }
        });
    }

    //CONFIRMATION BEFORE DELETING TRANSACTION WITHOUT HANDYMAN
    private void showConfirmCancelPopUpForTransaction() {
        C_C_P.setContentView(R.layout.confirmcancellationpendingtransaction);
        C_C_P.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        cardViewYes = C_C_P.findViewById(R.id.cardViewYes);
        cardViewNo = C_C_P.findViewById(R.id.cardViewNo);
        C_C_P.show();
        C_C_P.setCancelable(false);
        C_C_P.setCanceledOnTouchOutside(false);
        cardViewNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                C_C_P.dismiss();
            }
        });
        cardViewYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                F_I_P.dismiss();
                W_F_H.dismiss();
                pulsator.stop();
                C_C_P.dismiss();
                mHandler.removeCallbacks(pendingTransactionRunnable);
                deletePendingTransaction();
            }
        });
    }
    //DELETING PENDING TRANSACTION
    private void deletePendingTransaction() {
        Call<TransactionInfo> call = apiInterface.deletePendingTransaction("deletePendingTransaction",ti_id);
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                String value = response.body().getValue();
                if(value.equals("1")){
                    Toast.makeText(getActivity(),"Cancelled",Toast.LENGTH_SHORT);
                }
                else {
                    Toast.makeText(getActivity(),"FAILED",Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call<TransactionInfo> call, Throwable t) {
                Toast.makeText(getActivity(), "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    //HANDLER FOR USER IN FETCHING HANDYMAN
    private void transactionInfoHandlerStart() {
        F_I_P.hide();
        pendingTransactionRunnable.run();
    }
    //STARTS THE LOOP OF PENDING TRANSACTION RUNNABLE
    public void transactionInfoHandlerStartLoop()
    {
        mHandler.removeCallbacks(pendingTransactionRunnable);
        pendingTransactionRunnable.run();
    }
    //STOP RUNNABLE
    public void transactionInfoHandlerStop()
    {
        W_F_H.dismiss();
        pulsator.stop();
        F_I_P.dismiss();
        mHandler.removeCallbacks(pendingTransactionRunnable);
        if(_task_type.equals("ForLater")){
            return;
        }
        else{
            fetchHandymanProfile("fetchHandymanProfle",_handyman_id);
        }
    }
    //RUNNABLE THAT WAITS FOR HANDYMAN THAT WILL ACCEPT THE TASK
    private Runnable pendingTransactionRunnable = new Runnable() {
        @Override
        public void run() {
            Call<TransactionInfo> call = apiInterface.checkTransactionHandyman("checkTransactionHandyman",user_idFromActivity,ti_id);
            call.enqueue(new Callback<TransactionInfo>() {
                @Override
                public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                    if(response.body()==null)
                    {
                        transactionInfoHandlerStartLoop();
                    }
                    else
                    {
                        if(response.body().getTask_type().equals("OnTheSpot")){
                            _ti_id = response.body().getTi_id();
                            _service_id = response.body().getService_id();
                            _subservice_id  = response.body().getSubservice_id();
                            _handyman_id = response.body().getHandyman_id();
                            conversation_id = response.body().getConversation_id();
                            transactionInfoHandlerStop();
                        }
                        else{
                            progressDialog = ProgressDialog.show(getActivity(), "THANK YOU!", "YOUR SERVICE IS ALREADY ACCEPTED AND RESERVED.", true);
                            progressDialog.show();
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    progressDialog.dismiss();
                                    transactionInfoHandlerStop();
                                }
                            }, 5000);
                        }
                    }
                }
                @Override
                public void onFailure(Call<TransactionInfo> call, Throwable t) {
                    Toast.makeText(getActivity(), "rp :"+
                                    t.getMessage().toString(),
                            Toast.LENGTH_SHORT).show();
                }
            });
            mHandler.postDelayed(this, 5000);
        }
    };
    //END HANDLER FOR USER IN FETCHING HANDYMAN

    //FETCHING THE HANDYMAN PROFILE
    private void fetchHandymanProfile(String key, int handyman_id) {
        ((HomeActivity)getActivity()).lockNavigationDrawer();
        String value = ti_id+"";
        int user_id = handyman_id;
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        Call<User> call = apiInterface.fetchHandymanProfile(key,user_id,value);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                String value = response.body().getValue();
                if(value.equals("1")){
                    _handymanName = response.body().getUser_fname()+" "+response.body().getUser_lname();
                    _handymanAddress = response.body().getUser_add();
                    _handymanQuickPitch = response.body().getService_quickpitch();
                    _handymanBio = response.body().getUser_description();
                    _handymanWorkInfo = response.body().getUser_workDescription();
                    _handymanPic = response.body().getUser_pic();
                    _handymanRate = response.body().getRating();
                    _handymanEmail = response.body().getUser_email();
                    convo_partner_email = response.body().getUser_email();
                    _handymanNumber = response.body().getUser_number();
                    _handyman_id = response.body().getUser_id();

                    recyclerViewforSubservices.setVisibility(View.INVISIBLE);
                    recyclerViewforServices.setVisibility(View.INVISIBLE);
                    handymanProfileLayoutVisibility = true;
                    showHandymanInfoLayout();
                    showProfileInfoPopUp(_handymanName,_handymanAddress,_handymanQuickPitch,_handymanBio,_handymanWorkInfo,_handymanPic,_handymanRate,_handyman_id);
                }
                else{
                    progressDialog = ProgressDialog.show(getActivity(), "ERROR", ""+response.body().getMassage(), true);
                    progressDialog.show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            progressDialog.dismiss();
                        }
                    }, 5000);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getActivity(), "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    //SHOWING HANDYMAN INFORMATION POPUP
    private void showProfileInfoPopUp(String handymanName, String handymanAddress, String handymanQuickPitch, String handymanBio, String handymanWorkInfo, String handymanPic, float handymanRate,int handyman_ids) {
        H_P_I.setContentView(R.layout.booknowpopup);
        H_P_I.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        initializeHandymanInfo(H_P_I);
        Glide.with(MapFragment.this)
                    .asBitmap()
                    .load(handymanPic)
                    .into(handyman_profile);
        handyman_name.setText(handymanName);
        handyman_add.setText(handymanAddress);
        handyman_rating.setRating(handymanRate);
        handyman_quickpitch.setText(handymanQuickPitch);
        handyman_bio.setText(handymanBio);
        handyman_workinfo.setText(handymanWorkInfo);
        H_P_I.show();
        profileInfoisOpen = true;

        handymanDocs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDocumentsPopup(handyman_ids);
            }
        });

        closeHandymanPopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                H_P_I.dismiss();
            }
        });
    }

    private void showDocumentsPopup(int handyman_id) {
        H_P_I.setContentView(R.layout.handymandocspopup);
        H_P_I.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        recyclerViewHandymanDocs = H_P_I.findViewById(R.id.recyclerViewHandyDocs);
        closeHandymanPopUp = H_P_I.findViewById(R.id.closeProfile);
        getHandymanDocs(handyman_id,recyclerViewHandymanDocs);
        H_P_I.show();
        H_P_I.setCancelable(false);
        H_P_I.setCanceledOnTouchOutside(false);
        closeHandymanPopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                H_P_I.dismiss();
            }
        });
    }

    private void getHandymanDocs(int user_id,RecyclerView recyclerView){
        String key = "selectHandymanDocs";
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        Call<List<HandymanDocs>> call = apiInterface.getHandymanDocs(key,user_id);
        call.enqueue(new Callback<List<HandymanDocs>>() {
            @Override
            public void onResponse(Call<List<HandymanDocs>> call, Response<List<HandymanDocs>> response) {
                hmDocsList = response.body();
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(layoutManager);
                handymanDocumentsAdapter = new HandymanDocumentsAdapter(hmDocsList,getActivity(), hmDocsListener);
                recyclerView.setAdapter(handymanDocumentsAdapter);

                handymanDocumentsAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<HandymanDocs>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    //INITIALIZING ELEMENTS TO XML FOR HANDYMAN INFO POPUP
    private void initializeHandymanInfo(Dialog h_p_i) {
        handyman_profile = h_p_i.findViewById(R.id.handyman_profile);
        handymanDocs = h_p_i.findViewById(R.id.docs);
        closeHandymanPopUp = h_p_i.findViewById(R.id.closeProfile);
        handyman_name = h_p_i.findViewById(R.id.handyman_name);
        handyman_add = h_p_i.findViewById(R.id.handyman_add);
        handyman_rating = h_p_i.findViewById(R.id.handyman_rating);
        LayerDrawable stars = (LayerDrawable) handyman_rating.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        handyman_quickpitch = h_p_i.findViewById(R.id.handymanQuickPitch);
        handyman_bio = h_p_i.findViewById(R.id.handymanInfo);
        handyman_workinfo = h_p_i.findViewById(R.id.handymanWorkInfo);
    }

    //FUNCTION THAT TRIGGERS THAT LAYOUT VISIBILITY
    private void showHandymanInfoLayout() {
        initializeHandymanInfoLayout();
        passValue();
        hmArrivalTrackerStart();
    }

    //HANDYMAN ARRIVAL TRACKER START
    private void hmArrivalTrackerStart() {
        hmArrivalTrackerRunnable.run();
    }
    //HANDYMAN ARRIVAL TRACKER START LOOPER
    private void hmArrivalTrackerLooperStart() {
        mHandler.removeCallbacks(hmArrivalTrackerRunnable);
        hmArrivalTrackerRunnable.run();
    }
    //HANDYMAN ARRIVAL TRACKER STOP
    private void hmArrivalTrackerStop() {
        mHandler.removeCallbacks(hmArrivalTrackerRunnable);
        showHandymanArrivalConfirmation();

    }
    //HANDYMAN ARRIVAL TRACKER RUNNABLE
    private Runnable hmArrivalTrackerRunnable = new Runnable() {
        @Override
        public void run() {
            hmTracker();
            mHandler.postDelayed(this, 5000);
        }
    };

    //POPUP FOR HANDYMAN'S ARRIVAL
    private void showHandymanArrivalConfirmation() {
        C_H_A.setContentView(R.layout.confirmarrivalpopup);
        C_H_A.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        cardViewYes = C_H_A.findViewById(R.id.cardViewConfirmHandymanArrival);
        cardViewNo = C_H_A.findViewById(R.id.cardViewCancelHandymanArrival);
        C_H_A.setCancelable(false);
        C_H_A.setCanceledOnTouchOutside(false);
        cardViewYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            setHandymanArrival();
            hmCompletionTrackerStart();
            }
        });
        cardViewNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            cancelHandymanArrival();
            }
        });
        C_H_A.show();
    }

    //HANDYMAN COMPLETION TRACKER START
    private void hmCompletionTrackerStart() {
        hmCompletionTrackerRunnable.run();
    }
    //HANDYMAN COMPLETION TRACKER LOOPER START
    private void hmCompletionTrackerLooperStart() {
        mHandler.removeCallbacks(hmCompletionTrackerRunnable);
        hmCompletionTrackerRunnable.run();
    }
    //HANDYMAN COMPLETION TRACKER LOOPER STOP
    private void hmCompletionTrackerStop() {
        mHandler.removeCallbacks(hmCompletionTrackerRunnable);
        showConfirmCompletionPopup();
    }
    //HANDYMAN COMPLETION TRACKER RUNNABLE
    private Runnable hmCompletionTrackerRunnable = new Runnable() {
        @Override
        public void run() {
            hmTracker();
            mHandler.postDelayed(this, 5000);
        }
    };

    //POPUP FOR HANDYMAN'S SERVICE COMPLETION
    private void showConfirmCompletionPopup() {
        C_S_C.setContentView(R.layout.confirmcompletionpopup);
        C_S_C.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        cardViewYes = C_S_C.findViewById(R.id.cardViewConfirmHandymanCompletion);
        cardViewNo = C_S_C.findViewById(R.id.cardViewCancelHandymanCompletion);
        C_S_C.show();
        C_S_C.setCancelable(false);
        C_S_C.setCanceledOnTouchOutside(false);
        cardViewYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setHandymanCompletion();
            }
        });
        cardViewNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelHandymanCompletion();

            }
        });
    }

    //CODE FOR UPDATING THE TRANSACTION INFO INTO SERVICE IN PROGRESS

    private void setHandymanArrival() {
        Call<TransactionInfo> call = apiInterface.setHandymanArrival("setHandymanArrival",user_idFromActivity,ti_id);
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                if(response.body()!=null)
                {
                    if(response.body().getValue().equals("1"))
                    {
                        C_H_A.dismiss();
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }

            @Override
            public void onFailure(Call<TransactionInfo> call, Throwable t) {
                Toast.makeText(getActivity(), "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    //CODE FOR CANCELING THE ARRIVAL OF HANDYMAN IF EVER THEY DID NOT MEET WITH THE CUSTOMERS YET
    private void cancelHandymanArrival() {
        Call<TransactionInfo> call = apiInterface.cancelHandymanArrival("cancelHandymanArrival",user_idFromActivity,ti_id);
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                if(response.body()!=null)
                {
                    if(response.body().getValue().equals("1"))
                    {
                        C_H_A.dismiss();
                        hmArrivalTrackerStart();
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }

            @Override
            public void onFailure(Call<TransactionInfo> call, Throwable t) {
                Toast.makeText(getActivity(), "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }


    //CODE FOR UPDATING THE TRANSACTION INFO TO COMPLETED
    private void setHandymanCompletion() {
        Call<TransactionInfo> call = apiInterface.setHandymanCompletion("setHandymanCompletion",user_idFromActivity,ti_id,warranty_id);
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                if(response.body()!=null)
                {
                    if(response.body().getValue().equals("1"))
                    {
                        C_S_C.dismiss();
                        recyclerViewforServices.setVisibility(View.VISIBLE);
                        if(handymanProfileLayoutVisibility){
                            handyman_info_layout_parent.setVisibility(View.INVISIBLE);
                        }
                        String transaction_date,subservice;
                        int transaction_id;
                        double transaction_earnings;

                        transaction_date = response.body().getTransaction_date();
                        subservice = response.body().getSubservice();
                        transaction_id = response.body().getTransaction_id();
                        transaction_earnings = response.body().getTransaction_earnings();
                        serviceCompletionPopup(transaction_id,transaction_earnings,subservice,transaction_date);

                    }
                    else if(response.body().getValue().equals("2")){
                        C_S_C.dismiss();
                        recyclerViewforServices.setVisibility(View.VISIBLE);
                        if(handymanProfileLayoutVisibility){
                            handyman_info_layout_parent.setVisibility(View.INVISIBLE);
                        }
                        progressDialog2 = ProgressDialog.show(getActivity(), "THANK YOU !", "You have already used your warranty for this service.", true);
                        progressDialog2.show();
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                ((HomeActivity)getActivity()).unLockNavigationDrawer();
                                progressDialog2.dismiss();
                            }
                        }, 5000);
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }

            @Override
            public void onFailure(Call<TransactionInfo> call, Throwable t) {
                Toast.makeText(getActivity(), "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    //CODE FOR CANCELLING THE COMPLETION OF THE SERVICE IF EVER THE REQUIREMENTS ARE NOT MEET YET
    private void cancelHandymanCompletion() {
        Call<TransactionInfo> call = apiInterface.cancelHandymanCompletion("cancelHandymanCompletion",user_idFromActivity,ti_id);
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                if(response.body()!=null)
                {
                    if(response.body().getValue().equals("1"))
                    {
                        C_S_C.dismiss();
                        hmCompletionTrackerStart();
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }

            @Override
            public void onFailure(Call<TransactionInfo> call, Throwable t) {
                Toast.makeText(getActivity(), "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    //HANDYMAN TRACKER
    private void hmTracker() {
        Call<TransactionInfo> call = apiInterface.hmTracker("hmTracker",user_idFromActivity,ti_id);
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                if(response.body()!=null)
                {
                    String value = response.body().getValue();
                    if(value.equals("0")){
                        hmArrivalTrackerLooperStart();
                    }
                    else if(value.equals("1")){
                        hmArrivalTrackerStop();
                    }
                    else if(value.equals("2")){
                        hmCompletionTrackerLooperStart();
                    }
                    else{
                        hmCompletionTrackerStop();
                    }

                }
                else
                {
                    return;
                }
            }

            @Override
            public void onFailure(Call<TransactionInfo> call, Throwable t) {
                Toast.makeText(getActivity(), "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    //PASSING OF VALUE TO THE LAYOUT
    private void passValue() {
        handyman_info_layout_parent.setVisibility(View.VISIBLE);
        handyman_acquired_name.setText(_handymanName);
        handyman_acquired_email.setText(_handymanEmail);
        handyman_acquired_number.setText(_handymanNumber);
        handyman_acquired_address.setText(_handymanAddress);
        Glide.with(MapFragment.this)
                .asBitmap()
                .load(_handymanPic)
                .into(handyman_acquired_picture);

        cardViewCancelHandymanAcquired.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmCancelPopUpForHandyman();
            }
        });

        cardViewMessageHandymanAcquired.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMessagesPopUp(user_idFromActivity,user_roleFromActivity);
            }
        });
        handyman_info_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProfileInfoPopUp(_handymanName,_handymanAddress,_handymanQuickPitch,_handymanBio,_handymanWorkInfo,_handymanPic,_handymanRate,_handyman_id);

            }
        });
    }

    private void openMessagesPopUp(int user_id,String user_role) {
        C_M_P.setContentView(R.layout.conversationmessagepopup);
        C_M_P.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        send = C_M_P.findViewById(R.id.cardViewSend);
        message = C_M_P.findViewById(R.id.messageHere);
        recyclerViewMessages = C_M_P.findViewById(R.id.recyclerViewMessages);
        close = C_M_P.findViewById(R.id.close);
        updateAvailability("onlineUser",user_id,user_role,conversation_id,recyclerViewMessages);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateAvailability("offlineUser",user_id,user_role,conversation_id,recyclerViewMessages);
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messages = message.getText().toString().trim();
                if(!messages.equals("")||messages!=null){
                    sendMessage(messages,convo_partner_email,recyclerViewMessages);
                }
                else{
                    Toast.makeText(getActivity().getApplicationContext(), "Please enter a message.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void updateAvailability(String key,int user_id, String user_role,int conversation_id,RecyclerView recyclerView) {
        Call<User> call = apiInterface.updateAvailability(key,user_id,user_role,conversation_id);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.body()!=null){
                    if(response.body().getValue().equals("1")){
                        if(key.equals("onlineUser")){
                            fetchMessages(recyclerViewMessages);
                            C_M_P.show();
                        }
                        else{
                            C_M_P.dismiss();
                        }
                    }
                    else{
                        Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                        response.body().getMassage(),
                                Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    return;
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void handlerStart(RecyclerView recyclerViewMessages) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                fetchMessages(recyclerViewMessages);
            }
        }, 10000);
    }

    private void sendMessage(String messages, String email,RecyclerView recyclerView) {
        Call<Messages> call = apiInterface.sendMessage("sendMessage",messages,email,user_roleFromActivity,user_idFromActivity+"");
        call.enqueue(new Callback<Messages>() {
            @Override
            public void onResponse(Call<Messages> call, Response<Messages> response) {
                if(response.body()!=null){
                    String value = response.body().getValue();
                    if(value.equals("1")){
                        fetchMessages(recyclerView);
                        message.setText("");
                    }
                    else{
                        Toast.makeText(getActivity(),"ERROR:"+response.body().getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
                else{
                    return;
                }
            }

            @Override
            public void onFailure(Call<Messages> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void fetchMessages(RecyclerView recyclerView) {
        Call<List<Messages>> call = apiInterface.fetchMessages("fetchMessages",conversation_id+"",user_idFromActivity,user_roleFromActivity);
        call.enqueue(new Callback<List<Messages>>() {
            @Override
            public void onResponse(Call<List<Messages>> call, Response<List<Messages>> response) {
                messagesList = response.body();
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(layoutManager);
                messagesAdapter = new MessagesAdapter(messagesList,getActivity(), messageClickListener);
                recyclerView.setAdapter(messagesAdapter);
                messagesAdapter.notifyDataSetChanged();
                layoutManager.scrollToPosition(messagesList.size()-1);
                handlerStart(recyclerView);
            }

            @Override
            public void onFailure(Call<List<Messages>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    //RELATIVE LAYOUT HANDYMAN INFO PASSING OF XML ELEMENT
    private void initializeHandymanInfoLayout() {
        handyman_info_layout_parent = v.findViewById(R.id.handyman_info_layout_parent);
        handyman_info_layout = v.findViewById(R.id.handyman_info_layout);
        cardViewCancelHandymanAcquired = v.findViewById(R.id.cardViewCancelHandymanAcquired);
        cardViewMessageHandymanAcquired = v.findViewById(R.id.cardViewMessageHandymanAcquired);
        handyman_acquired_name = v.findViewById(R.id.handyman_acquired_name);
        handyman_acquired_email = v.findViewById(R.id.handyman_acquired_email);
        handyman_acquired_number = v.findViewById(R.id.handyman_acquired_number);
        handyman_acquired_address = v.findViewById(R.id.handyman_acquired_address);
        handyman_acquired_picture = v.findViewById(R.id.handyman_acquired_picture);
    }
    //POPUP FOR CONFIRMATION OF CANCELLATION OR RESCHEDULE
    private void showConfirmCancelPopUpForHandyman() {
        C_C_P.setContentView(R.layout.confirmcancellationpopup);
        C_C_P.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        cardViewReschedule = C_C_P.findViewById(R.id.cardViewReschedule);
        cardViewYes = C_C_P.findViewById(R.id.cardViewYes);
        cardViewNo = C_C_P.findViewById(R.id.cardViewNo);
        C_C_P.show();
        C_C_P.setCancelable(false);
        C_C_P.setCanceledOnTouchOutside(false);

        cardViewYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                C_C_P.dismiss();
                reasonsListPopUp();
            }
        });

        cardViewNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                C_C_P.dismiss();
            }
        });

        cardViewReschedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendarReschedule();
            }
        });
    }

    private void showCalendarReschedule() {

        Calendar calendar = Calendar.getInstance();
        final int year,month,dayOfMonth,hourOfDay,minute;
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);

        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                rescheduledDate = year+"-"+(month+1)+"-"+dayOfMonth;

                new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if(hourOfDay>12){
                            hourOfDay = hourOfDay - 12;
                            rescheduledDate = year+"-"+(month+1)+"-"+dayOfMonth+" "+hourOfDay+":"+minute+":00 PM";
                        }
                        else{
                            rescheduledDate = year+"-"+(month+1)+"-"+dayOfMonth+" "+hourOfDay+":"+minute+":00 AM";
                        }

                        Calendar startTime = Calendar.getInstance();
                        startTime.set(Calendar.YEAR, year);
                        startTime.set(Calendar.MONTH, month);
                        startTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        startTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        startTime.set(Calendar.MINUTE, minute);
                        startTime.set(Calendar.SECOND, 0);

                        alarmStartTime = startTime.getTimeInMillis();
                        confirmRescheduleDatePopUp(rescheduledDate);
                    }
                }, hourOfDay,minute, false).show();
            }
        },year,month,dayOfMonth);

        datePickerDialog.show();
    }

    //POPUP FOR CONFIRMATION OF RESCHEDULING
    private void confirmRescheduleDatePopUp(String date) {
        boolean havePassed = false;
        long currentInMilli,endInMilli,difference;
        double convertedDifference;
        Date currentTimeDate,endTimeDate=null;
        currentDateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").format(Calendar.getInstance().getTime());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a",Locale.US);
        try {
            currentTimeDate = sdf.parse(currentDateTime);
            endTimeDate = sdf.parse(date);
            currentInMilli = currentTimeDate.getTime();
            endInMilli = endTimeDate.getTime();
            difference = endInMilli - currentInMilli;
            convertedDifference = ((difference / (1000*60*60)) % 24);
            if(convertedDifference<=1.59) {
                havePassed = false;
            } else {
                havePassed = true;
            }
        } catch (ParseException ignored) {

        }
        if(havePassed){
            C_R_D.setContentView(R.layout.confirmrescheduleddatepopup);
            C_R_D.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            cardViewYes = C_R_D.findViewById(R.id.cardViewYes);
            cardViewNo = C_R_D.findViewById(R.id.cardViewNo);
            rescheduledDateTxtView = C_R_D.findViewById(R.id.rescheduledDateTxtView);
            rescheduledDateTxtView.setText(date);
            C_R_D.show();
            C_R_D.setCancelable(false);
            C_R_D.setCanceledOnTouchOutside(false);
            cardViewYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    C_R_D.dismiss();
                    C_C_P.dismiss();
                    rescheduleTransaction("rescheduleTransaction",ti_id,date,user_idFromActivity);
                }
            });
            cardViewNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    C_R_D.dismiss();
                    C_C_P.show();
                }
            });
        }
        else{
            progressDialog2 = ProgressDialog.show(getActivity(), "", "You need to choose 2 hours ahead. Thank you.", true);
            progressDialog2.show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    progressDialog2.dismiss();
                    showCalendarReschedule();
                }
            }, 5000);
        }
    }
    //CODE FOR DATABASE UPDATING FOR RESCHEDULED TRANSACTION
    private void rescheduleTransaction(String key, int ti_id,String date,int user_id) {
        Call<TransactionInfo> call = apiInterface.rescheduleTransaction(key,ti_id,date,user_id);
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                if(response.body()!=null){
                    String value = response.body().getValue();
                    if(value.equals("1")){
                        rt_id = response.body().getRt_id();
                        rescheduledHandlerStart();
                    }
                }
            }
            @Override
            public void onFailure(Call<TransactionInfo> call, Throwable t) {
                Toast.makeText(getActivity(), "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    //LOOPER FOR USER TO CHECK IF THE HANDYMAN CONFIRMED OR CANCELLED THE RESCHEDULED TRANSACTION
    private void rescheduledHandlerStart() {
        progressDialog2 = ProgressDialog.show(getActivity(), "", "Let's wait for the handymans confirmation...", true);
        progressDialog2.show();
        rescheduledTransactionRunnable.run();
    }
    //STARTS THE LOOP TO CHECK IF THE HANDYMAN CONFIRMED OR CANCELLED THE RESCHEDULED TRANSACTION
    public void rescheduledInfoHandlerStartLoop()
    {
        mHandler.removeCallbacks(rescheduledTransactionRunnable);
        rescheduledTransactionRunnable.run();
    }
    //STOP RUNNABLE
    public void rescheduledHandlerStop(String value)
    {
        ((HomeActivity)getActivity()).unLockNavigationDrawer();
        mHandler.removeCallbacks(rescheduledTransactionRunnable);
        if(value.equals("2")){
            progressDialog = ProgressDialog.show(getActivity(), "CONGRATS", "The handyman have confirm your rescheduled date.", true);
            progressDialog.show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    progressDialog.dismiss();
                }
            }, 5000);
           // setUpAlarm();
            recyclerViewforServices.setVisibility(View.VISIBLE);
            if(handymanProfileLayoutVisibility){
                handymanProfileLayoutVisibility = false;
                handyman_info_layout_parent.setVisibility(View.INVISIBLE);

            }
        }
        else{
            progressDialog = ProgressDialog.show(getActivity(), "SORRY", "The handyman have cancelled your rescheduled service.", true);
            progressDialog.show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    progressDialog.dismiss();
                }
            }, 5000);
            recyclerViewforServices.setVisibility(View.VISIBLE);
            if(handymanProfileLayoutVisibility){
                handymanProfileLayoutVisibility = false;
                handyman_info_layout_parent.setVisibility(View.INVISIBLE);

            }
        }

    }

    //RUNNABLE TO CHECK IF THE HANDYMAN CONFIRMED OR CANCELLED THE RESCHEDULED TRANSACTION
    private Runnable rescheduledTransactionRunnable = new Runnable() {
        @Override
        public void run() {
            Call<TransactionInfo> call = apiInterface.checkRescheduledTransaction("checkRescheduledTransaction",user_idFromActivity,ti_id,rt_id);
            call.enqueue(new Callback<TransactionInfo>() {
                @Override
                public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                    if(response.body()!=null)
                    {
                        String value = response.body().getValue();
                        if(value.equals("1")){
                            rescheduledInfoHandlerStartLoop();
                        }
                        else if(value.equals("2")){
                            progressDialog2.dismiss();
                            rescheduledHandlerStop(value);
                        }
                        else{
                            progressDialog2.dismiss();
                            rescheduledHandlerStop(value);
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                @Override
                public void onFailure(Call<TransactionInfo> call, Throwable t) {
                    Toast.makeText(getActivity(), "rp :"+
                                    t.getMessage().toString(),
                            Toast.LENGTH_SHORT).show();
                }
            });
            mHandler.postDelayed(this, 5000);
        }
    };

    //EXTERNAL ALARM FOR RESCHEDULED SERVICES
    private void setUpAlarm() {
        Intent intent = new Intent(getActivity(), AlarmReceiver.class);
        intent.putExtra("notificationId", 1);
        intent.putExtra("todo", "IT'S TIME FOR YOUR RESCHEDULED SERVICE!");
        PendingIntent alarmIntent = PendingIntent.getBroadcast(getActivity(), 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarm = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
        alarm.set(AlarmManager.RTC_WAKEUP, alarmStartTime, alarmIntent);
        Toast.makeText(getActivity(),"SETUP ALARM SUCCESFULLY!",Toast.LENGTH_LONG).show();
    }
    //SHOW REASONS POPUP FOR CANCELLING TRANSACTION
    private void reasonsListPopUp() {
        String key = "checkReasons";
        L_O_R.setContentView(R.layout.listofreasonpopup);
        L_O_R.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        recyclerViewforReasons = L_O_R.findViewById(R.id.recyclerviewReasons);
        L_O_R.show();
        Call<List<Reasons>> call = apiInterface.checkReasons(key);
        call.enqueue(new Callback<List<Reasons>>() {
            @Override
            public void onResponse(Call<List<Reasons>> call, Response<List<Reasons>> response) {
                reasonsList = response.body();
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerViewforReasons.setLayoutManager(layoutManager);
                reasonsAdapter = new ReasonsAdapter(reasonsList,getActivity(), reasonsClickListener);
                recyclerViewforReasons.setAdapter(reasonsAdapter);
                reasonsAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Reasons>> call, Throwable t) {
                Toast.makeText(getActivity(), "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });

        reasonsClickListener = new ReasonsAdapter.reasonsClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                reason_id = reasonsList.get(position).getReasons_id();
                L_O_R.dismiss();
                cancelHandyman("cancelHandyman");
            }
        };

    }
    //CANCEL HANDYMAN FOR USER
    private void cancelHandyman(String key) {
        Call<TransactionInfo> call = apiInterface.cancelHandyman(key,user_idFromActivity,_handyman_id,ti_id,reason_id);
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                if(response.body().getValue().equals("1")){
                    recyclerViewforServices.setVisibility(View.VISIBLE);
                    if(handymanProfileLayoutVisibility){
                        handyman_info_layout_parent.setVisibility(View.INVISIBLE);
                        ((HomeActivity)getActivity()).unLockNavigationDrawer();
                    }
                }
                else {
                    return;
                }
            }
            @Override
            public void onFailure(Call<TransactionInfo> call, Throwable t) {
                Toast.makeText(getActivity(), "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    //GET COMPLETE ADDRESS OF THE USER
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null)
            {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            }
            if(strAdd.equals(""))
            {
                getCompleteAddressString(LATITUDE,LONGITUDE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return strAdd;

    }
    //GET THE DEVICE LOCATION LATITUDE AND LONGTITUDE
    public void getDeviceLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this.getActivity());
        if (ActivityCompat.checkSelfPermission(this.getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        Task location = fusedLocationProviderClient.getLastLocation();
        location.addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()) {
                    Location currentLocation = (Location) task.getResult();
                    getLatiLongi(currentLocation.getLatitude(),currentLocation.getLongitude());
                } else {
                    moveCamera(new LatLng(-15, 52), DEFAULT_ZOOM,"Not my Location");
                }

            }
        });
    }
    //GET LATITUDE AND LONGITUDE OF THE USER/HANDYMAN
    private void getLatiLongi(double latitude, double longitude) {
        if(latitude!=0&&longitude!=0){
            lati = latitude;
            longi = longitude;
            if(isAcquired){
                currentLocationMarker.setVisible(false);
                moveCamera(new LatLng(lati,longi),
                        DEFAULT_ZOOM,"My Location");
               // CalculationByDistance(new LatLng(lati,longi),new LatLng(_latitude,_longitude));
            }
            else{
                moveCamera(new LatLng(lati,longi),
                        DEFAULT_ZOOM,"My Location");
            }
        }
        else{
            moveCamera(new LatLng(-15,-15),
                    DEFAULT_ZOOM,"Unknown Location");
        }
    }
    //MOVE CAMERA TO WHERE THE USER/HANDYMAN IS
    private void moveCamera(LatLng latLng, float zoom, String title) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        origin = new MarkerOptions().position(latLng).title(title);
        currentLocationMarker = mMap.addMarker(origin);
    }
    //MOVE CAMERA WHERE THE CUSTOMER IS
    private void moveCamera2(LatLng latLng, float zoom, String title,String snippet) {
        ((HomeActivity)getActivity()).lockNavigationDrawer();
        passValueToDisplayDistance();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        destination = new MarkerOptions().position(latLng).title(title).snippet(snippet);
        destination.icon(BitmapDescriptorFactory.fromResource(R.drawable.serviceyouiconformap));
        customerLocationMarker = mMap.addMarker(destination);
        customerLocationMarker.showInfoWindow();
        displayDistance();

    }
    //PASSING THE VALUE TO THE DISTANCE TEXTVIEW, RELATIVE LAYOUT AND LABEL
    private void passValueToDisplayDistance() {
        messageBtn = v.findViewById(R.id.messageBtn);
        textViewArrived = v.findViewById(R.id.textViewArrived);
        distanceInKmRelativeLayout = v.findViewById(R.id.distanceInKmRelativeLayout);
        messageBtn.setVisibility(View.VISIBLE);
        distanceInKmRelativeLayout.setVisibility(View.VISIBLE);
        textViewArrived.setVisibility(View.VISIBLE);
        if(hasArrived){
            textViewArrived.setText("SERVICE IN PROGRESS");
        }
    }

    //DISPLAY THE DISTANCE OF THE HANDYMAN FROM THE CUSTOMER
    private void displayDistance() {
        distanceInKmRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textViewArrived.getText().toString().trim().equals("ARRIVED AT DESTINATION")) {
                    progressDialog2 = ProgressDialog.show(getActivity(), "", "Let's wait for the customers confirmation...", true);
                    progressDialog2.show();
                    autoUpdateDistanceStop("0");
                    updateArrival();
                }
                else {
                    progressDialog2 = ProgressDialog.show(getActivity(), "", "Let's wait for the customers confirmation...", true);
                    progressDialog2.show();
                    updateCompletion();
                }
            }
        });
        messageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMessagesPopUp(user_idFromActivity,user_roleFromActivity);
            }
        });

    }

    //HANDYMAN UPDATE HIS ARRIVAL
    private void updateArrival() {
        Call<TransactionInfo> call = apiInterface.updateArrival("updateArrival",user_idFromActivity,_ti_id);
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                if(response.body()!=null)
                {
                    if(response.body().getValue().equals("1"))
                    {
                        confirmationTrackerStart();
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }

            @Override
            public void onFailure(Call<TransactionInfo> call, Throwable t) {
                Toast.makeText(getActivity(), "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    //HANDYMAN UPDATE THE SERVICE COMPLETION
    private void updateCompletion() {
        Call<TransactionInfo> call = apiInterface.updateCompletion("updateCompletion",user_idFromActivity,_ti_id);
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                if(response.body()!=null)
                {
                    if(response.body().getValue().equals("1"))
                    {
                        confirmationTrackerStart();
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }

            @Override
            public void onFailure(Call<TransactionInfo> call, Throwable t) {
                Toast.makeText(getActivity(), "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    //HANDYMAN TRACKER FOR CUSTOMERS CONFIRMATION OF ARRIVAL AND COMPLETION
    private void confirmationTracker() {
        Call<TransactionInfo> call = apiInterface.confirmationTracker("confirmationTracker",user_idFromActivity,_ti_id,warranty_id);
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                if(response.body()!=null)
                {
                    String value = response.body().getValue();
                    if(value.equals("1")){
                        confirmationTrackerLooperStart();
                    }
                    else if(value.equals("2")){
                        progressDialog2.dismiss();
                        confirmationTrackerStop(value);
                    }
                    else if(value.equals("3")){
                        confirmationTrackerLooperStart();
                    }
                    else if(value.equals("5")){
                        progressDialog2.dismiss();
                        confirmationTrackerStop(value);
                    }
                    else if(value.equals("6"))
                    {
                        progressDialog2.dismiss();
                        confirmationTrackerStop(value);

                        progressDialog2 = ProgressDialog.show(getActivity(), "THANK YOU !", "You have served the warranty succesfully.", true);
                        progressDialog2.show();
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                ((HomeActivity)getActivity()).unLockNavigationDrawer();
                                progressDialog2.dismiss();
                            }
                        }, 5000);
                    }
                    else{
                        progressDialog2.dismiss();
                        confirmationTrackerStop(value);

                        String transaction_date,subservice;
                        int transaction_id;
                        double transaction_earnings;

                        transaction_date = response.body().getTransaction_date();
                        subservice = response.body().getSubservice();
                        transaction_id = response.body().getTransaction_id();
                        transaction_earnings = response.body().getTransaction_earnings();

                        serviceCompletionPopup(transaction_id,transaction_earnings,subservice,transaction_date);
                    }
                }
                else
                {
                    return;
                }
            }

            @Override
            public void onFailure(Call<TransactionInfo> call, Throwable t) {
                Toast.makeText(getActivity(), "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    //LOOPER THAT CHECKS THAT CONFIRMATION OF THE CUSTOMER
    private void confirmationTrackerStart() {
        confirmationTrackerRunnable.run();
    }
    //HANDYMAN COMPLETION TRACKER LOOPER START
    private void confirmationTrackerLooperStart() {
        mHandler.removeCallbacks(confirmationTrackerRunnable);
        confirmationTrackerRunnable.run();
    }
    //HANDYMAN COMPLETION TRACKER LOOPER STOP
    private void confirmationTrackerStop(String value) {
        mHandler.removeCallbacks(confirmationTrackerRunnable);

        if(value.equals("2")){
            textViewArrived.setText("SERVICE COMPLETED");
            hasArrived = false;
        }
        else if(value.equals("5")){
            progressDialog2 = ProgressDialog.show(getActivity(), "", "It seems like you and the customer's haven't met yet.. Please wait...", true);
            progressDialog2.show();
            hasArrived=false;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    progressDialog2.dismiss();
                    autoUpdateDistanceStart();
                }
            }, 10000);
        }
        else{
            availabilitySwitch.setVisibility(View.VISIBLE);
            textViewArrived.setText("ARRIVED AT DESTINATION");
            messageBtn.setVisibility(View.INVISIBLE);
            distanceInKmRelativeLayout.setVisibility(View.INVISIBLE);
            textViewArrived.setVisibility(View.INVISIBLE);
            customerLocationMarker.setVisible(false);
        }
    }
    //HANDYMAN COMPLETION TRACKER RUNNABLE
    private Runnable confirmationTrackerRunnable = new Runnable() {
        @Override
        public void run() {
            confirmationTracker();
            mHandler.postDelayed(this, 5000);
        }
    };

    //CALCULATE THE DISTANCE BETWEEN THE HANDYMAN AND CUSTOMER
    public void CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);

        double c = 2 * Math.asin(Math.sqrt(a));

        valueResult = Radius * c;
        km = valueResult / 1;
        meter = valueResult / 1000;

        DecimalFormat newFormat = new DecimalFormat("####.0000");
        kmInDec = Double.valueOf(newFormat.format(km));
    }

    //SERVICE COMPLETION POPUP FOR BOTH HANDYMAN AND CUSTOMER
    private void serviceCompletionPopup(int transaction_id, double transaction_earnings, String subservice, String transaction_date) {
        ((HomeActivity)getActivity()).unLockNavigationDrawer();
        C_S_P.setContentView(R.layout.completeservicepopup);
        C_S_P.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView transactionID,transactionDate,transactionSubservice,transactionPayment;
        CardView done;
        transactionID = C_S_P.findViewById(R.id.transactionID);
        transactionDate = C_S_P.findViewById(R.id.transactionDate);
        transactionSubservice = C_S_P.findViewById(R.id.transactionService);
        transactionPayment = C_S_P.findViewById(R.id.transactionPayment);
        done = C_S_P.findViewById(R.id.cardViewDoneService);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date mDate = null;
        try {
            mDate = sdf.parse(transaction_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sdf.applyPattern("MMMM dd yyyy");
        transactionID.setText(transaction_id+"");
        transactionDate.setText(sdf.format(mDate));
        transactionSubservice.setText(subservice);
        transactionPayment.setText(transaction_earnings+"");

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                C_S_P.dismiss();
                    showRatingPopup(transaction_id);
            }
        });


        C_S_P.show();
        C_S_P.setCancelable(false);
        C_S_P.setCanceledOnTouchOutside(false);
    }

    private void showRatingPopup(int transaction_id) {
        R_R_P.setContentView(R.layout.rateandreviewpopup);
        R_R_P.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        RatingBar ratingBar;
        final EditText reviews;
        CardView cardViewRate,cardViewRateLater;

        ratingBar = R_R_P.findViewById(R.id.ratingBar);
        reviews = R_R_P.findViewById(R.id.reviews);
        cardViewRate = R_R_P.findViewById(R.id.cardViewRate);
        cardViewRateLater = R_R_P.findViewById(R.id.cardViewRateLater);

        cardViewRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float rating = ratingBar.getRating();
                String reviewsByTheUser = reviews.getText().toString().trim();
                if(reviewsByTheUser==""||reviewsByTheUser==null){
                    Toast.makeText(getActivity().getApplicationContext(), "Please don't leave the field empty.", Toast.LENGTH_SHORT).show();
                }
                else{
                    if(user.getUser_role().equals("Handyman")){
                        insertRate(rating,reviewsByTheUser,user_idFromActivity,_customer_id,transaction_id);
                    }
                    else{
                        insertRate(rating,reviewsByTheUser,user_idFromActivity,_handyman_id,transaction_id);
                    }
                }
            }
        });

        cardViewRateLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                R_R_P.dismiss();
            }
        });

        R_R_P.show();
        R_R_P.setCancelable(false);
        R_R_P.setCanceledOnTouchOutside(false);
    }

    private void insertRate(float rating, String reviews, int rater_id, int user_id, int transaction_id) {
        Call<RateAndReview> call = apiInterface.rate("rate",rating,reviews,rater_id,user_id,transaction_id);
        call.enqueue(new Callback<RateAndReview>() {
            @Override
            public void onResponse(Call<RateAndReview> call, Response<RateAndReview> response) {
                if(response.body()!=null){
                    String value = response.body().getValue();
                    if(value.equals("1")){
                        Toast.makeText(getActivity(), "Succesfully rated and reviewed.", Toast.LENGTH_SHORT).show();
                        R_R_P.dismiss();
                    }
                    else{
                        return;
                    }
                }
                else {
                    return;
                }
            }

            @Override
            public void onFailure(Call<RateAndReview> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    //ON ACTIVITY RESULT WHERE INITIALIZATION STARTS
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
    //ON MAP READY
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        getDeviceLocation();
        if (ActivityCompat.checkSelfPermission(this.getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
    }

    //GET SERVICES FOR USER
    private void getServices(){
        Call<List<Services>> call = apiInterface.getServices();
        call.enqueue(new Callback<List<Services>>() {
            @Override
            public void onResponse(Call<List<Services>> call, Response<List<Services>> response) {
                servicesList = response.body();

                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                recyclerViewforServices.setLayoutManager(layoutManager);
                serviceAdapter = new ServiceAdapter(servicesList,getActivity(), serviceClickListener);
                recyclerViewforServices.setAdapter(serviceAdapter);

                serviceAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Services>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    //GET SUBSERVICES OF SERVICES THAT IS CHOSEN FOR USER
    private void getSubService(String key, int service_id) {
        Call<List<SubServices>> call = apiInterface.getSubServices(key,service_id);

        call.enqueue(new Callback<List<SubServices>>() {
            @Override
            public void onResponse(Call<List<SubServices>> call, Response<List<SubServices>> response) {
                subServicesList = response.body();

                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                recyclerViewforSubservices.setVisibility(View.VISIBLE);
                recyclerViewforSubservices.setLayoutManager(layoutManager);
                subServiceAdapter = new SubServiceAdapter(subServicesList,getActivity(), subServiceClickListener);
                recyclerViewforSubservices.setAdapter(subServiceAdapter);

                subServiceAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<SubServices>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }


}