package com.example.jecoy.serviceyou.Fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.example.jecoy.serviceyou.Adapters.TransactionsAdapter;
import com.example.jecoy.serviceyou.Adapters.WarrantyAdapter;
import com.example.jecoy.serviceyou.Api.ApiInterface;
import com.example.jecoy.serviceyou.Api.RetrofitClient;
import com.example.jecoy.serviceyou.Class.LoginUser;
import com.example.jecoy.serviceyou.Class.TransactionInfo;
import com.example.jecoy.serviceyou.Class.Warranty;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Storage.SharedPrefManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WarrantyFragment extends Fragment {
    View v;
    LoginUser user;
    int user_id;
    String user_role;
    RecyclerView recyclerView;
    ApiInterface apiInterface;
    WarrantyAdapter warrantyAdapter;
    WarrantyAdapter.warrantyClickListener warrantyClickListener;
    List<Warranty> warrantyList;
    Dialog R_W_P;

    ProgressDialog progressDialog;
    int transaction_id,warranty_id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        user = SharedPrefManager.getInstance(this.getActivity().getApplicationContext()).getUser();
        user_id = user.getUser_id();
        user_role = user.getUser_role();
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        v = inflater.inflate(R.layout.fragment_warranty, container, false);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        progressDialog = new ProgressDialog(this.getActivity());
        R_W_P = new Dialog(getActivity());
        fetchTransactions("fetchWarrantyList",user_id,user_role);
        warrantyClickListener = new WarrantyAdapter.warrantyClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if(user_role.equals("Customer")){
                    String warranty_status = warrantyList.get(position).getWarranty_status();
                    if(warranty_status.equals("Unused")){
                        transaction_id = warrantyList.get(position).getTransaction_id();
                        warranty_id = warrantyList.get(position).getWarranty_id();
                        checkHandymanAvailability(transaction_id);
                    }
                    else if(warranty_status.equals("Requested")){
                        Toast.makeText(getActivity().getApplicationContext(),"Your warranty request is being processed.",Toast.LENGTH_LONG);
                    }
                    else{
                        return;
                    }
                }
                else{
                    return;
                }
            }
        };
    }

    private void checkHandymanAvailability(int transaction_id) {
        Call<Warranty> call = apiInterface.checkHandymanAvailability("checkHandymanAvailability",transaction_id+"");
        call.enqueue(new Callback<Warranty>() {
            @Override
            public void onResponse(Call<Warranty> call, Response<Warranty> response) {
                if(response.body()!=null){
                    String value = response.body().getValue();
                    if(value.equals("1")){
                        progressDialog = ProgressDialog.show(getActivity(), "SORRY", "It seems like the handyman is busy at the moment. Please wait...", true);
                        progressDialog.show();
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                progressDialog.dismiss();
                            }
                        }, 5000);
                    }
                    else{
                        requestWarranty(warranty_id);
                    }
                }
                else{
                    Toast.makeText(getActivity().getApplicationContext(), "ERROR :"+
                                    response.body().getMessage(),
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Warranty> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void requestWarranty(int warranty_id) {
        R_W_P.setContentView(R.layout.requestwarrantypopup);
        R_W_P.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        final EditText request;
        CardView cardViewRequest,cardViewRequestLater;

        request = R_W_P.findViewById(R.id.warrantyinstruction);
        cardViewRequest = R_W_P.findViewById(R.id.cardViewRequest);
        cardViewRequestLater = R_W_P.findViewById(R.id.cardViewCancelRequest);

        cardViewRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String requestinstruction = request.getText().toString().trim();
                    insertWarrantyRequest("requestWarranty",warranty_id,requestinstruction);

            }
        });

        cardViewRequestLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                R_W_P.dismiss();
            }
        });

        R_W_P.show();
        R_W_P.setCancelable(false);
        R_W_P.setCanceledOnTouchOutside(false);

    }

    private void insertWarrantyRequest(String key, int warranty_id,String requestinstruction) {
        Call<Warranty> call = apiInterface.requestWarranty(key,warranty_id,requestinstruction);
        call.enqueue(new Callback<Warranty>() {
            @Override
            public void onResponse(Call<Warranty> call, Response<Warranty> response) {
                if(response.body()!=null){
                    String value = response.body().getValue();
                    if(value.equals("1")){
                        R_W_P.dismiss();
                    }
                    else {
                        return;
                    }
                }
                else{
                    return;
                }
            }

            @Override
            public void onFailure(Call<Warranty> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fetchTransactions(String key, int user_id, String user_role) {
        Call<List<Warranty>> call = apiInterface.fetchWarrantyList(key,user_id+"",user_role);
        call.enqueue(new Callback<List<Warranty>>() {
            @Override
            public void onResponse(Call<List<Warranty>> call, Response<List<Warranty>> response) {
                warrantyList = response.body();
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView = v.findViewById(R.id.recyclerViewWarranty);
                recyclerView.setLayoutManager(layoutManager);
                warrantyAdapter = new WarrantyAdapter(warrantyList,getActivity(), warrantyClickListener);
                recyclerView.setAdapter(warrantyAdapter);
                warrantyAdapter.notifyDataSetChanged();
            }
            @Override
            public void onFailure(Call<List<Warranty>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

}
