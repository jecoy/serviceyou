package com.example.jecoy.serviceyou.Class;

import com.google.gson.annotations.SerializedName;

public class HandymanDocs {
    @SerializedName("hm_docu_id")
    private int hm_docu_id;
    @SerializedName("user_id")
    private int user_id;

    @SerializedName("docu_pic")
    private String docu_pic;
    @SerializedName("docu_name")
    private String docu_name;
    @SerializedName("docu_info")
    private String docu_info;

    @SerializedName("value")
    private String value;
    @SerializedName("message")
    private String message;

    public int getHm_docu_id() {
        return hm_docu_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getDocu_pic() {
        return docu_pic;
    }

    public String getDocu_name() {
        return docu_name;
    }

    public String getDocu_info() {
        return docu_info;
    }

    public String getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }
}
