package com.example.jecoy.serviceyou.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.jecoy.serviceyou.Class.TransactionInfo;
import com.example.jecoy.serviceyou.Class.Warranty;
import com.example.jecoy.serviceyou.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class WarrantyAdapter extends RecyclerView.Adapter<WarrantyAdapter.RecyclerViewAdapter> {
    List<Warranty> warrantyList;
    Context context;
    warrantyClickListener warrantyClickListener;

    public WarrantyAdapter(List<Warranty> warrantyList, Context context, WarrantyAdapter.warrantyClickListener warrantyClickListener) {
        this.warrantyList = warrantyList;
        this.context = context;
        this.warrantyClickListener = warrantyClickListener;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listoftransactions,viewGroup,false);
        return new RecyclerViewAdapter(view,warrantyClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter recyclerViewAdapter, int i) {
        Warranty warranty = warrantyList.get(i);
        recyclerViewAdapter.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation));
        recyclerViewAdapter.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));
        recyclerViewAdapter.warrantyID.setText(warranty.getWarranty_id()+"");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date currentTimeDate=null;
        try {
            currentTimeDate = sdf.parse(warranty.getWarranty_expiration());
        } catch (ParseException ignored) {

        }
        sdf.applyPattern("MMMM dd,yyyy hh:mm a");

        recyclerViewAdapter.warrantyDate.setText(sdf.format(currentTimeDate));
        recyclerViewAdapter.warrantyStatus.setText(warranty.getWarranty_status());
    }

    @Override
    public int getItemCount() {
        return warrantyList.size();
    }

    public class RecyclerViewAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView warrantyID,warrantyDate,warrantyStatus;
        warrantyClickListener warrantyClickListener;
        RelativeLayout container;

        public RecyclerViewAdapter(@NonNull View itemView,warrantyClickListener warrantyClickListener) {
            super(itemView);
            warrantyID = itemView.findViewById(R.id.transactionID);
            warrantyDate = itemView.findViewById(R.id.transactionSubservice);
            warrantyStatus = itemView.findViewById(R.id.transactionDate);
            container = itemView.findViewById(R.id.container);
            this.warrantyClickListener = warrantyClickListener;
            container.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            warrantyClickListener.onItemClick(v, getAdapterPosition());
        }

    }

    public interface warrantyClickListener{
        void onItemClick(View view, int position);
    }
}
