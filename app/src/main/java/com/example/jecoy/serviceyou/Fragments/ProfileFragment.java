package com.example.jecoy.serviceyou.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.jecoy.serviceyou.Activities.HandymanRegistration;
import com.example.jecoy.serviceyou.Class.LoginUser;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Storage.SharedPrefManager;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment {
    CardView editProfileCardView;
    FloatingActionButton profile_image,cover_image;
    CircleImageView profilepicture;
    RelativeLayout coverpicture;
    ImageView membershippicture;
    EditText emailEditText,numberEditText,passwordEditText,editTextWorkingLoc,aboutyourselfEditTxt,aboutyourworkEditTxt,user_nameEditText;
    boolean editable = false;
    TextView editProfileTextView;
    View view;
    LoginUser user;
    RatingBar handymanAveRating;
    LinearLayout docsLayout,serviceLayout;
    int user_idFromActivity;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        user = SharedPrefManager.getInstance(this.getActivity().getApplicationContext()).getUser();
        user_idFromActivity = user.getUser_id();
        if(user.getUser_role().equals("Handyman")){
            view = inflater.inflate(R.layout.fragment_handyman_profile,container,false);
        }

        else{
            view = inflater.inflate(R.layout.fragment_profile,container,false);
        }
        varInitialized(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        readMode();
        editProfileCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editProfileTextView.getText().equals("EDIT PROFILE"))
                {
                    editMode();
                }
                else
                {
                    readMode();
                }
            }
        });
        if(user.getUser_role().equals("Handyman")){
            serviceLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent verificationIntent = new Intent(getActivity(), HandymanRegistration.class);
                    verificationIntent.putExtra("user_id",user_idFromActivity+"");
                    verificationIntent.putExtra("value",5);
                    verificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(verificationIntent);
                }
            });
            docsLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent verificationIntent = new Intent(getActivity(), HandymanRegistration.class);
                    verificationIntent.putExtra("user_id",user_idFromActivity+"");
                    verificationIntent.putExtra("value",4);
                    verificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(verificationIntent);
                }
            });
        }

        super.onViewCreated(view, savedInstanceState);
    }

    private void varInitialized(View view) {
        profilepicture = view.findViewById(R.id.profile_image);
        coverpicture = view.findViewById(R.id.cover_image);
        membershippicture = view.findViewById(R.id.memberLvl);
        aboutyourselfEditTxt = view.findViewById(R.id.aboutyourselfEditTxt);
        aboutyourworkEditTxt = view.findViewById(R.id.aboutyourworkEditTxt);
        editProfileCardView = view.findViewById(R.id.cardViewEditProfile);
        profile_image = view.findViewById(R.id.btnForPofile);
        cover_image = view.findViewById(R.id.btnForCover);
        editProfileTextView = view.findViewById(R.id.editProfileTextView);
        emailEditText = view.findViewById(R.id.editTextEmail);
        numberEditText = view.findViewById(R.id.editTextNumber);
        passwordEditText = view.findViewById(R.id.editTextPassword);
        editTextWorkingLoc = view.findViewById(R.id.editTextWorkingLoc);
        user_nameEditText = view.findViewById(R.id.editTextusername);
        handymanAveRating = view.findViewById(R.id.handymanAveRating);
        if(user.getUser_role().equals("Handyman")){
            serviceLayout = view.findViewById(R.id.serviceLayout);
            docsLayout = view.findViewById(R.id.docsLayout);
        }
    }

    public void readMode(){
        user_nameEditText.setFocusable(false);
        emailEditText.setFocusable(false);
        numberEditText.setFocusable(false);
        passwordEditText.setFocusable(false);
        emailEditText.setFocusable(false);
        numberEditText.setFocusable(false);
        passwordEditText.setFocusable(false);
        editProfileTextView.setText("EDIT PROFILE");
        profile_image.setVisibility(View.INVISIBLE);
        cover_image.setVisibility(View.INVISIBLE);

        if(user.getUser_role().equals("Handyman")){
            aboutyourworkEditTxt.setFocusable(false);
            aboutyourselfEditTxt.setFocusable(false);
            editTextWorkingLoc.setFocusable(false);
        }
    }

    public void editMode(){
        profile_image.setVisibility(View.VISIBLE);
        cover_image.setVisibility(View.VISIBLE);
        editable = true;
        emailEditText.setFocusableInTouchMode(true);
        numberEditText.setFocusableInTouchMode(true);
        passwordEditText.setFocusableInTouchMode(true);
        editProfileTextView.setText("SAVE");

        if(user.getUser_role().equals("Handyman")){
            aboutyourselfEditTxt.setFocusableInTouchMode(true);
            aboutyourworkEditTxt.setFocusableInTouchMode(true);
            editTextWorkingLoc.setFocusableInTouchMode(true);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if(user.getUser_role().equals("Customer")){
            passProfileValuesUser();
        }
        else{
            passProfileValuesHandyman();
        }
    }

    private void passProfileValuesHandyman() {
        emailEditText.setText(user.getUser_email());
        user_nameEditText.setText(user.getUser_fname()+" "+user.getUser_lname());
        numberEditText.setText(user.getUser_number());
        aboutyourselfEditTxt.setText(user.getUser_description());
        aboutyourworkEditTxt.setText(user.getUser_workDescription());
        editTextWorkingLoc.setText(user.getUser_address());

        Glide.with(getActivity().getApplicationContext())
                .asBitmap()
                .load(user.getUser_pic())
                .into(profilepicture);
    }

    private void passProfileValuesUser() {
        emailEditText.setText(user.getUser_email());
        user_nameEditText.setText(user.getUser_fname()+" "+user.getUser_lname());
        numberEditText.setText(user.getUser_number());

        Glide.with(getActivity().getApplicationContext())
                .asBitmap()
                .load(user.getUser_pic())
                .into(profilepicture);

        if(user.getUser_badge().equals("Bronze")){
            Glide.with(getActivity().getApplicationContext())
                    .asBitmap()
                    .load(R.drawable.icon_bronze)
                    .into(membershippicture);
        }
        else if(user.getUser_badge().equals("Silver")){
            Glide.with(getActivity().getApplicationContext())
                    .asBitmap()
                    .load(R.drawable.icon_silver)
                    .into(membershippicture);
        }
        else{
            Glide.with(getActivity().getApplicationContext())
                    .asBitmap()
                    .load(R.drawable.icon_gold)
                    .into(membershippicture);
        }
    }
}
