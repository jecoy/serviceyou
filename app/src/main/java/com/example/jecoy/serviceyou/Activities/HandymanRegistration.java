package com.example.jecoy.serviceyou.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.jecoy.serviceyou.Api.ApiInterface;
import com.example.jecoy.serviceyou.Api.RetrofitClient;
import com.example.jecoy.serviceyou.Class.User;
import com.example.jecoy.serviceyou.EmailPackage.SendMail;
import com.example.jecoy.serviceyou.Fragments.HandymanRegistrationFragment;
import com.example.jecoy.serviceyou.Fragments.ProfileFragment;
import com.example.jecoy.serviceyou.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HandymanRegistration extends AppCompatActivity {

    int value,back=0;
    String user_id;
    FragmentManager manager;
    FragmentTransaction ft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handyman_registration);

        Bundle registerData = getIntent().getExtras();
        manager = getSupportFragmentManager();
        ft = manager.beginTransaction();
        ft.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);

        if(registerData!=null){
            user_id = registerData.getString("user_id");
            value = registerData.getInt("value");
            if(value == 2 || value == 5){
                Bundle bundle = new Bundle();
                if(value==2){
                    value=2;
                }
                else{
                    value=5;
                }
                bundle.putInt("value",value);
                bundle.putString("user_id",user_id);
                HandymanRegistrationFragment myObj = new HandymanRegistrationFragment();
                myObj.setArguments(bundle);
                ft.replace(R.id.fragment_container,myObj).commit();
            }
            else if(value==3||value==4){
                if(value==3){
                    value=3;
                }
                else{
                    value=4;
                }
                Bundle bundle = new Bundle();
                bundle.putInt("value",value);
                bundle.putString("user_id",user_id);
                HandymanRegistrationFragment myObj = new HandymanRegistrationFragment();
                myObj.setArguments(bundle);
                ft.replace(R.id.fragment_container,myObj).commit();
            }
            else if(value==7){
                Intent verificationIntent = new Intent(HandymanRegistration.this, HomeActivity.class);
                verificationIntent.putExtra("profile","profile");
                verificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(verificationIntent);
            }
            else{
                Intent verificationIntent = new Intent(HandymanRegistration.this, EmailVerificationActivity.class);
                verificationIntent.putExtra("user_id",user_id);
                verificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(verificationIntent);
            }
        }
        else{
            value = 1;
            Bundle bundle = new Bundle();
            bundle.putInt("value",value);
            HandymanRegistrationFragment myObj = new HandymanRegistrationFragment();
            myObj.setArguments(bundle);

            ft.replace(R.id.fragment_container,
                    myObj).commit();
        }
    }

    @Override
    public void onBackPressed() {
        if(value == 2 && back == 0){
            back =1;
            if(back == 1){
                back = 2;
                Toast.makeText(getApplicationContext(),"Please press back again to exit.",Toast.LENGTH_SHORT);
            }
        }
        else if(value == 2 && back == 2){
                finishAndRemoveTask();
        }
        else if(value==3){
            value = 2;
            Bundle bundle = new Bundle();
            bundle.putInt("value",value);
            bundle.putString("user_id",user_id);
            HandymanRegistrationFragment myObj = new HandymanRegistrationFragment();
            myObj.setArguments(bundle);
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            ft.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);

            ft.replace(R.id.fragment_container,myObj).commit();
        }
        else{
            Intent verificationIntent = new Intent(HandymanRegistration.this, LoginActivity.class);
            verificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(verificationIntent);
        }
    }
}
