package com.example.jecoy.serviceyou.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.jecoy.serviceyou.Api.ApiInterface;
import com.example.jecoy.serviceyou.Api.RetrofitClient;
import com.example.jecoy.serviceyou.Class.LoginUser;
import com.example.jecoy.serviceyou.Class.MessageService;
import com.example.jecoy.serviceyou.Class.Messages;
import com.example.jecoy.serviceyou.Class.Notifications;
import com.example.jecoy.serviceyou.Class.RescheduledService;
import com.example.jecoy.serviceyou.Class.ReservedService;
import com.example.jecoy.serviceyou.Class.TransactionInfo;
import com.example.jecoy.serviceyou.Class.User;
import com.example.jecoy.serviceyou.Class.Warranty;
import com.example.jecoy.serviceyou.Class.WarrantyService;
import com.example.jecoy.serviceyou.Fragments.ApplicationReportFragment;
import com.example.jecoy.serviceyou.Fragments.BookingsFragment;
import com.example.jecoy.serviceyou.Fragments.HandymanRegistrationFragment;
import com.example.jecoy.serviceyou.Fragments.MessageFragment;
import com.example.jecoy.serviceyou.Fragments.NotificationFragment;
import com.example.jecoy.serviceyou.Fragments.PointsFragment;
import com.example.jecoy.serviceyou.Fragments.ProfileFragment;
import com.example.jecoy.serviceyou.Fragments.RateAndReviewFragment;
import com.example.jecoy.serviceyou.Fragments.TransactionsFragment;
import com.example.jecoy.serviceyou.Fragments.WarrantyFragment;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Storage.SharedPrefManager;
import com.google.android.gms.maps.MapFragment;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private DrawerLayout drawer;
    String user_id,user_role,notif_id;
    android.support.v7.widget.Toolbar toolbar;
    FragmentManager manager;
    FragmentTransaction ft;
    TextView email,fullname;
    NavigationView navigationView;
    String location=null;
    ImageView user_pic;
    LoginUser user;
    private ApiInterface apiInterface;
    int warranty_ids=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(R.layout.activity_home);
        user = SharedPrefManager.getInstance(getApplicationContext()).getUser();
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        manager = getSupportFragmentManager();
        ft = manager.beginTransaction();
        ft.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);


        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawer,toolbar,
                R.string.navigation_drawer_open,R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        View headerView = navigationView.getHeaderView(0);

        toggle.syncState();

        fullname = headerView.findViewById(R.id.fullnameTextView);
        email = headerView.findViewById(R.id.emailTextView);
        user_pic = headerView.findViewById(R.id.user_pic);
        Glide.with(getApplicationContext())
                .asBitmap()
                .load(user.getUser_pic())
                .into(user_pic);
        fullname.setText(user.getUser_fname()+" "+user.getUser_lname());
        email.setText(user.getUser_email());

        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction ft = manager.beginTransaction();
                ft.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
                ft.replace(R.id.fragment_containter,
                        new ProfileFragment()).commit();
                getSupportActionBar().setTitle("My Profile");
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        Bundle loginData = getIntent().getExtras();

        if(loginData!=null){
            location = loginData.getString("profile");
            user_id = loginData.getString("user_id");
            user_role = loginData.getString("user_role");
           // String ti_id = loginData.getString("ti_id");
            String conversation_id = loginData.getString("conversation_id");
            String warranty_id = loginData.getString("warranty_id");
           /* if(ti_id==null||ti_id.equals("")){

            }
            else{
                int ti_ids = Integer.parseInt(ti_id);
                updateTransaction(ti_ids);
            }*/
            if(conversation_id==null||conversation_id.equals("")){

            }
            else{
                int conversation_ids = Integer.parseInt(conversation_id);
                updateMessages(conversation_ids);
            }
            if(warranty_id==null||warranty_id.equals("")){

            }
            else{
                warranty_ids = Integer.parseInt(warranty_id);
                updateWarrantyStatus(warranty_ids);
            }

            if(location==null||!location.equals("")){

            }
            else{
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction ft = manager.beginTransaction();
                ft.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
                ft.replace(R.id.fragment_containter,
                        new ProfileFragment()).commit();
                getSupportActionBar().setTitle("My Profile");
                drawer.closeDrawer(GravityCompat.START);
            }
        }

        else{
            Toast.makeText(HomeActivity.this,"NO ID PASS!",Toast.LENGTH_LONG);
        }

        if(savedInstanceState==null||location==null||location.equals(""))
        {
            Bundle bundle = new Bundle();
            bundle.putInt("warranty_id",warranty_ids);
            com.example.jecoy.serviceyou.Fragments.MapFragment myObj = new com.example.jecoy.serviceyou.Fragments.MapFragment();
            myObj.setArguments(bundle);
            ft.replace(R.id.fragment_containter,myObj).commit();
            getSupportActionBar().setTitle("Service.YOU");
            navigationView.setCheckedItem(R.id.nav_home);
        }
    }

    private void updateWarrantyStatus(int warranty_ids) {
        retrofit2.Call<Warranty> call = apiInterface.updateWarrantyStatus("updateWarrantyStatus",warranty_ids,user.getUser_id()+"",user.getUser_role());
        call.enqueue(new Callback<Warranty>() {
            @Override
            public void onResponse(retrofit2.Call<Warranty> call, Response<Warranty> response) {
                if(response.body()!=null){
                    String value = response.body().getValue();
                    if(value.equals("1")){
                        return;
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"ERROR :"+response.body().getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(retrofit2.Call<Warranty> call, Throwable t) {

            }
        });
    }

    private void updateTransaction(int ti_ids) {
        retrofit2.Call<TransactionInfo> call = apiInterface.updateTransaction("updateRescheduledStatusNotification",ti_ids,user.getUser_id()+"",user.getUser_role());
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(retrofit2.Call<TransactionInfo> call, Response<TransactionInfo> response) {
                if(response.body()!=null){
                    String value = response.body().getValue();
                    if(value.equals("1")){
                        return;
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"ERROR :"+response.body().getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(retrofit2.Call<TransactionInfo> call, Throwable t) {

            }
        });
    }

    private void updateMessages(int conversation_id) {
        retrofit2.Call<Messages> call = apiInterface.updateMessages("updateMessages",conversation_id,user_id);
        call.enqueue(new Callback<Messages>() {
            @Override
            public void onResponse(retrofit2.Call<Messages> call, Response<Messages> response) {
                if(response.body()!=null){
                    String value = response.body().getValue();
                    if(value.equals("1")){
                      return;
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"ERROR :"+response.body().getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(retrofit2.Call<Messages> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        switch (menuItem.getItemId()){
            case R.id.nav_home:
                ft.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
                ft.replace(R.id.fragment_containter,
                        new com.example.jecoy.serviceyou.Fragments.MapFragment()).commit();
                getSupportActionBar().setTitle("Service.YOU");
                navigationView.setCheckedItem(R.id.nav_home);
                break;

            case R.id.nav_bookings:
                ft.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
                ft.replace(R.id.fragment_containter,
                        new BookingsFragment()).commit();
                getSupportActionBar().setTitle("My Bookings");
                navigationView.setCheckedItem(R.id.nav_bookings);
                break;
            case R.id.nav_points:
                ft.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
                ft.replace(R.id.fragment_containter,
                        new PointsFragment()).commit();
                getSupportActionBar().setTitle("My Points");
                navigationView.setCheckedItem(R.id.nav_points);
                break;
            case R.id.nav_transactions:
                ft.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
                ft.replace(R.id.fragment_containter,
                        new TransactionsFragment()).commit();
                getSupportActionBar().setTitle("My Transactions");
                navigationView.setCheckedItem(R.id.nav_transactions);
                break;
            case R.id.nav_warranty:
                ft.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
                ft.replace(R.id.fragment_containter,
                        new WarrantyFragment()).commit();
                getSupportActionBar().setTitle("My Warranty");
                navigationView.setCheckedItem(R.id.nav_warranty);
                break;
            case R.id.nav_messages:
                ft.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
                ft.replace(R.id.fragment_containter,
                        new MessageFragment()).commit();
                getSupportActionBar().setTitle("My Messages");
                navigationView.setCheckedItem(R.id.nav_messages);
                break;
            case R.id.nav_ratesAndreviews:
                ft.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
                ft.replace(R.id.fragment_containter,
                        new RateAndReviewFragment()).commit();
                getSupportActionBar().setTitle("My Rates and Reviews");
                navigationView.setCheckedItem(R.id.nav_ratesAndreviews);
                break;
            case R.id.nav_reportissues:
                ft.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
                ft.replace(R.id.fragment_containter,
                        new ApplicationReportFragment()).commit();
                getSupportActionBar().setTitle("My Reports");
                navigationView.setCheckedItem(R.id.nav_reportissues);
                break;
            case R.id.nav_logout:
                SharedPrefManager.getInstance(this).clear();
                Intent logOutIntent = new Intent(HomeActivity.this,LoginActivity.class);
                logOutIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(logOutIntent);
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)) {
        drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            super.onBackPressed();
        }
    }

    public void openNavigationDrawer() {
        drawer.openDrawer(Gravity.LEFT);
    }

    public void closeNavigationDrawer() {
        drawer.closeDrawer(Gravity.LEFT);
    }

    public void lockNavigationDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void unLockNavigationDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }
}
