package com.example.jecoy.serviceyou.Class;

import com.google.gson.annotations.SerializedName;

public class ApplicationReport {
    @SerializedName("reports_id")
    private int reports_id;
    @SerializedName("user_id")
    private int user_id;

    @SerializedName("report")
    private String report;
    @SerializedName("report_status")
    private String report_status;
    @SerializedName("report_date")
    private String report_date;

    @SerializedName("user_email")
    private String user_email;
    @SerializedName("user_role")
    private String user_role;
    @SerializedName("user_pic")
    private String user_pic;

    @SerializedName("report_isActive")
    private boolean report_isActive;
    @SerializedName("message")
    private String message;
    @SerializedName("value")
    private String value;

    public String getUser_pic() {
        return user_pic;
    }

    public String getUser_email() {
        return user_email;
    }

    public int getReports_id() {
        return reports_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getReport() {
        return report;
    }

    public String getReport_status() {
        return report_status;
    }

    public String getReport_date() {
        return report_date;
    }

    public String getUser_role() {
        return user_role;
    }

    public boolean isReport_isActive() {
        return report_isActive;
    }

    public String getMessage() {
        return message;
    }

    public String getValue() {
        return value;
    }
}
