package com.example.jecoy.serviceyou.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jecoy.serviceyou.Class.LoginUser;
import com.example.jecoy.serviceyou.Class.Messages;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Storage.SharedPrefManager;

import java.util.List;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.RecyclerViewAdapter> {
    List<Messages> messagesList;
    Context context;
    messageClickListener messageClickListener;

    public MessagesAdapter(List<Messages> messagesList, Context context, MessagesAdapter.messageClickListener messageClickListener) {
        this.messagesList = messagesList;
        this.context = context;
        this.messageClickListener = messageClickListener;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listofmessages,viewGroup,false);
        return new RecyclerViewAdapter(view,messageClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter recyclerViewAdapter, int i) {
        Messages messages= messagesList.get(i);

        recyclerViewAdapter.messages.setText(messages.getMessage());
        recyclerViewAdapter.sender.setText(messages.getMessage_sender());

        if(messages.getSender_id()==SharedPrefManager.getInstance(context).getUser().getUser_id()){
            recyclerViewAdapter.container.setBackgroundResource(R.drawable.popup2);
            recyclerViewAdapter.messages.setGravity(Gravity.RIGHT);
            recyclerViewAdapter.sender.setGravity(Gravity.RIGHT);
        }
        else{
            recyclerViewAdapter.container.setBackgroundResource(R.drawable.popup3);
            recyclerViewAdapter.messages.setGravity(Gravity.LEFT);
            recyclerViewAdapter.sender.setGravity(Gravity.LEFT);
            recyclerViewAdapter.messages.setTextColor(Color.BLACK);
            recyclerViewAdapter.sender.setTextColor(Color.BLACK);
        }
    }

    @Override
    public int getItemCount() {
        return messagesList.size();
    }

    public class RecyclerViewAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView messages,sender;
        RelativeLayout container;
        messageClickListener messageClickListener;
        public RecyclerViewAdapter(@NonNull View itemView, messageClickListener messageClickListener) {
            super(itemView);
            messages = itemView.findViewById(R.id.messages);
            sender = itemView.findViewById(R.id.sender);
            container = itemView.findViewById(R.id.container);
            this.messageClickListener = messageClickListener;
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
        messageClickListener.onItemClick(v,getAdapterPosition());
        }
    }

    public interface messageClickListener{
        void onItemClick(View view,int position);
    }
}
