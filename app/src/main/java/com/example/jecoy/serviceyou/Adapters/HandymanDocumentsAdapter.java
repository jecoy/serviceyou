package com.example.jecoy.serviceyou.Adapters;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jecoy.serviceyou.Class.HandymanDocs;
import com.example.jecoy.serviceyou.Class.HandymanServices;
import com.example.jecoy.serviceyou.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class HandymanDocumentsAdapter extends RecyclerView.Adapter<HandymanDocumentsAdapter.RecyclerViewAdapter> {
    List<HandymanDocs> handymanDocs;
    Context context;
    hmDocsListener hmDocsListener;

    public HandymanDocumentsAdapter(List<com.example.jecoy.serviceyou.Class.HandymanDocs> handymanDocs, Context context, HandymanDocumentsAdapter.hmDocsListener hmDocsListener) {
        this.handymanDocs = handymanDocs;
        this.context = context;
        this.hmDocsListener = hmDocsListener;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listofhandymandocs, parent, false);
        return new RecyclerViewAdapter(view,hmDocsListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter holder, int position) {
        HandymanDocs handymanDoc = handymanDocs.get(position);
        holder.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation));
        holder.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));
        Glide.with(context)
                .asBitmap()
                .load(handymanDoc.getDocu_pic())
                .into(holder.docu_pic);
        holder.docu_name.setText(handymanDoc.getDocu_name());
        holder.docu_info.setText(handymanDoc.getDocu_info());
    }

    @Override
    public int getItemCount() {
        return handymanDocs.size();
    }

    class RecyclerViewAdapter extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView docu_pic;
        TextView docu_name;
        TextView docu_info;
        hmDocsListener hmDocsListener;
        RelativeLayout container;

        RecyclerViewAdapter(@NonNull View itemView,hmDocsListener hmDocsListener) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            docu_pic = itemView.findViewById(R.id.image_view);
            docu_name = itemView.findViewById(R.id.docname);
            docu_info = itemView.findViewById(R.id.docinfo);

            this.hmDocsListener = hmDocsListener;
            docu_pic.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            hmDocsListener.onItemClick(v, getAdapterPosition());
        }
    }

    public interface hmDocsListener{
        void onItemClick(View view, int position);
    }

}

