package com.example.jecoy.serviceyou.Fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jecoy.serviceyou.Adapters.ApplicationReportAdapter;
import com.example.jecoy.serviceyou.Adapters.RateAndReviewAdapter;
import com.example.jecoy.serviceyou.Api.ApiInterface;
import com.example.jecoy.serviceyou.Api.RetrofitClient;
import com.example.jecoy.serviceyou.Class.ApplicationReport;
import com.example.jecoy.serviceyou.Class.LoginUser;
import com.example.jecoy.serviceyou.Class.RateAndReview;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Storage.SharedPrefManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApplicationReportFragment extends Fragment {
    View v;
    LoginUser user;
    int user_id;
    String user_role;
    RecyclerView recyclerView;
    ApiInterface apiInterface;
    ApplicationReportAdapter applicationReportAdapter;
    ApplicationReportAdapter.applicationReportClickListener applicationReportClickListener;
    List<ApplicationReport> applicationReports;
    FloatingActionButton btnAdd;
    Dialog A_R_P;
    CardView btnReport,btnCancel;
    EditText report;
    ProgressDialog progressDialog2;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        user = SharedPrefManager.getInstance(this.getActivity().getApplicationContext()).getUser();
        user_id = user.getUser_id();
        user_role = user.getUser_role();
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        v = inflater.inflate(R.layout.fragment_application_report, container, false);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        btnAdd = v.findViewById(R.id.addBtn);
        A_R_P = new Dialog(getActivity());
        progressDialog2 = new ProgressDialog(this.getActivity());

        fetchTransactions("fetchApplicationReport",user_id,user_role);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showApplicationReportPopup();
            }
        });

        applicationReportClickListener = new ApplicationReportAdapter.applicationReportClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                return;
            }
        };
    }

    private void showApplicationReportPopup() {
        A_R_P.setContentView(R.layout.applicationreport_popup);
        A_R_P.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        btnReport = A_R_P.findViewById(R.id.cardViewReport);
        btnCancel = A_R_P.findViewById(R.id.cardViewCancelReport);
        report = A_R_P.findViewById(R.id.report);

        A_R_P.show();
        A_R_P.setCancelable(false);
        A_R_P.setCanceledOnTouchOutside(false);

        btnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!report.getText().toString().trim().equals("")){
                    sendReport("sendReport",report.getText().toString().trim());
                }
                else{
                    return;
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                A_R_P.dismiss();
            }
        });
    }

    private void sendReport(String key, String report) {
        Call<ApplicationReport> call = apiInterface.sendReport(key,report,user_id);
        call.enqueue(new Callback<ApplicationReport>() {
            @Override
            public void onResponse(Call<ApplicationReport> call, Response<ApplicationReport> response) {
                if(response.body()!=null){
                    String value = response.body().getValue();
                    if(value.equals("1")){
                        progressDialog2 = ProgressDialog.show(getActivity(), "", "Report is submitted. Thank you.", true);
                        progressDialog2.show();
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                progressDialog2.dismiss();
                                A_R_P.dismiss();
                                fetchTransactions("fetchApplicationReport",user_id,user_role);
                            }
                        }, 2000);
                    }
                    else{
                        progressDialog2 = ProgressDialog.show(getActivity(), "", "Error 404."+response.body().getMessage(), true);
                        progressDialog2.show();
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                progressDialog2.dismiss();
                            }
                        }, 3000);
                    }
                }
                else {
                    return;
                }
            }

            @Override
            public void onFailure(Call<ApplicationReport> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fetchTransactions(String key, int user_id, String user_role) {
        Call<List<ApplicationReport>> call = apiInterface.fetchApplicationReports(key,user_id+"",user_role);
        call.enqueue(new Callback<List<ApplicationReport>>() {
            @Override
            public void onResponse(Call<List<ApplicationReport>> call, Response<List<ApplicationReport>> response) {
                applicationReports = response.body();
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView = v.findViewById(R.id.recyclewViewApplicationReport);
                recyclerView.setLayoutManager(layoutManager);
                applicationReportAdapter = new ApplicationReportAdapter(applicationReports,getActivity(), applicationReportClickListener);
                recyclerView.setAdapter(applicationReportAdapter);
                applicationReportAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<ApplicationReport>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });


    }

}
