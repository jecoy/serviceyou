package com.example.jecoy.serviceyou.Class;

import com.google.gson.annotations.SerializedName;

public class SubServices {
    @SerializedName("subservice_id")
    private int subservice_id;
    @SerializedName("service_id")
    private int service_id;
    @SerializedName("subservice")
    private String subservice;
    @SerializedName("subservice_icon")
    private String subservice_icon;
    @SerializedName("subservice_rate")
    private double subservice_rate;
    @SerializedName("subservice_status")
    private String subservice_status;
    @SerializedName("subservice_isActive")
    private boolean subservice_isActive;

    public int getSubservice_id() {
        return subservice_id;
    }

    public int getService_id() {
        return service_id;
    }

    public String getSubservice() {
        return subservice;
    }

    public String getSubservice_icon() {
        return subservice_icon;
    }

    public double getSubservice_rate() {
        return subservice_rate;
    }

    public String getSubservice_status() {
        return subservice_status;
    }

    public boolean isSubservice_isActive() {
        return subservice_isActive;
    }
}
