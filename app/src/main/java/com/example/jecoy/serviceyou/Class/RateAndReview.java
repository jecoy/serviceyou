package com.example.jecoy.serviceyou.Class;

import com.google.gson.annotations.SerializedName;

public class RateAndReview {
    @SerializedName("rating_id")
    private int rating_id;
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("rater_id")
    private int rater_id;
    @SerializedName("transaction_id")
    private int transaction_id;

    @SerializedName("rating_date")
    private String rating_date;
    @SerializedName("reviews")
    private String reviews;
    @SerializedName("ratings")
    private double ratings;

    @SerializedName("ratings_status")
    private String ratings_status;
    @SerializedName("rating_isActive")
    private boolean rating_isActive;

    @SerializedName("value")
    private String value;
    @SerializedName("message")
    private String message;

    @SerializedName("rater_email")
    private String rater_email;
    @SerializedName("user_email")
    private String user_email;
    @SerializedName("user_pic")
    private String user_pic;

    public String getUser_pic() {
        return user_pic;
    }

    public int getRating_id() {
        return rating_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public int getRater_id() {
        return rater_id;
    }

    public int getTransaction_id() {
        return transaction_id;
    }

    public String getRating_date() {
        return rating_date;
    }

    public String getReviews() {
        return reviews;
    }

    public double getRatings() {
        return ratings;
    }

    public String getRatings_status() {
        return ratings_status;
    }

    public boolean isRating_isActive() {
        return rating_isActive;
    }

    public String getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }

    public String getRater_email() {
        return rater_email;
    }

    public String getUser_email() {
        return user_email;
    }
}
