package com.example.jecoy.serviceyou.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jecoy.serviceyou.Adapters.PointsAdapter;
import com.example.jecoy.serviceyou.Adapters.TransactionInfoAdapter;
import com.example.jecoy.serviceyou.Api.ApiInterface;
import com.example.jecoy.serviceyou.Api.RetrofitClient;
import com.example.jecoy.serviceyou.Class.LoginUser;
import com.example.jecoy.serviceyou.Class.Points;
import com.example.jecoy.serviceyou.Class.TransactionInfo;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Storage.SharedPrefManager;

import org.w3c.dom.Text;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PointsFragment extends Fragment {
    View v;
    LoginUser user;
    int user_id;
    double total_points;
    String user_role;
    RecyclerView recyclerView;
    ApiInterface apiInterface;
    TextView currentPoints;
    List<Points> pointsList;
    PointsAdapter pointsAdapter;
    PointsAdapter.pointsClickListener pointsClickListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        user = SharedPrefManager.getInstance(this.getActivity().getApplicationContext()).getUser();
        user_id = user.getUser_id();
        user_role = user.getUser_role();
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        v = inflater.inflate(R.layout.fragment_points, container, false);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        currentPoints = v.findViewById(R.id.currentPoints);
        fetchPoints("fetchPointsUpdate",user_id,user_role);
        pointsClickListener = new PointsAdapter.pointsClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                return;
            }
        };
    }

    private void fetchPoints(String key, int user_id, String user_role) {
        Call<List<Points>> call = apiInterface.fetchPoints(key,user_id+"",user_role);
        call.enqueue(new Callback<List<Points>>() {
            @Override
            public void onResponse(Call<List<Points>> call, Response<List<Points>> response) {
                pointsList = response.body();
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView = v.findViewById(R.id.recyclerViewPoints);
                recyclerView.setLayoutManager(layoutManager);
                pointsAdapter = new PointsAdapter(pointsList,getActivity(), pointsClickListener);
                recyclerView.setAdapter(pointsAdapter);
                pointsAdapter.notifyDataSetChanged();
                getPoints();
            }

            @Override
            public void onFailure(Call<List<Points>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getPoints() {
        Call<Points> call = apiInterface.fetchTotalPoints("getAvailablePoints",user_id+"",user_role);
        call.enqueue(new Callback<Points>() {
            @Override
            public void onResponse(Call<Points> call, Response<Points> response) {
                if(response.body()!=null){
                    String value = response.body().getValue();
                    if(value.equals("1")){
                        total_points = response.body().getTotal_points();
                        currentPoints.setText(total_points+"");
                    }
                    else{
                        Toast.makeText(getActivity().getApplicationContext(), "ERROR",
                                Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    return;
                }
            }

            @Override
            public void onFailure(Call<Points> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}
