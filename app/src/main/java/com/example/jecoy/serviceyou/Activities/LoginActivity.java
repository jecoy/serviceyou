package com.example.jecoy.serviceyou.Activities;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jecoy.serviceyou.Api.ApiInterface;
import com.example.jecoy.serviceyou.Api.RetrofitClient;
import com.example.jecoy.serviceyou.Class.LoginUser;
import com.example.jecoy.serviceyou.Class.MessageService;
import com.example.jecoy.serviceyou.Class.User;
import com.example.jecoy.serviceyou.Class.WarrantyService;
import com.example.jecoy.serviceyou.EmailPackage.SendMail;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Storage.SharedPrefManager;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.w3c.dom.Text;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private CardView login,userRegister;
    private TextView handymanRegister,forgotPass;
    private EditText emailEditTextLogin,passEditTextLogin;
    String user_email,user_pass,user_id,email,message,subject;
    private  Dialog myDialog;
    private ApiInterface apiInterface;
    ProgressDialog progressDialog;
    Boolean emailVerified = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        setContentView(R.layout.activity_login);
        login =  findViewById(R.id.cardViewLogin);
        userRegister = findViewById(R.id.cardViewRegister);
        handymanRegister = findViewById(R.id.handyManRegister);
        forgotPass = findViewById(R.id.txtClickHere);
        myDialog = new Dialog(this);
        progressDialog = new ProgressDialog(this);
        emailEditTextLogin = findViewById(R.id.emailEditTextLogin);
        passEditTextLogin = findViewById(R.id.passwordEditTextLogin);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loginFunction("login");

            }
        });

        userRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(LoginActivity.this, UserRegistration.class);
                startActivity(registerIntent);
            }
        });

        handymanRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent hmRegisterIntent = new Intent(LoginActivity.this, HandymanRegistration.class);
                startActivity(hmRegisterIntent);
            }
        });

        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CardView cardViewEmailConfirm;
                final EditText emailAdd;
                final TextView congrats,message,confirmTxtView;

                myDialog.setContentView(R.layout.forgotpasswordpopup);

                congrats = myDialog.findViewById(R.id.textView7);
                message = myDialog.findViewById(R.id.textView8);
                cardViewEmailConfirm = myDialog.findViewById(R.id.cardViewEmailConfirm);
                emailAdd = myDialog.findViewById(R.id.emailAdd);
                confirmTxtView = myDialog.findViewById(R.id.confirmTxtView);
                cardViewEmailConfirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String email_forgotpass = emailAdd.getText().toString().trim();

                        if(email_forgotpass.isEmpty()){
                            emailAdd.setError("Please input empty fields.");
                            emailAdd.requestFocus();
                            return;
                        }
                        if(!Patterns.EMAIL_ADDRESS.matcher(email_forgotpass).matches()){
                            emailAdd.setError("Enter a valid email");
                            emailAdd.requestFocus();
                            return;
                        }

                        else{
                            sendPassToEmailFunction("selectByEmail",email_forgotpass);
                            if(emailVerified){
                                if(confirmTxtView.getText().toString().equals("CONFIRM"))
                                {
                                    emailAdd.setVisibility(View.GONE);
                                    congrats.setText("CONGRATULATIONS");
                                    message.setText("The password is sent to the email you provided, thanks.");
                                    confirmTxtView.setText("DONE");
                                }
                                else
                                    myDialog.dismiss();
                            }

                        }
                    }
                });
                myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                myDialog.show();
            }
        });

    }

    private void sendPassToEmailFunction(String key, final String email) {
        user_email = email;
        progressDialog.setMessage("Checking...");
        progressDialog.show();

        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        Call<User> call = apiInterface.forgotPasswordFunction(key,user_email);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                progressDialog.dismiss();

                Log.i(UserRegistration.class.getSimpleName(), response.toString());

                String value = response.body().getValue();
                String messages = response.body().getMassage();
                String retrievedEmail = response.body().getUser_email();
                String password = response.body().getUser_pass();

                if (value.equals("1")) {
                    subject = "PASSWORD RECOVERY:SERVICE.YOU";
                    message = "Hi "+retrievedEmail+" your forgotten password is "+password;
                    SendMail sm = new SendMail(LoginActivity.this,retrievedEmail,subject,message);
                    sm.execute();
                    emailVerified = true;
                }
                else {
                    Toast.makeText(LoginActivity.this, messages, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void loginFunction(String key) {
        user_email = emailEditTextLogin.getText().toString().trim();
        user_pass = passEditTextLogin.getText().toString().trim();

        if(user_email.isEmpty()||user_pass.isEmpty()){
           emailEditTextLogin.setError("Please input empty fields.");
           passEditTextLogin.setError("Please input empty fields");
           emailEditTextLogin.requestFocus();
           return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(user_email).matches()){
            emailEditTextLogin.setError("Enter a valid email");
            emailEditTextLogin.requestFocus();
            return;
        }

        else{
            progressDialog.setMessage("Checking...");
            progressDialog.show();

            apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
            Call<User> call = apiInterface.loginFunction(key,user_email,user_pass);

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    progressDialog.dismiss();

                    Log.i(EmailVerificationActivity.class.getSimpleName(), response.toString());

                    String value = response.body().getValue();
                    String messages = response.body().getMassage();
                    user_id = response.body().getUser_id()+"";
                    String user_email = response.body().getUser_email();
                    String user_fname = response.body().getUser_fname();
                    String user_lname = response.body().getUser_lname();
                    String user_role = response.body().getUser_role();
                    String user_status = response.body().getUser_status();
                    String user_pic = response.body().getUser_pic();
                    String user_address = response.body().getUser_add();
                    String user_number = response.body().getUser_number();

                    String user_description = response.body().getUser_description();
                    String user_workDescription = response.body().getUser_workDescription();
                    double hm_efficieny = response.body().getHm_efficiency();
                    String user_badge = response.body().getUser_badge();
                    String user_idPic = response.body().getUser_idPic();


                    if (value.equals("1")) {

                        SharedPrefManager.getInstance(LoginActivity.this)
                                .saveUser(Integer.parseInt(user_id),user_email,user_fname,user_lname,
                                        user_role,user_status,user_pic,user_address,user_number,user_description,user_workDescription,hm_efficieny+"",user_badge,user_idPic);

                        Intent verificationIntent = new Intent(LoginActivity.this,HomeActivity.class);
                        verificationIntent.putExtra("user_id",user_id);
                        verificationIntent.putExtra("user_role",user_role);
                        verificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(verificationIntent);


                    }
                    else if(value.equals("2")){
                        if(user_role.equals("Handyman")){
                            Intent verificationIntent = new Intent(LoginActivity.this, HandymanRegistration.class);
                            verificationIntent.putExtra("user_id",user_id);
                            verificationIntent.putExtra("value",2);
                            verificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(verificationIntent);
                        }
                        else{
                            Intent verificationIntent = new Intent(LoginActivity.this,EmailVerificationActivity.class);
                            verificationIntent.putExtra("user_id",user_id);
                            verificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(verificationIntent);
                        }
                    }
                    else {
                        Toast.makeText(LoginActivity.this, messages, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }



    @Override
    protected void onStart() {
        super.onStart();
        Intent warrantyServiceIntent = new Intent(getApplicationContext(),WarrantyService.class);
        getApplicationContext().startService(warrantyServiceIntent);
        Intent messageServiceIntent = new Intent(getApplicationContext(),MessageService.class);
        getApplicationContext().startService(messageServiceIntent);

        if (SharedPrefManager.getInstance(this).isLoggedIn()) {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }
}
