package com.example.jecoy.serviceyou.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jecoy.serviceyou.Class.Promos;
import com.example.jecoy.serviceyou.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PromoAdapter extends RecyclerView.Adapter<PromoAdapter.RecyclewViewAdapter> {
    List<Promos> promos;
    Context context;
    promosClickListener _promosClickListener;

    public PromoAdapter(List<Promos> promos, Context context, promosClickListener _promosClickListener) {
        this.promos = promos;
        this.context = context;
        this._promosClickListener = _promosClickListener;
    }

    @NonNull
    @Override
    public PromoAdapter.RecyclewViewAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listofpromos, viewGroup, false);
        return new RecyclewViewAdapter(view,_promosClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclewViewAdapter holder, int position) {
        Promos promo = promos.get(position);
        holder.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation));
        holder.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));
        holder.promocode.setText(promo.getPromo_code()+"");
        holder.promoprice.setText(promo.getPromo_price()+"");
        if(promo.getPromo_type().equals("Percentage")){
            holder.promoamount.setText(promo.getPromo_amount()+"%");
        }
        else{
            holder.promoamount.setText("PHP "+promo.getPromo_amount());
        }

    }

    @Override
    public int getItemCount() {
        return promos.size();
    }

    public class RecyclewViewAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView promocode;
        TextView promoamount,promoprice;
        promosClickListener promosClickListener;
        RelativeLayout container;
        CircleImageView icon;
        public RecyclewViewAdapter(@NonNull View itemView, PromoAdapter.promosClickListener _promosClickListener) {
            super(itemView);
            promocode = itemView.findViewById(R.id.promocode);
            promoamount = itemView.findViewById(R.id.promoamount);
            promoprice = itemView.findViewById(R.id.promoprice);
            container = itemView.findViewById(R.id.container);

            this.promosClickListener = _promosClickListener;
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            promosClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public interface promosClickListener{
        void onItemClick(View view, int position);
    }
}
