package com.example.jecoy.serviceyou.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.jecoy.serviceyou.Adapters.ReasonsAdapter;
import com.example.jecoy.serviceyou.Adapters.TransactionInfoAdapter;
import com.example.jecoy.serviceyou.Api.ApiInterface;
import com.example.jecoy.serviceyou.Api.RetrofitClient;
import com.example.jecoy.serviceyou.Class.LoginUser;
import com.example.jecoy.serviceyou.Class.TransactionInfo;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Storage.SharedPrefManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookingsFragment extends Fragment {
    View v;
    LoginUser user;
    int user_id;
    String user_role,handyman_id,customer_id;
    RecyclerView recyclerView;
    ApiInterface apiInterface;
    TransactionInfoAdapter transactionInfoAdapter;
    TransactionInfoAdapter.transactionInfoClickListener transactionInfoClickListener;
    List<TransactionInfo> transactionInfoList;
    CardView completed,cancelled,rescheduled,open;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        user = SharedPrefManager.getInstance(this.getActivity().getApplicationContext()).getUser();
        user_id = user.getUser_id();
        user_role = user.getUser_role();
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        v = inflater.inflate(R.layout.fragment_bookings, container, false);
         return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        completed = v.findViewById(R.id.cardViewCompleted);
        cancelled = v.findViewById(R.id.cardViewCancelled);
        rescheduled = v.findViewById(R.id.cardViewRescheduled);
        open = v.findViewById(R.id.cardViewOpen);

        completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchTransaction("fetchTransactions","Completed",user_id,user_role);
            }
        });
        cancelled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchTransaction("fetchTransactions","Cancelled",user_id,user_role);
            }
        });
        rescheduled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchTransaction("fetchTransactions","Rescheduled",user_id,user_role);
            }
        });
        open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchTransaction("fetchTransactions","",user_id,user_role);
            }
        });
        fetchTransaction("fetchTransactions","",user_id,user_role);

        transactionInfoClickListener = new TransactionInfoAdapter.transactionInfoClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                return;
            }
        };
    }

    private void fetchTransaction(String key,String filter, int user_id, String user_role) {
        Call<List<TransactionInfo>> call = apiInterface.fetchTransactions(key,filter,user_id+"",user_role);
        call.enqueue(new Callback<List<TransactionInfo>>() {
            @Override
            public void onResponse(Call<List<TransactionInfo>> call, Response<List<TransactionInfo>> response) {
                transactionInfoList = response.body();
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView = v.findViewById(R.id.recyclerViewBookings);
                recyclerView.setLayoutManager(layoutManager);
                transactionInfoAdapter = new TransactionInfoAdapter(transactionInfoList,getActivity(), transactionInfoClickListener);
                recyclerView.setAdapter(transactionInfoAdapter);
                transactionInfoAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<TransactionInfo>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}
