package com.example.jecoy.serviceyou.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.jecoy.serviceyou.Class.Points;
import com.example.jecoy.serviceyou.R;

import java.util.List;

public class PointsAdapter extends RecyclerView.Adapter<PointsAdapter.RecyclerViewAdapter> {
    List<Points> points;
    Context context;
    pointsClickListener pointsClickListener;

    public PointsAdapter(List<Points> points, Context context, PointsAdapter.pointsClickListener pointsClickListener) {
        this.points = points;
        this.context = context;
        this.pointsClickListener = pointsClickListener;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listofpoints,viewGroup,false);
        return new RecyclerViewAdapter(view,pointsClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter recyclerViewAdapter, int i) {
        Points point = points.get(i);
        recyclerViewAdapter.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation));
        recyclerViewAdapter.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));
        recyclerViewAdapter.pointsAmount.setText(point.getPoints_amount()+"");
        recyclerViewAdapter.pointsUpdate.setText(point.getPoints_update_type());
        recyclerViewAdapter.pointsUpdateDate.setText(point.getPoints_update_date());
    }

    @Override
    public int getItemCount() {
        return points.size();
    }

    public class RecyclerViewAdapter extends RecyclerView.ViewHolder implements AdapterView.OnClickListener {
        TextView pointsAmount,pointsUpdate,pointsUpdateDate;
        RelativeLayout container;
        pointsClickListener pointsClickListener;
        public RecyclerViewAdapter(@NonNull View itemView,pointsClickListener pointsClickListener) {
            super(itemView);
            pointsAmount = itemView.findViewById(R.id.pointsAmount);
            pointsUpdate = itemView.findViewById(R.id.pointsUpdateType);
            pointsUpdateDate = itemView.findViewById(R.id.pointsUpdateDate);
            container = itemView.findViewById(R.id.container);
            this.pointsClickListener = pointsClickListener;
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            pointsClickListener.onItemClick(v,getAdapterPosition());
        }
    }

    public interface pointsClickListener{
        void onItemClick(View view, int position);
    }
}
