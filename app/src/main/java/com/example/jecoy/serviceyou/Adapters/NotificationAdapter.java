package com.example.jecoy.serviceyou.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.jecoy.serviceyou.Class.Notifications;
import com.example.jecoy.serviceyou.Class.Points;
import com.example.jecoy.serviceyou.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.RecyclerViewAdapter> {
    List<Notifications> notificationsList;
    Context context;
    notificationClickListener notificationClickListener;

    public NotificationAdapter(List<Notifications> notificationsList, Context context, NotificationAdapter.notificationClickListener notificationClickListener) {
        this.notificationsList = notificationsList;
        this.context = context;
        this.notificationClickListener = notificationClickListener;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listofnotifications,viewGroup,false);
        return new RecyclerViewAdapter(view,notificationClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter recyclerViewAdapter, int i) {
        Notifications notifications = notificationsList.get(i);
        recyclerViewAdapter.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation));
        recyclerViewAdapter.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));
        recyclerViewAdapter.notifSub.setText(notifications.getNotif_subj());
        recyclerViewAdapter.notifCont.setText(notifications.getNotif_content());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date currentTimeDate=null;
        try {
            currentTimeDate = sdf.parse(notifications.getNotif_date());
        } catch (ParseException ignored) {

        }
        sdf.applyPattern("MMMM dd,yyyy hh:mm a");
        recyclerViewAdapter.notifDate.setText(sdf.format(currentTimeDate));
    }

    @Override
    public int getItemCount() {
        return notificationsList.size();
    }

    public class RecyclerViewAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView notifSub,notifCont,notifDate;
        RelativeLayout container;
        notificationClickListener notificationClickListener;
        public RecyclerViewAdapter(@NonNull View itemView,notificationClickListener notificationClickListener) {
            super(itemView);
            notifSub = itemView.findViewById(R.id.notifSubject);
            notifCont = itemView.findViewById(R.id.notifContent);
            notifDate = itemView.findViewById(R.id.notifDate);
            container = itemView.findViewById(R.id.container);
            this.notificationClickListener = notificationClickListener;
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            notificationClickListener.onItemClick(v,getAdapterPosition());
        }
    }

    public interface notificationClickListener{
        void onItemClick(View view,int position);
    }

}
