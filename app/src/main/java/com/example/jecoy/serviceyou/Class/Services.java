package com.example.jecoy.serviceyou.Class;

import com.google.gson.annotations.SerializedName;

public class Services {
    @SerializedName("service_id")
    private int service_id;
    @SerializedName("service")
    private String service;
    @SerializedName("service_icon")
    private String service_icon;
    @SerializedName("service_status")
    private String service_info;
    @SerializedName("service_isActive")
    private boolean service_isActive;

    public int getService_id() {
        return service_id;
    }

    public void setService_id(int service_id) {
        this.service_id = service_id;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService_icon() {
        return service_icon;
    }

    public void setService_icon(String service_icon) {
        this.service_icon = service_icon;
    }

    public String getService_info() {
        return service_info;
    }

    public void setService_info(String service_info) {
        this.service_info = service_info;
    }

    public boolean isService_isActive() {
        return service_isActive;
    }

    public void setService_isActive(boolean service_isActive) {
        this.service_isActive = service_isActive;
    }
}
