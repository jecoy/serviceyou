package com.example.jecoy.serviceyou.Class;

import com.google.gson.annotations.SerializedName;

public class HandymanServices {
    @SerializedName("hm_service_id")
    private int hm_service_id;
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("service_id")
    private int service_id;
    @SerializedName("subservice_id")
    private int subservice_id;
    @SerializedName("service_quickpitch")
    private String service_quickpitch;
    @SerializedName("service_levelofexp")
    private String service_levelofexp;
    @SerializedName("service")
    private String service;
    @SerializedName("subservice")
    private String subservice;
    @SerializedName("subservice_icon")
    private String subservice_icon;
    @SerializedName("subservice_rate")
    private double subservice_rate;

    @SerializedName("value")
    private String value;
    @SerializedName("message")
    private String message;

    public void setHm_service_id(int hm_service_id) {
        this.hm_service_id = hm_service_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void setService_id(int service_id) {
        this.service_id = service_id;
    }

    public void setSubservice_id(int subservice_id) {
        this.subservice_id = subservice_id;
    }

    public void setService_quickpitch(String service_quickpitch) {
        this.service_quickpitch = service_quickpitch;
    }

    public void setService_levelofexp(String service_levelofexp) {
        this.service_levelofexp = service_levelofexp;
    }

    public void setService(String service) {
        this.service = service;
    }

    public void setSubservice(String subservice) {
        this.subservice = subservice;
    }

    public void setSubservice_icon(String subservice_icon) {
        this.subservice_icon = subservice_icon;
    }

    public void setSubservice_rate(double subservice_rate) {
        this.subservice_rate = subservice_rate;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setMassage(String message) {
        this.message = message;
    }


    public String getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }

    public int getHm_service_id() {
        return hm_service_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public int getService_id() {
        return service_id;
    }

    public int getSubservice_id() {
        return subservice_id;
    }

    public String getService_quickpitch() {
        return service_quickpitch;
    }

    public String getService_levelofexp() {
        return service_levelofexp;
    }

    public String getService() {
        return service;
    }

    public String getSubservice() {
        return subservice;
    }

    public String getSubservice_icon() {
        return subservice_icon;
    }

    public double getSubservice_rate() {
        return subservice_rate;
    }

}
