package com.example.jecoy.serviceyou.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.jecoy.serviceyou.Adapters.RateAndReviewAdapter;
import com.example.jecoy.serviceyou.Adapters.TransactionsAdapter;
import com.example.jecoy.serviceyou.Api.ApiInterface;
import com.example.jecoy.serviceyou.Api.RetrofitClient;
import com.example.jecoy.serviceyou.Class.LoginUser;
import com.example.jecoy.serviceyou.Class.RateAndReview;
import com.example.jecoy.serviceyou.Class.TransactionInfo;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Storage.SharedPrefManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RateAndReviewFragment extends Fragment {
    View v;
    LoginUser user;
    int user_id;
    String user_role;
    RecyclerView recyclerView;
    ApiInterface apiInterface;
    RateAndReviewAdapter rateAndReviewAdapter;
    RateAndReviewAdapter.rateAndReviewClickListener rateAndReviewClickListener;
    List<RateAndReview> rateAndReviewList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        user = SharedPrefManager.getInstance(this.getActivity().getApplicationContext()).getUser();
        user_id = user.getUser_id();
        user_role = user.getUser_role();
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        v = inflater.inflate(R.layout.fragment_rate_and_review, container, false);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        fetchTransactions("fetchRateAndReview",user_id,user_role);
        rateAndReviewClickListener = new RateAndReviewAdapter.rateAndReviewClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                return;
            }
        };
    }

    private void fetchTransactions(String key, int user_id, String user_role) {
        Call<List<RateAndReview>> call = apiInterface.fetchRateAndReview(key,user_id+"",user_role);
        call.enqueue(new Callback<List<RateAndReview>>() {
            @Override
            public void onResponse(Call<List<RateAndReview>> call, Response<List<RateAndReview>> response) {
                rateAndReviewList = response.body();
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView = v.findViewById(R.id.recyclewViewRateAndReview);
                recyclerView.setLayoutManager(layoutManager);
                rateAndReviewAdapter = new RateAndReviewAdapter(rateAndReviewList,getActivity(), rateAndReviewClickListener);
                recyclerView.setAdapter(rateAndReviewAdapter);
                rateAndReviewAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<RateAndReview>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }
}
