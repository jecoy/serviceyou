package com.example.jecoy.serviceyou.Api;

import com.example.jecoy.serviceyou.Class.ApplicationReport;
import com.example.jecoy.serviceyou.Class.Conversations;
import com.example.jecoy.serviceyou.Class.HandymanDocs;
import com.example.jecoy.serviceyou.Class.HandymanServices;
import com.example.jecoy.serviceyou.Class.Messages;
import com.example.jecoy.serviceyou.Class.Notifications;
import com.example.jecoy.serviceyou.Class.Points;
import com.example.jecoy.serviceyou.Class.Promos;
import com.example.jecoy.serviceyou.Class.RateAndReview;
import com.example.jecoy.serviceyou.Class.Reasons;
import com.example.jecoy.serviceyou.Class.Services;
import com.example.jecoy.serviceyou.Class.SubServices;
import com.example.jecoy.serviceyou.Class.TransactionInfo;
import com.example.jecoy.serviceyou.Class.User;
import com.example.jecoy.serviceyou.Class.Warranty;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


public interface ApiInterface{

        @FormUrlEncoded
        @POST("android_api_user.php")
        Call<User> addUser(
                @Field("key") String key,
                @Field("user_fname") String user_fname,
                @Field("user_lname") String user_lname,
                @Field("user_add") String user_add,
                @Field("user_number") String user_number,
                @Field("user_email") String user_email,
                @Field("user_pass") String user_pass,
                @Field("user_pic") String user_pic,
                @Field("user_idPic") String user_idPic,
                @Field("verification_code") String verification_code);

    @FormUrlEncoded
    @POST("android_api_user.php")
    Call<User> checkVerification(
            @Field("key") String key,
            @Field("user_id") String user_id,
            @Field("verification_code") String verification_code);

    @FormUrlEncoded
    @POST("android_api_user.php")
    Call<User> loginFunction(
            @Field("key") String key,
            @Field("user_email") String user_email,
            @Field("user_pass") String user_pass);

    @FormUrlEncoded
    @POST("android_api_user.php")
    Call<User> forgotPasswordFunction(
            @Field("key") String key,
            @Field("user_email") String user_email);

    @FormUrlEncoded
    @POST("android_api_user.php")
    Call<User> checkHandymanPoints(
            @Field("key") String key,
            @Field("user_id") int user_id);

    @FormUrlEncoded
    @POST("android_api_user.php")
    Call<User> updateBanned(
            @Field("key") String key,
            @Field("user_id") int user_id);

    @FormUrlEncoded
    @POST("android_api_user.php")
    Call<List<Notifications>> getAllNotificationUsers(
            @Field("key") String key,
            @Field("user_id") int user_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<List<Messages>> getAllUnreadMessages(
            @Field("key") String key,
            @Field("user_id") int user_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> updateTransaction(
            @Field("key") String key,
            @Field("ti_id") int ti_id,
            @Field("user_id") String user_id,
            @Field("user_role") String user_role);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<Warranty> updateWarrantyStatus(
            @Field("key") String key,
            @Field("warranty_id") int warranty_id,
            @Field("user_id") String user_id,
            @Field("user_role") String user_role);

    @FormUrlEncoded
    @POST("android_api_user.php")
    Call<List<Notifications>> fetchNotifications(
            @Field("key") String key,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<Warranty> checkHandymanAvailability(
            @Field("key") String key,
            @Field("transaction_id") String transaction_id);


    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<Messages> updateMessages(
            @Field("key") String key,
            @Field("conversation_id") int conversation_id,
            @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("android_api_user.php")
    Call<User> addHandyman(
            @Field("key") String key,
            @Field("user_fname") String user_fname,
            @Field("user_lname") String user_lname,
            @Field("user_add") String user_add,
            @Field("user_number") String user_number,
            @Field("user_email") String user_email,
            @Field("user_pass") String user_pass,
            @Field("user_pic") String user_pic,
            @Field("verification_code") String verification_code,
            @Field("user_description") String user_description,
            @Field("user_workDescription") String user_workDescription,
            @Field("hms_location") String hms_location);

    @FormUrlEncoded
    @POST("android_api_user.php")
    Call<HandymanDocs> addHandymanDocs(
            @Field("key") String key,
            @Field("user_id") int user_id,
            @Field("docu_pic") String docu_pic,
            @Field("docu_name") String docu_name,
            @Field("docu_info") String docu_info);

    @POST("android_api_getServices.php")
    Call<List<Services>> getServices();

    @FormUrlEncoded
    @POST("android_api_getServices.php")
    Call<List<SubServices>> getSubServices(
                    @Field("key") String key,
                    @Field("service_id") int service_id);

    @FormUrlEncoded
    @POST("android_api_getServices.php")
    Call<List<HandymanServices>> getHandymanServices(
            @Field("key") String key,
            @Field("user_id") int user_id);

    @FormUrlEncoded
    @POST("android_api_getServices.php")
    Call<HandymanServices> checkHandymanServices(
            @Field("key") String key,
            @Field("subservice_id") int subservice_id,
            @Field("user_id") int user_id);

    @FormUrlEncoded
    @POST("android_api_getServices.php")
    Call<HandymanServices> addHandymanServices(
            @Field("key") String key,
            @Field("subservice_id") int subservice_id,
            @Field("service_id") int service_id,
            @Field("user_id") int user_id,
            @Field("service_levelofexp") String service_levelofexp,
            @Field("service_quickpitch") String service_quickpitch);

    @FormUrlEncoded
    @POST("android_api_user.php")
    Call<List<HandymanDocs>> getHandymanDocs(
            @Field("key") String key,
            @Field("user_id") int user_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> insertTransactionInfo(
            @Field("key") String key,
            @Field("customer_id") int customer_id,
            @Field("service_id") int service_id,
            @Field("subservice_id") int subservice_id,
            @Field("task_date") String task_date,
            @Field("task_location") String task_location,
            @Field("task_instruction") String task_instruction,
            @Field("latitude") double latitude,
            @Field("longitude") double longitude,
            @Field("promo_id")int promo_id,
            @Field("task_type") String task_type);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> checkTransactionHandyman(
            @Field("key") String key,
            @Field("customer_id") int customer_id,
            @Field("ti_id") int ti_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> hmTracker(
            @Field("key") String key,
            @Field("customer_id") int customer_id,
            @Field("ti_id") int ti_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> confirmationTracker(
            @Field("key") String key,
            @Field("handyman_id") int handyman_id,
            @Field("ti_id") int ti_id,
            @Field("warranty_id") int warranty_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> setHandymanArrival(
            @Field("key") String key,
            @Field("customer_id") int customer_id,
            @Field("ti_id") int ti_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> cancelHandymanArrival(
            @Field("key") String key,
            @Field("customer_id") int customer_id,
            @Field("ti_id") int ti_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> setHandymanCompletion(
            @Field("key") String key,
            @Field("customer_id") int customer_id,
            @Field("ti_id") int ti_id,
            @Field("warranty_id") int warranty_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> cancelHandymanCompletion(
            @Field("key") String key,
            @Field("customer_id") int customer_id,
            @Field("ti_id") int ti_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> updateArrival(
            @Field("key") String key,
            @Field("handyman_id") int handyman_id,
            @Field("ti_id") int ti_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> updateCompletion(
            @Field("key") String key,
            @Field("handyman_id") int handyman_id,
            @Field("ti_id") int ti_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> checkAvaialableTransaction(
            @Field("key") String key,
            @Field("handyman_id") int handyman_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> checkTransactionAvailability(
            @Field("key") String key,
            @Field("ti_id") int ti_id,
            @Field("user_id") int user_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> rescheduleTransaction(
            @Field("key") String key,
            @Field("ti_id") int ti_id,
            @Field("date") String date,
            @Field("user_id") int user_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> checkRescheduledTransaction(
            @Field("key") String key,
            @Field("user_id") int user_id,
            @Field("ti_id") int ti_id,
            @Field("rt_id") int rt_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> updateRescheduledStatus(
            @Field("key") String key,
            @Field("rt_id") int rt_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> deleteRescheduledStatus(
            @Field("key") String key,
            @Field("rt_id") int rt_id,
            @Field("ti_id") int ti_id,
            @Field("user_id") int user_id,
            @Field("transaction_id") int transaction_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> cancelHandyman(
            @Field("key") String key,
            @Field("customer_id") int customer_id,
            @Field("handyman_id") int handyman_id,
            @Field("ti_id") int ti_id,
            @Field("reason_id") int reason_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<List<Promos>> checkAvailablePromos(
            @Field("key") String key,
            @Field("user_id") int user_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<User> fetchHandymanProfile(
            @Field("key") String key,
            @Field("user_id") int user_id,
            @Field("value") String value);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> handymanAcceptTransaction(
            @Field("key") String key,
            @Field("ti_id") int ti_id,
            @Field("handyman_id") int handyman_id,
            @Field("customer_id") int customer_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<List<Reasons>> checkReasons(
            @Field("key") String key);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> deletePendingTransaction(
            @Field("key") String key,
            @Field("ti_id") int ti_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<List<TransactionInfo>> fetchTransactions(
            @Field("key") String key,
            @Field("filter") String filter,
            @Field("user_id") String user_id,
            @Field("user_role") String user_role);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> fetchTransactionInfo(
            @Field("key") String key,
            @Field("transaction_id") int transaction_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<List<TransactionInfo>> fetchTransactionsList(
            @Field("key") String key,
            @Field("user_id") String user_id,
            @Field("user_role") String user_role);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<List<Warranty>> fetchWarrantyList(
            @Field("key") String key,
            @Field("user_id") String user_id,
            @Field("user_role") String user_role);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<List<RateAndReview>> fetchRateAndReview(
            @Field("key") String key,
            @Field("user_id") String user_id,
            @Field("user_role") String user_role);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<List<ApplicationReport>> fetchApplicationReports(
            @Field("key") String key,
            @Field("user_id") String user_id,
            @Field("user_role") String user_role);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<Warranty> requestWarranty(
            @Field("key") String key,
            @Field("warranty_id") int warranty_id,
            @Field("instruction") String instruction);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<TransactionInfo> checkTransaction(
            @Field("key") String key,
            @Field("user_id") int user_id,
            @Field("user_role") String user_role);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<ApplicationReport> sendReport(
            @Field("key") String key,
            @Field("report") String report,
            @Field("user_id") int user_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<List<Points>> fetchPoints(
            @Field("key") String key,
            @Field("user_id") String user_id,
            @Field("user_role") String user_role);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<List<Conversations>> fetchConversations(
            @Field("key") String key,
            @Field("user_id") String user_id,
            @Field("user_role") String user_role);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<List<Messages>> fetchMessages(
            @Field("key") String key,
            @Field("conversation_id") String conversation_id,
            @Field("user_id") int user_id,
            @Field("user_role") String user_role);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<List<Warranty>> getAllPendingWarranty(
            @Field("key") String key,
            @Field("user_id") int user_id,
            @Field("user_role") String user_role);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<List<TransactionInfo>> getAllRescheduledService(
            @Field("key") String key,
            @Field("user_id") int user_id,
            @Field("user_role") String user_role);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<User> updateAvailability(
            @Field("key") String key,
            @Field("user_id") int user_id,
            @Field("user_role") String user_role,
            @Field("conversation_id") int conversation_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<Messages> sendMessage(
            @Field("key") String key,
            @Field("messages") String messages,
            @Field("email") String email,
            @Field("user_role") String user_role,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<Points> fetchTotalPoints(
            @Field("key") String key,
            @Field("user_id") String user_id,
            @Field("user_role") String user_role);

    @FormUrlEncoded
    @POST("android_api_transactions.php")
    Call<RateAndReview> rate(
            @Field("key") String key,
            @Field("rate") double rate,
            @Field("reviews") String reviews,
            @Field("rater_id") int rater_id,
            @Field("user_id") int user_id,
            @Field("transaction_id") int transaction_id);
}
