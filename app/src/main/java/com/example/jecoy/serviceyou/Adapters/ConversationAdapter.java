package com.example.jecoy.serviceyou.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jecoy.serviceyou.Class.ApplicationReport;
import com.example.jecoy.serviceyou.Class.Conversations;
import com.example.jecoy.serviceyou.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ConversationAdapter extends RecyclerView.Adapter<ConversationAdapter.RecyclerViewAdapter> {
    List<Conversations> conversations;
    Context context;
    conversationClickListener conversationClickListener;

    public ConversationAdapter(List<Conversations> conversations, Context context, ConversationAdapter.conversationClickListener conversationClickListener) {
        this.conversations = conversations;
        this.context = context;
        this.conversationClickListener = conversationClickListener;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listofconverstation,viewGroup,false);
        return new RecyclerViewAdapter(view,conversationClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter recyclerViewAdapter, int i) {
        Conversations conversation = conversations.get(i);
        recyclerViewAdapter.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation));
        recyclerViewAdapter.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));

        recyclerViewAdapter.convo_partner_email.setText(conversation.getConversation_partner());
        Glide.with(context)
                .asBitmap()
                .load(conversation.getConversation_picture())
                .into(recyclerViewAdapter.image_view);
    }

    @Override
    public int getItemCount() {
        return conversations.size();
    }

    public class RecyclerViewAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircleImageView image_view;
        TextView convo_partner_email;
        RelativeLayout container;
        conversationClickListener conversationClickListener;
        public RecyclerViewAdapter(@NonNull View itemView,conversationClickListener conversationClickListener) {
            super(itemView);
            image_view = itemView.findViewById(R.id.image_view);
            convo_partner_email = itemView.findViewById(R.id.convo_partner_email);
            container = itemView.findViewById(R.id.container);
            this.conversationClickListener = conversationClickListener;
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            conversationClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public interface conversationClickListener{
        void onItemClick(View view, int position);
    }
}
