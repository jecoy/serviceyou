package com.example.jecoy.serviceyou.Adapters;

import android.app.Service;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jecoy.serviceyou.Class.Services;
import com.example.jecoy.serviceyou.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.RecyclerViewAdapter> {
    List<Services> services;
    Context context;
    serviceClickListener serviceClickListener;

    public ServiceAdapter(List<Services> services, Context context, ServiceAdapter.serviceClickListener serviceClickListener) {
        this.services = services;
        this.context = context;
        this.serviceClickListener = serviceClickListener;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listofservice, parent, false);
        return new RecyclerViewAdapter(view,serviceClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter holder, final int position) {
        Services service = services.get(position);
        holder.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation));
        holder.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));
        Glide.with(context)
                .asBitmap()
                .load(service.getService_icon())
                .into(holder.service_icon);
        holder.service_name.setText(service.getService());
    }

    @Override
    public int getItemCount() {
        return services.size();
    }

    class RecyclerViewAdapter extends RecyclerView.ViewHolder implements View.OnClickListener{
        CircleImageView service_icon;
        TextView service_name;
        serviceClickListener serviceClickListener;
        RelativeLayout container;

         RecyclerViewAdapter(@NonNull View itemView,serviceClickListener serviceClickListener) {
            super(itemView);

            service_icon = itemView.findViewById(R.id.image_view);
            service_name = itemView.findViewById(R.id.name);
            container = itemView.findViewById(R.id.container);

            this.serviceClickListener = serviceClickListener;
            service_icon.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            serviceClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public interface serviceClickListener{
        void onItemClick(View view,int position);
    }
}
