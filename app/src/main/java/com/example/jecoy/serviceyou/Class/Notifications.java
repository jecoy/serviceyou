package com.example.jecoy.serviceyou.Class;

import com.google.gson.annotations.SerializedName;

public class Notifications {
    @SerializedName("notif_id")
    private int notif_id;
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("notif_subj")
    private String notif_subj;
    @SerializedName("notif_date")
    private String notif_date;
    @SerializedName("notif_content")
    private String notif_content;
    @SerializedName("notif_isSeen")
    private boolean notif_isSeen;
    @SerializedName("notif_isActive")
    private boolean notif_isActive;

    @SerializedName("message")
    private String message;
    @SerializedName("value")
    private String value;

    public String getMessage() {
        return message;
    }

    public String getValue() {
        return value;
    }

    public int getNotif_id() {
        return notif_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getNotif_subj() {
        return notif_subj;
    }

    public String getNotif_date() {
        return notif_date;
    }

    public String getNotif_content() {
        return notif_content;
    }

    public boolean isNotif_isSeen() {
        return notif_isSeen;
    }

    public boolean isNotif_isActive() {
        return notif_isActive;
    }
}
