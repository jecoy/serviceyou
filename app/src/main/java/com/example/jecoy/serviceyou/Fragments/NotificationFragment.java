package com.example.jecoy.serviceyou.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jecoy.serviceyou.Adapters.NotificationAdapter;
import com.example.jecoy.serviceyou.Adapters.PointsAdapter;
import com.example.jecoy.serviceyou.Api.ApiInterface;
import com.example.jecoy.serviceyou.Api.RetrofitClient;
import com.example.jecoy.serviceyou.Class.LoginUser;
import com.example.jecoy.serviceyou.Class.Notifications;
import com.example.jecoy.serviceyou.Class.Points;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Storage.SharedPrefManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFragment extends Fragment {
    View v;
    LoginUser user;
    int user_id;
    double total_points;
    String user_role;
    RecyclerView recyclerView;
    ApiInterface apiInterface;
    TextView currentPoints;
    List<Notifications> notificationsList;
    NotificationAdapter notificationAdapter;
    NotificationAdapter.notificationClickListener notificationClickListener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        user = SharedPrefManager.getInstance(this.getActivity().getApplicationContext()).getUser();
        user_id = user.getUser_id();
        user_role = user.getUser_role();
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        v = inflater.inflate(R.layout.fragment_notification, container, false);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        currentPoints = v.findViewById(R.id.currentPoints);
        fetchNotifications("fetchNotifications",user_id);
        notificationClickListener = new NotificationAdapter.notificationClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                return;
            }
        };
    }

    private void fetchNotifications(String key, int user_id) {
        Call<List<Notifications>> call = apiInterface.fetchNotifications(key,user_id+"");
        call.enqueue(new Callback<List<Notifications>>() {
            @Override
            public void onResponse(Call<List<Notifications>> call, Response<List<Notifications>> response) {
                notificationsList = response.body();
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView = v.findViewById(R.id.recyclerViewNotif);
                recyclerView.setLayoutManager(layoutManager);
                notificationAdapter = new NotificationAdapter(notificationsList,getActivity(), notificationClickListener);
                recyclerView.setAdapter(notificationAdapter);
                notificationAdapter.notifyDataSetChanged();
            }
            @Override
            public void onFailure(Call<List<Notifications>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }
}
