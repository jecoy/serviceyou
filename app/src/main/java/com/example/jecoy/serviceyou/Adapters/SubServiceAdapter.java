package com.example.jecoy.serviceyou.Adapters;

import android.app.Service;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jecoy.serviceyou.Class.Services;
import com.example.jecoy.serviceyou.Class.SubServices;
import com.example.jecoy.serviceyou.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SubServiceAdapter extends RecyclerView.Adapter<SubServiceAdapter.RecyclerViewAdapter> {
    List<SubServices> subServices;
    Context context;
    subServiceClickListener subServiceClickListener;

    public SubServiceAdapter(List<SubServices> subServices, Context context, SubServiceAdapter.subServiceClickListener subServiceClickListener) {
        this.subServices = subServices;
        this.context = context;
        this.subServiceClickListener = subServiceClickListener;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listofservice, parent, false);
        return new RecyclerViewAdapter(view,subServiceClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter holder, final int position) {
        SubServices subService = subServices.get(position);
        holder.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation));
        holder.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));
        Glide.with(context)
                .asBitmap()
                .load(subService.getSubservice_icon())
                .into(holder.service_icon);
        holder.service_name.setText(subService.getSubservice());
    }

    @Override
    public int getItemCount() {
        return subServices.size();
    }

    class RecyclerViewAdapter extends RecyclerView.ViewHolder implements View.OnClickListener{
        CircleImageView service_icon;
        TextView service_name;
        subServiceClickListener subServiceClickListener;
        RelativeLayout container;

        RecyclerViewAdapter(@NonNull View itemView,subServiceClickListener serviceClickListener) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            service_icon = itemView.findViewById(R.id.image_view);
            service_name = itemView.findViewById(R.id.name);

            this.subServiceClickListener = serviceClickListener;
            service_icon.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            subServiceClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public interface subServiceClickListener{
        void onItemClick(View view,int position);
    }
}
