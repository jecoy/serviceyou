package com.example.jecoy.serviceyou;

public interface LockerInterface {
    public void lockDrawer();
    public void unlockDrawer();
}
