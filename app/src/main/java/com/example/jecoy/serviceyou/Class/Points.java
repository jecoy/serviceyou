package com.example.jecoy.serviceyou.Class;

import com.google.gson.annotations.SerializedName;

public class Points {
    @SerializedName("points_update_id")
    private int points_update_id;
    @SerializedName("user_id")
    private int user_id;

    @SerializedName("points_amount")
    private double points_amount;
    @SerializedName("points_update_date")
    private String points_update_date;
    @SerializedName("points_update_type")
    private String points_update_type;
    @SerializedName("points_update_status")
    private String points_update_status;


    @SerializedName("value")
    private String value;
    @SerializedName("message")
    private String message;
    @SerializedName("total_points")
    private double total_points;

    public int getPoints_update_id() {
        return points_update_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public double getPoints_amount() {
        return points_amount;
    }

    public String getPoints_update_date() {
        return points_update_date;
    }

    public String getPoints_update_type() {
        return points_update_type;
    }

    public String getPoints_update_status() {
        return points_update_status;
    }

    public String getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }

    public double getTotal_points() {
        return total_points;
    }
}
