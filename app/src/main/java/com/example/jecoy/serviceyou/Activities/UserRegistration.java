package com.example.jecoy.serviceyou.Activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.jecoy.serviceyou.Api.ApiInterface;
import com.example.jecoy.serviceyou.Api.RetrofitClient;
import com.example.jecoy.serviceyou.Class.User;
import com.example.jecoy.serviceyou.EmailPackage.SendMail;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Api.RetrofitClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRegistration extends AppCompatActivity {
    EditText emailAddReg,passReg,conPassReg,firstNameReg,lastNameReg,mobileNumReg,addReg;
    String user_fname,user_lname,confirmpass,user_pass,user_email,user_number,user_add,user_pic,user_idPic,verication_code,email,message,subject;
    CardView cardViewRegisterBtn,cardViewCamera,cardViewFile;
    RelativeLayout loginTxtView;
    ProgressBar regProgressBar;
    boolean imageView = false;
    ImageView imageViewID,regUserPhoto;
    private static final int SELECT_PICTURE = 1;
    private String selectedImagePath;
    private ApiInterface apiInterface;
    ProgressDialog progressDialog;
    Dialog myDialog;

    Bitmap bitmap,bitmap2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration);
        progressDialog = new ProgressDialog(this);
        myDialog = new Dialog(this);
        varInitialize();

        cardViewRegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postData("insert");
            }
        });

        imageViewID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(imageView == true)
                {
                    imageView = false;
                }
                openImageChoser();;
            }
        });

        loginTxtView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(UserRegistration.this, LoginActivity.class);
                startActivity(loginIntent);
            }
        });

        regUserPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView = true;
                openImageChoser();
            }
        });


    }

    private void openImageChoser() {
        myDialog.setContentView(R.layout.openimagepopup);
        cardViewCamera =  myDialog.findViewById(R.id.cardViewCamera);
        cardViewFile = myDialog.findViewById(R.id.cardViewFile);
        cardViewCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImage();
                myDialog.dismiss();
            }
        });
        cardViewFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseFile();
                myDialog.dismiss();
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }

    private void chooseFile() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), SELECT_PICTURE);
    }

    private void captureImage(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,0);
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE && data != null && data.getData() != null) {
                Uri selectedImageUri = data.getData();
                try {
                    if(imageView == false)
                    {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImageUri);
                        imageViewID.setImageBitmap(bitmap);
                    }
                    else
                    {
                        bitmap2 = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImageUri);
                        regUserPhoto.setImageBitmap(bitmap2);
                    }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            else{
                if(imageView == false)
                {
                    bitmap = (Bitmap) data.getExtras().get("data");
                    imageViewID.setImageBitmap(bitmap);
                }
                else
                {
                    bitmap2 = (Bitmap) data.getExtras().get("data");
                    regUserPhoto.setImageBitmap(bitmap2);

                }
            }
        }
    }

    private void postData(String key) {
        varTrapped();
        if(varTrapped()){

            progressDialog.setMessage("Saving...");
            progressDialog.show();

            if (bitmap == null) {
                user_pic = "";
            } else {
                user_pic = getStringImage(bitmap2);
            }
            if (bitmap2 == null) {
                user_idPic = "";
            } else {
                user_idPic = getStringImage(bitmap);
            }

            float vc = Float.parseFloat(user_number)/156246;
            verication_code = "SY-"+(int)vc+"";

            apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);

            Call<User> call = apiInterface.addUser(key,user_fname,user_lname,user_add,user_number,user_email,user_pass,user_pic,user_idPic,verication_code);

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {

                    progressDialog.dismiss();

                    Log.i(UserRegistration.class.getSimpleName(), response.toString());

                    String value = response.body().getValue();
                    String messages = response.body().getMassage();
                    String user_id = response.body().getUser_id()+"";

                    if (value.equals("1")) {

                        email = user_email;
                        subject = "VERIFICATION CODE FOR SERVICE.YOU";
                        message = "Hi "+user_fname+" "+user_lname+" your verification code for service you is "+verication_code;
                        SendMail sm = new SendMail(UserRegistration.this,email,subject,message);
                        sm.execute();

                        Intent verificationIntent = new Intent(UserRegistration.this, EmailVerificationActivity.class);
                        verificationIntent.putExtra("user_id",user_id);
                        startActivity(verificationIntent);

                    } else {
                        Toast.makeText(UserRegistration.this, messages, Toast.LENGTH_SHORT).show();
                    }

                }
                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(UserRegistration.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void varInitialize(){
        cardViewRegisterBtn = findViewById(R.id.cardViewRegisterBtn);
        loginTxtView = findViewById(R.id.loginTxtView);
        imageViewID = findViewById(R.id.imageViewID);
        regUserPhoto = findViewById(R.id.regUserPhoto);
        emailAddReg = findViewById(R.id.emailAddReg);
        passReg = findViewById(R.id.passReg);
        conPassReg = findViewById(R.id.conPassReg);
        firstNameReg = findViewById(R.id.firstNameReg);
        lastNameReg = findViewById(R.id.lastNameReg);
        mobileNumReg = findViewById(R.id.mobileNumReg);
        addReg = findViewById(R.id.addReg);
        regProgressBar = findViewById(R.id.progressBarUserReg);
    }

    private boolean varTrapped() {

        imageViewID = findViewById(R.id.imageViewID);
        regUserPhoto = findViewById(R.id.regUserPhoto);

        user_email = emailAddReg.getText().toString().trim();
        user_pass = passReg.getText().toString().trim();
        confirmpass = conPassReg.getText().toString().trim();
        user_fname = firstNameReg.getText().toString().trim();
        user_lname = lastNameReg.getText().toString().trim();
        user_number = mobileNumReg.getText().toString().trim();
        user_add = addReg.getText().toString().trim();
        if (user_email.isEmpty()) {
            emailAddReg.setError("Email is required");
            emailAddReg.requestFocus();
            return false;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(user_email).matches()) {
            emailAddReg.setError("Enter a valid email");
            emailAddReg.requestFocus();
            return false;
        }

        if (user_pass.isEmpty()) {
            passReg.setError("Password required");
            passReg.requestFocus();
            return false;
        }

        if (user_pass.length() < 6) {
            passReg.setError("Password should be atleast 6 character long");
            passReg.requestFocus();
            return false;
        }

        if (!user_pass.equals(confirmpass)) {
            passReg.setError("Password should match");
            passReg.requestFocus();
            return false;
        }

        if (user_fname.isEmpty()) {
            firstNameReg.setError("Firstname required");
            firstNameReg.requestFocus();
            return false;
        }

        if (user_lname.isEmpty()) {
            lastNameReg.setError("Lastname required");
            lastNameReg.requestFocus();
            return false;
        }

        if (user_number.isEmpty()) {
            mobileNumReg.setError("Number required");
            mobileNumReg.requestFocus();
            return false;
        }

        if (user_add.isEmpty()) {
            addReg.setError("Address required");
            addReg.requestFocus();
            return false;
        }

        return true;
    }

}
