package com.example.jecoy.serviceyou.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.jecoy.serviceyou.Class.TransactionInfo;
import com.example.jecoy.serviceyou.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.RecyclerViewAdapter> {
    List<TransactionInfo> transactionInfos;
    Context context;
    transactionInfoClickListener transactionInfoClickListener;

    public TransactionsAdapter(List<TransactionInfo> transactionInfos, Context context, TransactionsAdapter.transactionInfoClickListener transactionInfoClickListener) {
        this.transactionInfos = transactionInfos;
        this.context = context;
        this.transactionInfoClickListener = transactionInfoClickListener;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listoftransactions,viewGroup,false);
        return new RecyclerViewAdapter(view,transactionInfoClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter recyclerViewAdapter, int i) {
        TransactionInfo transactionInfo = transactionInfos.get(i);
        recyclerViewAdapter.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation));
        recyclerViewAdapter.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));
        recyclerViewAdapter.transactionID.setText("SY-"+transactionInfo.getTransaction_id());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date currentTimeDate=null;
        try {
            currentTimeDate = sdf.parse(transactionInfo.getTransaction_date());
        } catch (ParseException ignored) {

        }
        sdf.applyPattern("MMMM dd,yyyy");

        recyclerViewAdapter.transactionDate.setText(sdf.format(currentTimeDate));
        recyclerViewAdapter.transactionEarnings.setText(transactionInfo.getTransaction_earnings()+"");
    }

    @Override
    public int getItemCount() {
        return transactionInfos.size();
    }

    public class RecyclerViewAdapter extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView transactionID,transactionEarnings,transactionDate;
        transactionInfoClickListener transactionInfoClickListeners;
        RelativeLayout container;

        public RecyclerViewAdapter(@NonNull View itemView,transactionInfoClickListener transactionInfoClickListener) {
            super(itemView);
            transactionID = itemView.findViewById(R.id.transactionID);
            transactionEarnings = itemView.findViewById(R.id.transactionSubservice);
            transactionDate = itemView.findViewById(R.id.transactionDate);
            container = itemView.findViewById(R.id.container);
            this.transactionInfoClickListeners = transactionInfoClickListener;
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            transactionInfoClickListeners.onItemClick(v, getAdapterPosition());
        }
    }

    public interface transactionInfoClickListener{
        void onItemClick(View view,int position);
    }
}
