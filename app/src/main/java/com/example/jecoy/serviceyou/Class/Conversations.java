package com.example.jecoy.serviceyou.Class;

import com.google.gson.annotations.SerializedName;

public class Conversations {
    @SerializedName("conversation_id")
    private int conversation_id;

    @SerializedName("conversation_date")
    private String conversation_date;
    @SerializedName("conversation_status")
    private String conversation_status;
    @SerializedName("conversation_isActive")
    private boolean conversation_isActive;

    @SerializedName("conversation_partner")
    private String conversation_partner;
    @SerializedName("conversation_picture")
    private String conversation_picture;

    @SerializedName("message")
    private String message;
    @SerializedName("value")
    private String value;

    public int getConversation_id() {
        return conversation_id;
    }

    public String getConversation_date() {
        return conversation_date;
    }

    public String getConversation_status() {
        return conversation_status;
    }

    public boolean isConversation_isActive() {
        return conversation_isActive;
    }

    public String getConversation_partner() {
        return conversation_partner;
    }

    public String getConversation_picture() {
        return conversation_picture;
    }

    public String getMessage() {
        return message;
    }

    public String getValue() {
        return value;
    }
}
