package com.example.jecoy.serviceyou.Class;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.jecoy.serviceyou.Activities.HomeActivity;
import com.example.jecoy.serviceyou.Api.ApiInterface;
import com.example.jecoy.serviceyou.Api.RetrofitClient;
import com.example.jecoy.serviceyou.Fragments.MessageFragment;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Storage.SharedPrefManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gd185082 on 2/1/2019.
 */

public class MessageService extends Service {
    LoginUser user;
    public static final long NOTIFY_INTERVAL = 10 * 1000; // 10 seconds

    ApiInterface apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();
    // timer handling
    private Timer mTimer = null;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        user = SharedPrefManager.getInstance(getApplicationContext()).getUser();

        // cancel if already existed
        if (mTimer != null) {
            mTimer.cancel();
        } else {
            // recreate new
            mTimer = new Timer();
        }
        // schedule task
        mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, NOTIFY_INTERVAL);
    }

    class TimeDisplayTimerTask extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    Call<List<Messages>> call = apiInterface.getAllUnreadMessages("getMessages",user.getUser_id());
                    call.enqueue(new Callback<List<Messages>>() {
                        @RequiresApi(api = Build.VERSION_CODES.O)
                        @Override
                        public void onResponse(Call<List<Messages>> call, Response<List<Messages>> response) {
                            if(response.body()!=null) {
                                int notifyId = 0;
                                for (Messages a : response.body()) {

                                    Intent mainIntent = new Intent(getApplicationContext(), HomeActivity.class);
                                    int conversation_id = a.getConversation_id();
                                    mainIntent.putExtra("conversation_id",conversation_id+"");

                                    PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(),conversation_id,mainIntent,PendingIntent.FLAG_UPDATE_CURRENT);

                                    Notification.BigTextStyle bigTextStyle = new Notification.BigTextStyle();
                                    bigTextStyle.bigText(a.getMessage() );

                                    bigTextStyle.setBigContentTitle(a.getMessage_sender());

                                    Notification.Builder builder = new Notification.Builder(getApplicationContext());
                                    builder.setSmallIcon(R.drawable.serviceyouicon)
                                            .setContentTitle(a.getMessage_sender())
                                            .setContentText(a.getMessage())
                                            .setWhen(System.currentTimeMillis())
                                            .setAutoCancel(false)
                                            .setContentIntent(contentIntent)
                                            .setPriority(Notification.PRIORITY_MAX)
                                            .setStyle(bigTextStyle)
                                            .setDefaults(Notification.DEFAULT_ALL);

                                    NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                    mNotificationManager.notify(notifyId++, builder.build());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<List<Messages>> call, Throwable t) {

                        }
                    });
                }

            });
        }
    }
}
