package com.example.jecoy.serviceyou.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.jecoy.serviceyou.Class.Reasons;
import com.example.jecoy.serviceyou.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ReasonsAdapter extends RecyclerView.Adapter<ReasonsAdapter.RecyclewViewAdapter> {
    List<Reasons> reasons;
    Context context;
    reasonsClickListener _reasonsClickListner;

    public ReasonsAdapter(List<Reasons> reasons, Context context, reasonsClickListener _reasonsClickListner) {
        this.reasons = reasons;
        this.context = context;
        this._reasonsClickListner = _reasonsClickListner;
    }

    @NonNull
    @Override
    public ReasonsAdapter.RecyclewViewAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listofreasons, viewGroup, false);
        return new RecyclewViewAdapter(view,_reasonsClickListner);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclewViewAdapter holder, int position) {
        Reasons reason = reasons.get(position);
        holder.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation));
        holder.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));
        holder.reason.setText(reason.getReason());
    }

    @Override
    public int getItemCount() {
        return reasons.size();
    }

    public class RecyclewViewAdapter extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView reason;
        reasonsClickListener reasonsClickListener;
        RelativeLayout container;

        public RecyclewViewAdapter(@NonNull View itemView,ReasonsAdapter.reasonsClickListener _promosClickListener) {
            super(itemView);
            reason = itemView.findViewById(R.id.reason);
            container = itemView.findViewById(R.id.container);

            this.reasonsClickListener = _promosClickListener;
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            reasonsClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public interface reasonsClickListener{
        void onItemClick(View view, int position);
    }
}
