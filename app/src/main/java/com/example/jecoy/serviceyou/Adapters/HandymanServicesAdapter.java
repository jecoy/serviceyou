package com.example.jecoy.serviceyou.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jecoy.serviceyou.Class.HandymanServices;
import com.example.jecoy.serviceyou.Class.Services;
import com.example.jecoy.serviceyou.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class HandymanServicesAdapter extends RecyclerView.Adapter<HandymanServicesAdapter.RecyclerViewAdapter> {
    List<HandymanServices> HandymanServices;
    Context context;
    hmServiceClickListener hmServiceClickListener;

    public HandymanServicesAdapter(List<com.example.jecoy.serviceyou.Class.HandymanServices> handymanServices, Context context, HandymanServicesAdapter.hmServiceClickListener hmServiceClickListener) {
        this.HandymanServices = handymanServices;
        this.context = context;
        this.hmServiceClickListener = hmServiceClickListener;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listofhandymanservice, parent, false);
        return new RecyclerViewAdapter(view,hmServiceClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter holder, int position) {
        HandymanServices handymanService = HandymanServices.get(position);
        holder.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation));
        holder.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));
        Glide.with(context)
                .asBitmap()
                .load(handymanService.getSubservice_icon())
                .into(holder.service_icon);
        holder.servicename.setText(handymanService.getService());
        holder.subservicename.setText(handymanService.getSubservice());
        holder.subserviceexpertise.setText(handymanService.getService_levelofexp());
        holder.subservicerate.setText(handymanService.getSubservice_rate()+"");
    }

    @Override
    public int getItemCount() {
        return HandymanServices.size();
    }

    class RecyclerViewAdapter extends RecyclerView.ViewHolder implements View.OnClickListener{
        CircleImageView service_icon;
        TextView servicename;
        TextView subservicename;
        TextView subservicerate;
        TextView subserviceexpertise;
        hmServiceClickListener hmServiceClickListener;
        RelativeLayout container;

        RecyclerViewAdapter(@NonNull View itemView,hmServiceClickListener hmServiceClickListener) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            service_icon = itemView.findViewById(R.id.image_view);
            servicename = itemView.findViewById(R.id.servicename);
            subservicename = itemView.findViewById(R.id.subservicename);
            subservicerate = itemView.findViewById(R.id.subservicerate);
            subserviceexpertise = itemView.findViewById(R.id.subserviceexpertise);

            this.hmServiceClickListener = hmServiceClickListener;
            service_icon.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            hmServiceClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public interface hmServiceClickListener{
        void onItemClick(View view, int position);
    }

}
