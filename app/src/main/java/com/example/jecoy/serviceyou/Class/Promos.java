package com.example.jecoy.serviceyou.Class;

import com.google.gson.annotations.SerializedName;

public class Promos {
    @SerializedName("promo_id")
    private int promo_id;
    @SerializedName("promo_name")
    private String promo_name;

    @SerializedName("promo_code")
    private String promo_code;

    @SerializedName("promo_type")
    private String promo_type;

    @SerializedName("promo_amount")
    private double promo_amount;

    @SerializedName("promo_limit")
    private String promo_limit;

    @SerializedName("promo_price")
    private int promo_price;

    @SerializedName("promo_dateCreated")
    private String promo_dateCreated;

    @SerializedName("promo_dateExpire")
    private String promo_dateExpire;

    @SerializedName("promo_status")
    private String promo_status;

    public int getPromo_id() {
        return promo_id;
    }

    public String getPromo_name() {
        return promo_name;
    }

    public String getPromo_code() {
        return promo_code;
    }

    public String getPromo_type() {
        return promo_type;
    }

    public double getPromo_amount() {
        return promo_amount;
    }

    public String getPromo_limit() {
        return promo_limit;
    }

    public int getPromo_price() {
        return promo_price;
    }

    public String getPromo_dateCreated() {
        return promo_dateCreated;
    }

    public String getPromo_dateExpire() {
        return promo_dateExpire;
    }

    public String getPromo_status() {
        return promo_status;
    }
}
