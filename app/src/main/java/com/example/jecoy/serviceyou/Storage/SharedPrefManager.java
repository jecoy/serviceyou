package com.example.jecoy.serviceyou.Storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.jecoy.serviceyou.Class.LoginUser;
import com.example.jecoy.serviceyou.Class.User;


public class SharedPrefManager {

    private static final String SHARED_PREF_NAME = "my_shared_preff";

    private static SharedPrefManager mInstance;
    private Context mCtx;

    private SharedPrefManager(Context mCtx) {
        this.mCtx = mCtx;
    }


    public static synchronized SharedPrefManager getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(mCtx);
        }
        return mInstance;
    }


    public void saveUser(int user_id,String user_email,String user_fname, String user_lname,String user_role,String user_status,String user_pic,String user_address,String user_number,String user_description,String user_workDescription,String hm_efficieny,String user_badge,String user_idPic) {

        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt("user_id", user_id);
        editor.putString("user_email", user_email);
        editor.putString("user_fname", user_fname);
        editor.putString("user_lname", user_lname);
        editor.putString("user_role", user_role);
        editor.putString("user_status", user_status);
        editor.putString("user_pic",user_pic);
        editor.putString("user_address", user_address);
        editor.putString("user_number",user_number);
        editor.putString("user_description", user_description);
        editor.putString("user_workDescription", user_workDescription);
        editor.putString("hm_efficieny",hm_efficieny);
        editor.putString("user_badge", user_badge);
        editor.putString("user_idPic",user_idPic);

        editor.apply();

    }

    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt("user_id", -1) != -1;
    }

   public LoginUser getUser() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new LoginUser(
                sharedPreferences.getInt("user_id", -1),
                sharedPreferences.getString("user_email", null),
                sharedPreferences.getString("user_fname", null),
                sharedPreferences.getString("user_lname", null),
                sharedPreferences.getString("user_role", null),
                sharedPreferences.getString("user_status", null),
                sharedPreferences.getString("user_pic", null),
                sharedPreferences.getString("user_address", null),
                sharedPreferences.getString("user_number", null),
                sharedPreferences.getString("user_description", null),
                sharedPreferences.getString("user_workDescription", null),
                sharedPreferences.getString("hm_efficieny", null),
                sharedPreferences.getString("user_badge", null),
                sharedPreferences.getString("user_idPic", null)
        );
    }

    public void clear() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

}

