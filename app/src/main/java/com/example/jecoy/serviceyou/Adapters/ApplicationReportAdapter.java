package com.example.jecoy.serviceyou.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jecoy.serviceyou.Class.ApplicationReport;
import com.example.jecoy.serviceyou.Class.RateAndReview;
import com.example.jecoy.serviceyou.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ApplicationReportAdapter extends RecyclerView.Adapter<ApplicationReportAdapter.RecyclerViewAdapter> {
    List<ApplicationReport> applicationReports;
    Context context;
    applicationReportClickListener applicationReportClickListener;

    public ApplicationReportAdapter(List<ApplicationReport> applicationReports, Context context, ApplicationReportAdapter.applicationReportClickListener applicationReportClickListener) {
        this.applicationReports = applicationReports;
        this.context = context;
        this.applicationReportClickListener = applicationReportClickListener;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listofapplicationreport,viewGroup,false);
        return new RecyclerViewAdapter(view,applicationReportClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter recyclerViewAdapter, int i) {
        ApplicationReport applicationReport = applicationReports.get(i);
        recyclerViewAdapter.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation));
        recyclerViewAdapter.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));

        recyclerViewAdapter.reporter_email.setText(applicationReport.getUser_email());
        recyclerViewAdapter.report.setText(applicationReport.getReport());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date currentTimeDate=null;
        try {
            currentTimeDate = sdf.parse(applicationReport.getReport_date());
        } catch (ParseException ignored) {

        }
        sdf.applyPattern("MMMM dd,yyyy");


        recyclerViewAdapter.report_date.setText(sdf.format(currentTimeDate));
        Glide.with(context)
                .asBitmap()
                .load(applicationReport.getUser_pic())
                .into(recyclerViewAdapter.image_view);
    }

    @Override
    public int getItemCount() {
        return applicationReports.size();
    }

    public class RecyclerViewAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircleImageView image_view;
        TextView reporter_email,report_date,report;
        RelativeLayout container;
        applicationReportClickListener applicationReportClickListener;

        public RecyclerViewAdapter(@NonNull View itemView, applicationReportClickListener applicationReportClickListener) {
            super(itemView);
            image_view = itemView.findViewById(R.id.image_view);
            reporter_email = itemView.findViewById(R.id.reporter_email);
            report_date = itemView.findViewById(R.id.report_date);
            report = itemView.findViewById(R.id.report);
            container = itemView.findViewById(R.id.container);
            this.applicationReportClickListener = applicationReportClickListener;
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            applicationReportClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public interface applicationReportClickListener{
        void onItemClick(View view, int position);
    }
}
