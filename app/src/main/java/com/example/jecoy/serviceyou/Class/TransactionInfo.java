package com.example.jecoy.serviceyou.Class;

import com.google.gson.annotations.SerializedName;

public class TransactionInfo {
    @SerializedName("customer_id")
    private int customer_id;
    @SerializedName("handyman_id")
    private int handyman_id;
    @SerializedName("ti_id")
    private int ti_id;
    @SerializedName("service_id")
    private int service_id;
    @SerializedName("rt_id")
    private int rt_id;
    @SerializedName("subservice_id")
    private int subservice_id;
    @SerializedName("transaction_id")
    private int transaction_id;


    @SerializedName("latitude")
    private double latitude;
    @SerializedName("longitude")
    private double longitude;

    @SerializedName("task_date")
    private String task_date;
    @SerializedName("task_location")
    private String task_location;
    @SerializedName("task_instruction")
    private String task_instruction;
    @SerializedName("task_status")
    private String task_status;
    @SerializedName("promo_code")
    private String promo_code;
    @SerializedName("subservice")
    private String subservice;
    @SerializedName("task_type")
    private String task_type;
    @SerializedName("rt_proposed_date")
    private String rt_proposed_date;
    @SerializedName("rt_date")
    private String rt_date;

    @SerializedName("ti_isActive")
    private boolean ti_isActive;

    @SerializedName("message")
    private String message;
    @SerializedName("value")
    private String value;
    @SerializedName("user_fname")
    private String user_fname;
    @SerializedName("user_lname")
    private String user_lname;


    @SerializedName("promo_id")
    private int promo_id;
    @SerializedName("conversation_id")
    private int conversation_id;
    @SerializedName("transaction_earnings")
    private double transaction_earnings;
    @SerializedName("transaction_commission")
    private double transaction_commission;
    @SerializedName("transaction_date")
    private String transaction_date;
    @SerializedName("handyman_name")
    private String handyman_name;
    @SerializedName("customer_name")
    private String customer_name;
    @SerializedName("user_email")
    private String user_email;

    public String getUser_email() {
        return user_email;
    }

    public int getConversation_id() {
        return conversation_id;
    }

    public double getTransaction_commission() {
        return transaction_commission;
    }

    public String getHandyman_name() {
        return handyman_name;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public int getPromo_id() {
        return promo_id;
    }

    public double getTransaction_earnings() {
        return transaction_earnings;
    }

    public String getTransaction_date() {
        return transaction_date;
    }

    public String getUser_fname() {
        return user_fname;
    }

    public String getUser_lname() {
        return user_lname;
    }

    public String getRt_date() {
        return rt_date;
    }

    public int getTransaction_id() {
        return transaction_id;
    }

    public int getRt_id() {
        return rt_id;
    }
    public String getRt_proposed_date() {
        return rt_proposed_date;
    }
    public String getTask_type() {
        return task_type;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getPromo_code() {
        return promo_code;
    }

    public String getSubservice() {
        return subservice;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public int getHandyman_id() {
        return handyman_id;
    }

    public int getTi_id() {
        return ti_id;
    }

    public int getService_id() {
        return service_id;
    }

    public int getSubservice_id() {
        return subservice_id;
    }

    public String getTask_date() {
        return task_date;
    }

    public String getTask_location() {
        return task_location;
    }

    public String getTask_instruction() {
        return task_instruction;
    }

    public String getTask_status() {
        return task_status;
    }

    public boolean isTi_isActive() {
        return ti_isActive;
    }

    public String getMessage() {
        return message;
    }

    public String getValue() {
        return value;
    }
}
