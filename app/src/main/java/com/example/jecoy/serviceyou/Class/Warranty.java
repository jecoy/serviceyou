package com.example.jecoy.serviceyou.Class;

import com.google.gson.annotations.SerializedName;

public class Warranty {
    @SerializedName("warranty_id")
    private int warranty_id;
    @SerializedName("transaction_id")
    private int transaction_id;
    @SerializedName("warrantyInfo_id")
    private int warrantyInfo_id;

    @SerializedName("warranty_status")
    private String warranty_status;
    @SerializedName("warranty_expiration")
    private String warranty_expiration;
    @SerializedName("warranty_date")
    private String warranty_date;
    @SerializedName("warranty_instruction")
    private String warranty_instruction;

    @SerializedName("changes_isSeen_byCust")
    private String changes_isSeen_byCust;
    @SerializedName("changes_isSeen_byHandy")
    private String changes_isSeen_byHandy;

    @SerializedName("value")
    private String value;
    @SerializedName("message")
    private String message;


    public String getChanges_isSeen_byCust() {
        return changes_isSeen_byCust;
    }

    public String getChanges_isSeen_byHandy() {
        return changes_isSeen_byHandy;
    }


    public String getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }

    public int getWarranty_id() {
        return warranty_id;
    }

    public int getTransaction_id() {
        return transaction_id;
    }

    public int getWarrantyInfo_id() {
        return warrantyInfo_id;
    }

    public String getWarranty_status() {
        return warranty_status;
    }

    public String getWarranty_expiration() {
        return warranty_expiration;
    }

    public String getWarranty_date() {
        return warranty_date;
    }

    public String getWarranty_instruction() {
        return warranty_instruction;
    }
}
