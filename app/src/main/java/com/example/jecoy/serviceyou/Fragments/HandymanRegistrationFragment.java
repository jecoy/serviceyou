package com.example.jecoy.serviceyou.Fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jecoy.serviceyou.Activities.EmailVerificationActivity;
import com.example.jecoy.serviceyou.Activities.HandymanRegistration;
import com.example.jecoy.serviceyou.Activities.UserRegistration;
import com.example.jecoy.serviceyou.Adapters.HandymanDocumentsAdapter;
import com.example.jecoy.serviceyou.Adapters.HandymanServicesAdapter;
import com.example.jecoy.serviceyou.Adapters.ServiceAdapter;
import com.example.jecoy.serviceyou.Adapters.SubServiceAdapter;
import com.example.jecoy.serviceyou.Api.ApiInterface;
import com.example.jecoy.serviceyou.Api.RetrofitClient;
import com.example.jecoy.serviceyou.Class.HandymanDocs;
import com.example.jecoy.serviceyou.Class.HandymanServices;
import com.example.jecoy.serviceyou.Class.Services;
import com.example.jecoy.serviceyou.Class.SubServices;
import com.example.jecoy.serviceyou.Class.User;
import com.example.jecoy.serviceyou.EmailPackage.SendMail;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Storage.SharedPrefManager;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.libraries.places.api.Places;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HandymanRegistrationFragment extends Fragment {
    EditText quickPitch,workLocRegHM,workLocRadiusRegHM,emailAddRegHM,passRegHM,conPassRegHM,firstNameRegHM,lastNameRegHM,mobileNumRegHM,addressRegHM,aboutyou,aboutyourwork,_docuName,_docuInfo;
    String user_fname,user_lname,confirmpass,user_pass,user_email,user_number,user_add,user_pic,verication_code,email,message,subject;
    String user_description,user_workDescription,user_DOB,hms_location,hms_radius,user_idFromActivity,_subservice;
    CardView cardViewDocsPic,cardViewRegisterBtn,cardViewCamera,cardViewFile,cardViewAddSubService,cardViewCancelSubService,_cardViewAddDocs,_cardViewCancel;

    FloatingActionButton nextBtn,addBtn,doneBtn;

    ProgressBar regProgressBar;
    DatePickerDialog datePickerDialog;

    //FOR 2nd REGISTRATION PART OF HANDYMAN
    int value,sub_id,serv_id;
    double sub_rate;
    TextView subname,subrate;
    Spinner levelofexp;

    //DIALOGS
    Dialog myDialog,imageDialog;
    ProgressDialog progressDialog;

    //FOR IMAGE CAPTURING AND SELECTING
    private static final int SELECT_PICTURE = 1;
    Bitmap bitmap;
    CircleImageView regUserPhoto;
    ImageView _handydocsimage;

    //LIST
    private List<Services> servicesList;
    private List<SubServices> subServicesList;
    private List<HandymanServices> hmServicesList;
    private List<HandymanDocs> hmDocsList;

    //FOR RECYCLEWVIEW & ADAPTER
    ServiceAdapter.serviceClickListener serviceClickListener;
    ServiceAdapter serviceAdapter;
    SubServiceAdapter.subServiceClickListener subServiceClickListener;
    SubServiceAdapter subServiceAdapter;
    HandymanServicesAdapter.hmServiceClickListener hmServiceClickListener;
    HandymanServicesAdapter handymanServicesAdapter;
    HandymanDocumentsAdapter handymanDocumentsAdapter;
    HandymanDocumentsAdapter.hmDocsListener hmDocsListener;
    private RecyclerView recyclerView;

    //VIEW AND APIINTERFACE
    ApiInterface apiInterface;
    View v;

    //BOOLEAN TO IDENTIFY IF IT IS THE LAST REGISTRATION OF THE HANDYMAN
    Boolean lastFragment = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myDialog = new Dialog(getActivity());
        imageDialog = new Dialog(getActivity());
        value = this.getArguments().getInt("value");
        if (value != 0) {
            value = this.getArguments().getInt("value");
            user_idFromActivity = getArguments().getString("user_id");

            if(value==1){
                return inflateFirstRegistration(inflater,container,savedInstanceState);
            }

            else if((!user_idFromActivity.equals(""))&&value==2||value==5){
                serviceClickListener = ((view, position) -> {
                    int id = servicesList.get(position).getService_id();
                    getSubService("selecSub",id);
                });

                subServiceClickListener = new SubServiceAdapter.subServiceClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        sub_id = subServicesList.get(position).getSubservice_id();
                        serv_id = subServicesList.get(position).getService_id();
                        _subservice = subServicesList.get(position).getSubservice();
                        sub_rate = subServicesList.get(position).getSubservice_rate();

                        checkHandymanService("checkHmService",sub_id,user_idFromActivity,serv_id);

                    }
                };

                return inflateSecondRegistration(inflater,container,savedInstanceState);
            }
            else{
                return inflateThirdRegistration(inflater,container,savedInstanceState);
            }
        }
        else{
            return null;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if(value==2||value==5){
            nextBtn = v.findViewById(R.id.nextBtn);
            if(value==2){
                nextBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent verificationIntent = new Intent(getActivity(), HandymanRegistration.class);
                        verificationIntent.putExtra("user_id",user_idFromActivity);
                        verificationIntent.putExtra("value",3);
                        verificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(verificationIntent);
                    }
                });
            }
            else{
                nextBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent verificationIntent = new Intent(getActivity(), HandymanRegistration.class);
                        verificationIntent.putExtra("user_id",user_idFromActivity);
                        verificationIntent.putExtra("value",7);
                        verificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(verificationIntent);
                    }
                });
            }
        }

        if(value==3||value==4){
            addBtn = v.findViewById(R.id.addBtn);
            doneBtn = v.findViewById(R.id.doneBtn);
            addBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addDocDialog();
                }
            });
            if(value==3){
                doneBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent verificationIntent = new Intent(getActivity(), HandymanRegistration.class);
                        verificationIntent.putExtra("user_id",user_idFromActivity);
                        verificationIntent.putExtra("value",8);
                        verificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(verificationIntent);
                    }
                });
            }
            else{
                doneBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent verificationIntent = new Intent(getActivity(), HandymanRegistration.class);
                        verificationIntent.putExtra("user_id",user_idFromActivity);
                        verificationIntent.putExtra("value",7);
                        verificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(verificationIntent);
                    }
                });
            }
        }

        hmServiceClickListener = new HandymanServicesAdapter.hmServiceClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                return;
            }
        };

        hmDocsListener = new HandymanDocumentsAdapter.hmDocsListener() {
            @Override
            public void onItemClick(View view, int position) {
                return;
            }
        };
    }

    //CODE FOR INFLATING THIRD CONTAINER WHICH IS FOR THIRD HANDYMAN REGISTRATION SCREEN
    private View inflateThirdRegistration(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_handyman_third_registration, container, false);
        lastFragment = true;
        getHandymanDocs();

        return v;
    }
    //CODE FOR GETTING THE HANDYMAN DOCUMENTS
    private void getHandymanDocs(){
        String key = "selectHandymanDocs";
        int user_id = Integer.parseInt(user_idFromActivity);
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        Call<List<HandymanDocs>> call = apiInterface.getHandymanDocs(key,user_id);
        call.enqueue(new Callback<List<HandymanDocs>>() {
            @Override
            public void onResponse(Call<List<HandymanDocs>> call, Response<List<HandymanDocs>> response) {
                hmDocsList = response.body();

                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView = v.findViewById(R.id.recyclerViewDocuments);
                recyclerView.setLayoutManager(layoutManager);
                handymanDocumentsAdapter = new HandymanDocumentsAdapter(hmDocsList,getActivity(), hmDocsListener);
                recyclerView.setAdapter(handymanDocumentsAdapter);

                handymanDocumentsAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<HandymanDocs>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    //CODE FOR SHOWING DIALOG FOR ADDING DOCUMENTS
    private void addDocDialog() {
        myDialog.setContentView(R.layout.handyman_adddocs);
        initializeMyDialog(myDialog,2);

        _cardViewAddDocs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String __docuName,__docuInfo,_docuPic;
                __docuName = _docuName.getText().toString().trim();
                __docuInfo = _docuInfo.getText().toString().trim();

                if(bitmap==null){
                    _docuName.setError("Please input document image.");
                    if(__docuName.equals(""))
                    {
                        _docuName.setError("Please input document name.");
                        if (__docuInfo.equals("")) {

                            _docuInfo.setError("Please input document info.");
                        }
                    }
                    return;
                }
                else {
                    _docuPic = getStringImage(bitmap);
                }

                addHandymanDocs("insertHandymanDocs",user_idFromActivity,_docuPic,__docuName,__docuInfo);
            }
        });
        _cardViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });

        cardViewDocsPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImageChoser();
            }
        });


        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }
    //CODE FOR ADDING HANDYMAN DOCS
    private void addHandymanDocs(String key, String _user_id, String docu_pic, String docu_name, String docu_info) {
        progressDialog = ProgressDialog.show(getActivity(), "", "Saving....", true);
        progressDialog.show();

        int user_id = Integer.parseInt(_user_id);
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        Call<HandymanDocs> call = apiInterface.addHandymanDocs(key,user_id,docu_pic,docu_name,docu_info);

        call.enqueue(new Callback<HandymanDocs>() {
            @Override
            public void onResponse(Call<HandymanDocs> call, Response<HandymanDocs> response) {
                String value = response.body().getValue();
                String message = response.body().getMessage();

                if(value.equals("1")){
                    getHandymanDocs();
                    myDialog.dismiss();
                    progressDialog.dismiss();
                }
                else{
                    progressDialog = ProgressDialog.show(getActivity(), "ERROR", ""+response.body().getMessage(), true);
                    progressDialog.show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            progressDialog.dismiss();
                        }
                    }, 5000);
                }
            }

            @Override
            public void onFailure(Call<HandymanDocs> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }



    //CODE FOR INFLATING SECOND CONTAINER WHICH IS FOR SECOND HANDYMAN REGISTRATION SCREEN
    private View inflateSecondRegistration(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_handyman_second_registration, container, false);
        getServices();
        getHandymanServices();

        return v;
    }
    //GETTING THE HANDYMAN SERVICES OFFERED
    private void getHandymanServices() {
        String key = "selectHandymanServices";
        int user_id = Integer.parseInt(user_idFromActivity);
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        Call<List<HandymanServices>> call = apiInterface.getHandymanServices(key,user_id);
        call.enqueue(new Callback<List<HandymanServices>>() {
            @Override
            public void onResponse(Call<List<HandymanServices>> call, Response<List<HandymanServices>> response) {
                hmServicesList = response.body();

                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView = v.findViewById(R.id.recyclerViewHandymanServices);
                recyclerView.setLayoutManager(layoutManager);
                handymanServicesAdapter = new HandymanServicesAdapter(hmServicesList,getActivity(), hmServiceClickListener);
                recyclerView.setAdapter(handymanServicesAdapter);

                handymanServicesAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<HandymanServices>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    //CODE FOR GETTING THE AVAILABLE SERVICES
    private void getServices(){
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        Call<List<Services>> call = apiInterface.getServices();
        call.enqueue(new Callback<List<Services>>() {
            @Override
            public void onResponse(Call<List<Services>> call, Response<List<Services>> response) {
                servicesList = response.body();

                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                recyclerView = v.findViewById(R.id.recyclerViewServices);
                recyclerView.setLayoutManager(layoutManager);
                serviceAdapter = new ServiceAdapter(servicesList,getActivity(), serviceClickListener);
                recyclerView.setAdapter(serviceAdapter);

                serviceAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Services>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }
    //CODE FOR CHECKING IF THE SERVICE IS ALREADY ADDED AND IF NOT WILL DISPLAY THE SERVICE INFO
    private void checkHandymanService(String key, int subservice_id,String _user_id,int _service_id) {
        int user_id = Integer.parseInt(_user_id);
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        Call<HandymanServices> call = apiInterface.checkHandymanServices(key,subservice_id,user_id);

        call.enqueue(new Callback<HandymanServices>() {
            @Override
            public void onResponse(Call<HandymanServices> call, Response<HandymanServices> response) {
                if(response.body().getValue().equals("1"))
                {
                    progressDialog = ProgressDialog.show(getActivity(), "ERROR", "You've added that service already.", true);
                    progressDialog.show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            progressDialog.dismiss();
                        }
                    }, 1000);
                }
                else
                {
                    myDialog.setContentView(R.layout.handyman_addsubservice);
                    initializeMyDialog(myDialog,1);
                    passValueToDialog(_subservice,sub_rate);
                    cardViewAddSubService.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String _levelOfExp,_quickPitch;

                            _levelOfExp = levelofexp.getSelectedItem().toString();
                            _quickPitch = quickPitch.getText().toString().trim();
                            addHandymanSubService("addHandymanService",sub_id,_service_id,user_idFromActivity,_levelOfExp,_quickPitch);

                            myDialog.dismiss();
                        }
                    });
                    cardViewCancelSubService.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            myDialog.dismiss();
                        }
                    });

                    myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    myDialog.show();
                }
            }

            @Override
            public void onFailure(Call<HandymanServices> call, Throwable t) {

            }
        });

    }
    // CODE FOR ADDING SERVICES OF THE HANDYMAN
    private void addHandymanSubService(String key, int subservice_id, int service_id, String _user_id, String service_levelofexp, String service_quickpitch) {
        int user_id = Integer.parseInt(_user_id);
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        Call<HandymanServices> call = apiInterface.addHandymanServices(key,subservice_id,service_id,user_id,service_levelofexp,service_quickpitch);
        call.enqueue(new Callback<HandymanServices>() {
            @Override
            public void onResponse(Call<HandymanServices> call, Response<HandymanServices> response) {
                if(response.body().getValue().equals("1")){

                    getHandymanServices();
                }
                else{
                    progressDialog = ProgressDialog.show(getActivity(), "ERROR", ""+response.body().getMessage(), true);
                    progressDialog.show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            progressDialog.dismiss();
                        }
                    }, 10000);
                }

            }

            @Override
            public void onFailure(Call<HandymanServices> call, Throwable t) {

            }
        });
    }
    //PASSING VALUE TO DIALOG FROM RECYCLEVIEW OF SUBSERVICE
    private void passValueToDialog(String subservice, double sub_rate) {
        subname.setText(subservice);
        subrate.setText(sub_rate+"");
    }
    //INITIALIZING MYDIALOG ELEMENTS
    private void initializeMyDialog(Dialog myDialog,int key) {
        if(key == 1){
            quickPitch = myDialog.findViewById(R.id.quickPitch);
            subname = myDialog.findViewById(R.id.subname);
            subrate = myDialog.findViewById(R.id.subrate);
            levelofexp = myDialog.findViewById(R.id.levelofexp);
            cardViewAddSubService = myDialog.findViewById(R.id.cardViewAddSubService);
            cardViewCancelSubService = myDialog.findViewById(R.id.cardViewCancelSubService);
            setAdapter();
        }
        else{
            _handydocsimage = myDialog.findViewById(R.id.handydocsimage);
            _docuName = myDialog.findViewById(R.id.docuName);
            _docuInfo =  myDialog.findViewById(R.id.docuInfo);
            _cardViewAddDocs = myDialog.findViewById(R.id.cardViewAddDocs);
            _cardViewCancel = myDialog.findViewById(R.id.cardViewCancel);
            cardViewDocsPic = myDialog.findViewById(R.id.cardViewDocsPic);
        }
    }
    //SETTING ADAPTER FOR THE SPINNER
    private void setAdapter() {
        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("Fresher");
        spinnerArray.add("Mid-Level");
        spinnerArray.add("Expert");
        spinnerArray.add("Highly Skilled");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        levelofexp.setAdapter(adapter);
    }
    //GETTING AVAILABLE SUBSERVICE OF A CERTAIN SERVICE
    private void getSubService(String key, int service_id) {
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        Call<List<SubServices>> call = apiInterface.getSubServices(key,service_id);

        call.enqueue(new Callback<List<SubServices>>() {
            @Override
            public void onResponse(Call<List<SubServices>> call, Response<List<SubServices>> response) {
                subServicesList = response.body();

                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                recyclerView = v.findViewById(R.id.recyclerViewSubService);
                recyclerView.setLayoutManager(layoutManager);
                subServiceAdapter = new SubServiceAdapter(subServicesList,getActivity(), subServiceClickListener);
                recyclerView.setAdapter(subServiceAdapter);

                subServiceAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<SubServices>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }



    //CODE FOR INFLATING FIRST CONTAINER WHICH IS FOR FIRST HANDYMAN REGISTRATION SCREEN
    private View inflateFirstRegistration(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
        v = inflater.inflate(R.layout.fragment_handyman_registration, container, false);
        varInitialize();
        cardViewRegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postData("insertHandyman");
            }
        });
        regUserPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImageChoser();
            }
        });

        return v;
    }
    //CODE FOR INSERTING DATA OF HANDYMAN TO DATABASE
    private void postData(String key) {

        varTrapped();
        if(varTrapped()){

            progressDialog.setMessage("Saving...");
            progressDialog.show();

            if (bitmap == null) {
                user_pic = "";
            } else {
                user_pic = getStringImage(bitmap);
            }

            float vc = Float.parseFloat(user_number)/156246;
            verication_code = "SY-"+(int)vc+"";

            apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);

            Call<User> call = apiInterface.addHandyman(key,user_fname,user_lname,user_add,user_number,user_email,user_pass,user_pic,verication_code,user_description,user_workDescription,hms_location);

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {

                    progressDialog.dismiss();

                    Log.i(UserRegistration.class.getSimpleName(), response.toString());

                    String value = response.body().getValue();
                    String messages = response.body().getMassage();
                    String user_id = response.body().getUser_id()+"";

                    if (value.equals("1")) {

                        email = user_email;
                        subject = "VERIFICATION CODE FOR SERVICE.YOU";
                        message = "Hi "+user_fname+" "+user_lname+" your verification code for service you is "+verication_code;
                        SendMail sm = new SendMail(getActivity(),email,subject,message);
                        sm.execute();

                        Intent verificationIntent = new Intent(getActivity(), HandymanRegistration.class);
                        verificationIntent.putExtra("user_id",user_id);
                        verificationIntent.putExtra("value",2);
                        verificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(verificationIntent);

                    } else {
                        Toast.makeText(getActivity(), messages, Toast.LENGTH_SHORT).show();
                    }

                }
                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }
    //INITIALIZING THE XML ELEMENTS
    private void varInitialize(){
        regUserPhoto = v.findViewById(R.id.regUserPhoto);
        cardViewRegisterBtn = v.findViewById(R.id.cardViewRegisterBtn);
        cardViewRegisterBtn = v.findViewById(R.id.cardViewRegisterBtn);
        aboutyou = v.findViewById(R.id.aboutyou);
        aboutyourwork = v.findViewById(R.id.aboutyourwork);
        emailAddRegHM = v.findViewById(R.id.emailAddRegHM);
        passRegHM = v.findViewById(R.id.passRegHM);
        conPassRegHM = v.findViewById(R.id.conPassRegHM);
        firstNameRegHM = v.findViewById(R.id.firstNameRegHM);
        lastNameRegHM = v.findViewById(R.id.lastNameRegHM);
        mobileNumRegHM = v.findViewById(R.id.mobileNumRegHM);
        addressRegHM = v.findViewById(R.id.addressRegHM);
        workLocRegHM = v.findViewById(R.id.workLocRegHM);
        regProgressBar = v.findViewById(R.id.progressBarHandyReg);
        progressDialog = new ProgressDialog(this.getActivity());

    }
    //TRAPPING THE XML ELEMENTS VALUES
    private boolean varTrapped() {

        regUserPhoto = v.findViewById(R.id.regUserPhoto);

        user_email = emailAddRegHM.getText().toString().trim();
        user_pass = passRegHM.getText().toString().trim();
        confirmpass = conPassRegHM.getText().toString().trim();
        user_fname = firstNameRegHM.getText().toString().trim();
        user_lname = lastNameRegHM.getText().toString().trim();
        user_number = mobileNumRegHM.getText().toString().trim();
        user_add = addressRegHM.getText().toString().trim();
        user_description = aboutyou.getText().toString().trim();
        user_workDescription = aboutyourwork.getText().toString().trim();
        hms_location = workLocRegHM.getText().toString().trim();

        if (user_email.isEmpty()) {
            emailAddRegHM.setError("Email is required");
            emailAddRegHM.requestFocus();
            return false;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(user_email).matches()) {
            emailAddRegHM.setError("Enter a valid email");
            emailAddRegHM.requestFocus();
            return false;
        }

        if (user_pass.isEmpty()) {
            passRegHM.setError("Password required");
            passRegHM.requestFocus();
            return false;
        }

        if (user_pass.length() < 6) {
            passRegHM.setError("Password should be atleast 6 character long");
            passRegHM.requestFocus();
            return false;
        }

        if (!user_pass.equals(confirmpass)) {
            conPassRegHM.setError("Password should match");
            conPassRegHM.requestFocus();
            return false;
        }

        if (user_fname.isEmpty()) {
            firstNameRegHM.setError("Firstname required");
            firstNameRegHM.requestFocus();
            return false;
        }

        if (user_lname.isEmpty()) {
            lastNameRegHM.setError("Lastname required");
            lastNameRegHM.requestFocus();
            return false;
        }

        if (user_number.isEmpty()) {
            mobileNumRegHM.setError("Number required");
            mobileNumRegHM.requestFocus();
            return false;
        }

        if (user_add.isEmpty()) {
            addressRegHM.setError("Address required");
            addressRegHM.requestFocus();
            return false;
        }

        if (user_description.isEmpty()) {
            aboutyou.setError("User description required");
            aboutyou.requestFocus();
            return false;
        }

        if (user_workDescription.isEmpty()) {
            aboutyourwork.setError("Working Description required");
            aboutyourwork.requestFocus();
            return false;
        }

        if (hms_location.isEmpty()) {
            workLocRegHM.setError("Working location required");
            workLocRegHM.requestFocus();
            return false;
        }

        return true;
    }


    //FOR IMAGE CHOOSING OR CAPTURING OF THE HANDYMAN
    private void openImageChoser() {
        imageDialog.setContentView(R.layout.openimagepopup);
        cardViewCamera =  imageDialog.findViewById(R.id.cardViewCamera);
        cardViewFile = imageDialog.findViewById(R.id.cardViewFile);
        imageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        imageDialog.show();

        cardViewCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImage();
                imageDialog.dismiss();
            }
        });
        cardViewFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseFile();
                imageDialog.dismiss();
            }
        });
    }

    private void chooseFile() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
    }

    private void captureImage(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,0);
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PICTURE && data != null && data.getData() != null) {
                Uri selectedImageUri = data.getData();

                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getApplicationContext().getContentResolver(), selectedImageUri);
                    if(!lastFragment){
                        regUserPhoto.setImageBitmap(bitmap);
                    }
                    else{
                        _handydocsimage.setImageBitmap(bitmap);
                    }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            else{
                bitmap = (Bitmap) data.getExtras().get("data");
                if(!lastFragment){
                    regUserPhoto.setImageBitmap(bitmap);
                }
                else{
                    _handydocsimage.setImageBitmap(bitmap);
                }
            }
        }
    }

}
