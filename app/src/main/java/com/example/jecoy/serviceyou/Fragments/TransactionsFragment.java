package com.example.jecoy.serviceyou.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jecoy.serviceyou.Adapters.PromoAdapter;
import com.example.jecoy.serviceyou.Adapters.TransactionInfoAdapter;
import com.example.jecoy.serviceyou.Adapters.TransactionsAdapter;
import com.example.jecoy.serviceyou.Api.ApiInterface;
import com.example.jecoy.serviceyou.Api.RetrofitClient;
import com.example.jecoy.serviceyou.Class.LoginUser;
import com.example.jecoy.serviceyou.Class.Points;
import com.example.jecoy.serviceyou.Class.TransactionInfo;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Storage.SharedPrefManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransactionsFragment extends Fragment {
    View v;
    LoginUser user;
    int user_id;
    String user_role;
    RecyclerView recyclerView;
    ApiInterface apiInterface;
    TransactionsAdapter transactionsAdapter;
    TransactionsAdapter.transactionInfoClickListener transactionInfoClickListener;
    List<TransactionInfo> transactionInfoList;
    Dialog transactionInfo;
    TextView transactionID,transactionDate,transactionEarning,transactionCommission,promoCode,service,taskDate,taskLocation,handyman,customer,instruction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        user = SharedPrefManager.getInstance(this.getActivity().getApplicationContext()).getUser();
        user_id = user.getUser_id();
        user_role = user.getUser_role();
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        v = inflater.inflate(R.layout.fragment_transactions, container, false);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        fetchTransactions("fetchTransactionsUpdate",user_id,user_role);
        transactionInfo = new Dialog(getActivity());

        transactionInfoClickListener = new TransactionsAdapter.transactionInfoClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                int transaction_id = transactionInfoList.get(position).getTransaction_id();
                fetchTransactionInfo("fetchTransactionInfo",user_id,user_role,transaction_id);
            }
        };
    }

    private void fetchTransactionInfo(String key, int user_id, String user_role,int transaction_id) {
        Call<TransactionInfo> call = apiInterface.fetchTransactionInfo(key,transaction_id);
        call.enqueue(new Callback<TransactionInfo>() {
            @Override
            public void onResponse(Call<TransactionInfo> call, Response<TransactionInfo> response) {
                if(response.body()!=null){
                    String value = response.body().getValue();
                    if(value.equals("1")){
                        int transaction_id = response.body().getTransaction_id();
                        String transaction_date = response.body().getTransaction_date();
                        double transaction_earning = response.body().getTransaction_earnings();
                        double transaction_commission = response.body().getTransaction_commission();
                        String service = response.body().getSubservice();
                        String task_date = response.body().getTask_date();
                        String task_location = response.body().getTask_location();
                        String task_instruction = response.body().getTask_instruction();
                        String customer_name = response.body().getCustomer_name();
                        String handyman_name = response.body().getHandyman_name();
                        String promo = response.body().getPromo_code();
                        passValueToDialog(transaction_id,transaction_date,transaction_earning,transaction_commission,service,task_date,task_location,task_instruction,customer_name,handyman_name,promo);
                    }
                    else{
                        return;
                    }
                }
                else{
                    return;
                }
            }

            @Override
            public void onFailure(Call<TransactionInfo> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void passValueToDialog(int transaction_id, String transaction_date, double transaction_earning, double transaction_commission, String subservice, String task_date, String task_location, String task_instruction, String customer_name, String handyman_name, String promo) {
        transactionInfo.setContentView(R.layout.completedtransactioninfopopup);
        transactionInfo.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        initializeElements(transactionInfo);

        transactionID.setText(transaction_id+"");
        transactionDate.setText(transaction_date);
        transactionEarning.setText(transaction_earning+"");
        transactionCommission.setText(transaction_commission+"");
        if (promo==null) {
            promoCode.setText("NONE");
        }
        else{
            promoCode.setText(promo);
        }
        service.setText(subservice);
        taskDate.setText(task_date);
        taskLocation.setText(task_location);
        handyman.setText(handyman_name);
        customer.setText(customer_name);
        instruction.setText(task_instruction);
        transactionInfo.show();
    }

    private void initializeElements(Dialog transactionInfo) {
        transactionID = transactionInfo.findViewById(R.id.transactionID);
        transactionDate = transactionInfo.findViewById(R.id.transactionDate);
        transactionEarning = transactionInfo.findViewById(R.id.transactionEarnings);
        transactionCommission = transactionInfo.findViewById(R.id.transactionCommission);
        promoCode = transactionInfo.findViewById(R.id.promoCode);
        service = transactionInfo.findViewById(R.id.service);
        taskDate = transactionInfo.findViewById(R.id.taskDate);
        taskLocation = transactionInfo.findViewById(R.id.taskLocation);
        handyman = transactionInfo.findViewById(R.id.handyman);
        customer = transactionInfo.findViewById(R.id.customer);
        instruction = transactionInfo.findViewById(R.id.instruction);
    }

    private void fetchTransactions(String key, int user_id, String user_role) {
        Call<List<TransactionInfo>> call = apiInterface.fetchTransactionsList(key,user_id+"",user_role);
        call.enqueue(new Callback<List<TransactionInfo>>() {
            @Override
            public void onResponse(Call<List<TransactionInfo>> call, Response<List<TransactionInfo>> response) {
                transactionInfoList = response.body();
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView = v.findViewById(R.id.recyclerViewTransactions);
                recyclerView.setLayoutManager(layoutManager);
                transactionsAdapter = new TransactionsAdapter(transactionInfoList,getActivity(), transactionInfoClickListener);
                recyclerView.setAdapter(transactionsAdapter);
                transactionsAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<TransactionInfo>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}
