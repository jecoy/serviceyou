package com.example.jecoy.serviceyou.Class;

import com.google.gson.annotations.SerializedName;

public class Messages {
    @SerializedName("message_id")
    private int message_id;
    @SerializedName("conversation_id")
    private int conversation_id;
    @SerializedName("recepient_id")
    private int recepient_id;
    @SerializedName("sender_id")
    private int sender_id;

    @SerializedName("message")
    private String message;
    @SerializedName("date")
    private String date;
    @SerializedName("message_status")
    private String message_status;
    @SerializedName("message_sender")
    private String message_sender;
    @SerializedName("message_isActive")
    private boolean message_isActive;

    @SerializedName("value")
    private String value;
    @SerializedName("messages")
    private boolean messages;

    public String getMessage_sender() {
        return message_sender;
    }

    public int getMessage_id() {
        return message_id;
    }

    public int getConversation_id() {
        return conversation_id;
    }

    public int getRecepient_id() {
        return recepient_id;
    }

    public int getSender_id() {
        return sender_id;
    }

    public String getMessage() {
        return message;
    }

    public String getDate() {
        return date;
    }

    public String getMessage_status() {
        return message_status;
    }

    public boolean isMessage_isActive() {
        return message_isActive;
    }

    public String getValue() {
        return value;
    }

    public boolean isMessages() {
        return messages;
    }
}
