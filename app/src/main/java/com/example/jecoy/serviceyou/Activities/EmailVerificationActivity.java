package com.example.jecoy.serviceyou.Activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.example.jecoy.serviceyou.Api.ApiInterface;
import com.example.jecoy.serviceyou.Api.RetrofitClient;
import com.example.jecoy.serviceyou.Class.User;
import com.example.jecoy.serviceyou.EmailPackage.SendMail;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Storage.SharedPrefManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmailVerificationActivity extends AppCompatActivity {
    Dialog myDialog;
    EditText vcEditText;
    String verification_code,user_id;
    private ApiInterface apiInterface;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_verification);
        myDialog = new Dialog(this);
        vcEditText = findViewById(R.id.vcEditText);
        progressDialog = new ProgressDialog(this);

        Bundle registerData = getIntent().getExtras();
        if(registerData!=null){
            user_id = registerData.getString("user_id");
        }
        else{
            Toast.makeText(EmailVerificationActivity.this,"NO ID PASS!",Toast.LENGTH_LONG);
        }

    }

    public void ShowPopup(View v) {
        verification_code = vcEditText.getText().toString().trim();
        if (verification_code.isEmpty()) {
            vcEditText.setError("Please fill out empty fields.");
            vcEditText.requestFocus();
            return;
        }
        else{
            verifyCode("select");
        }
    }

    private void verifyCode(String key) {
        progressDialog.setMessage("Checking...");
        progressDialog.show();

        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        Call<User> call = apiInterface.checkVerification(key,user_id,verification_code);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                progressDialog.dismiss();

                Log.i(EmailVerificationActivity.class.getSimpleName(), response.toString());

                String value = response.body().getValue();
                String messages = response.body().getMassage();
                String user_email = response.body().getUser_email();
                String user_fname = response.body().getUser_fname();
                String user_lname = response.body().getUser_lname();
                String user_role = response.body().getUser_role();
                String user_status = response.body().getUser_status();
                String user_pic = response.body().getUser_pic();
                String user_address = response.body().getUser_add();
                String user_number = response.body().getUser_number();

                String user_description = response.body().getUser_description();
                String user_workDescription = response.body().getUser_workDescription();
                double hm_efficieny = response.body().getHm_efficiency();
                String user_badge = response.body().getUser_badge();
                String user_idPic = response.body().getUser_idPic();

                if (value.equals("1")) {

                    SharedPrefManager.getInstance(EmailVerificationActivity.this)
                            .saveUser(Integer.parseInt(user_id),user_email,user_fname,user_lname,
                                    user_role,user_status,user_pic,user_address,user_number,user_description,user_workDescription,hm_efficieny+"",user_badge,user_idPic);

                    Intent verificationIntent = new Intent(EmailVerificationActivity.this,HomeActivity.class);
                    verificationIntent.putExtra("user_id",user_id);
                    verificationIntent.putExtra("user_role",user_role);
                    verificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(verificationIntent);

                } else {
                    Toast.makeText(EmailVerificationActivity.this, messages, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(EmailVerificationActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent loginIntent = new Intent(EmailVerificationActivity.this, LoginActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
        super.onBackPressed();
    }
}
