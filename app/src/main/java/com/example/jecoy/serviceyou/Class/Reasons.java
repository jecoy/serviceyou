package com.example.jecoy.serviceyou.Class;

import com.google.gson.annotations.SerializedName;

public class Reasons {
    @SerializedName("reasons_id")
    private int reasons_id;
    @SerializedName("reason")
    private String reason;

    public int getReasons_id() {
        return reasons_id;
    }

    public String getReason() {
        return reason;
    }
}
