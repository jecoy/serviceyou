package com.example.jecoy.serviceyou.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.jecoy.serviceyou.Class.RateAndReview;
import com.example.jecoy.serviceyou.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RateAndReviewAdapter extends RecyclerView.Adapter<RateAndReviewAdapter.RecyclerViewAdapter> {
    List<RateAndReview> rateAndReviewList;
    Context context;
    rateAndReviewClickListener rateAndReviewClickListener;

    public RateAndReviewAdapter(List<RateAndReview> rateAndReviewList, Context context, RateAndReviewAdapter.rateAndReviewClickListener rateAndReviewClickListener) {
        this.rateAndReviewList = rateAndReviewList;
        this.context = context;
        this.rateAndReviewClickListener = rateAndReviewClickListener;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listofrateandreview,viewGroup,false);
        return new RecyclerViewAdapter(view,rateAndReviewClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter recyclerViewAdapter, int i) {
        RateAndReview rateAndReview = rateAndReviewList.get(i);
        recyclerViewAdapter.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_transition_animation));
        recyclerViewAdapter.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));

        recyclerViewAdapter.rater_email.setText(rateAndReview.getRater_email());
        recyclerViewAdapter.reviews.setText(rateAndReview.getReviews());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date currentTimeDate=null;
        try {
            currentTimeDate = sdf.parse(rateAndReview.getRating_date());
        } catch (ParseException ignored) {

        }
        sdf.applyPattern("MMMM dd,yyyy");


        recyclerViewAdapter.rating_date.setText(sdf.format(currentTimeDate));
        recyclerViewAdapter.rate.setRating((float)rateAndReview.getRatings());
        Glide.with(context)
                .asBitmap()
                .load(rateAndReview.getUser_pic())
                .into(recyclerViewAdapter.image_view);
    }

    @Override
    public int getItemCount() {
        return rateAndReviewList.size();
    }

    public class RecyclerViewAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircleImageView image_view;
        TextView rater_email,rating_date,reviews;
        RatingBar rate;
        RelativeLayout container;
        rateAndReviewClickListener rateAndReviewClickListener;

        public RecyclerViewAdapter(@NonNull View itemView,rateAndReviewClickListener rateAndReviewClickListener) {
            super(itemView);
            image_view = itemView.findViewById(R.id.image_view);
            rater_email = itemView.findViewById(R.id.rater_email);
            rating_date = itemView.findViewById(R.id.rating_date);
            reviews = itemView.findViewById(R.id.reviews);
            rate = itemView.findViewById(R.id.rate);
            container = itemView.findViewById(R.id.container);
            this.rateAndReviewClickListener = rateAndReviewClickListener;
            container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            rateAndReviewClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public interface rateAndReviewClickListener{
        void onItemClick(View view,int position);
    }
}
