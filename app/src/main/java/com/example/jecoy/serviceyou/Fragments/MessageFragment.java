package com.example.jecoy.serviceyou.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jecoy.serviceyou.Adapters.ConversationAdapter;
import com.example.jecoy.serviceyou.Adapters.MessagesAdapter;
import com.example.jecoy.serviceyou.Api.ApiInterface;
import com.example.jecoy.serviceyou.Api.RetrofitClient;
import com.example.jecoy.serviceyou.Class.Conversations;
import com.example.jecoy.serviceyou.Class.LoginUser;
import com.example.jecoy.serviceyou.Class.Messages;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Storage.SharedPrefManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageFragment extends Fragment {
    View v;
    LoginUser user;
    int user_id,conversation_id;
    String user_role;
    RecyclerView recyclerView;
    ApiInterface apiInterface;
    MessagesAdapter messagesAdapter;
    MessagesAdapter.messageClickListener messageClickListener;
    List<Messages> messagesList;
    ConversationAdapter conversationAdapter;
    ConversationAdapter.conversationClickListener conversationClickListener;
    List<Conversations> conversationsList;
    Dialog C_M_P;
    CardView send;
    EditText message;
    RecyclerView recyclerViewMessages;
    TextView close;
    String convo_partner_email;
    LinearLayoutManager layoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        user = SharedPrefManager.getInstance(this.getActivity().getApplicationContext()).getUser();
        user_id = user.getUser_id();
        user_role = user.getUser_role();
        apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
        v = inflater.inflate(R.layout.fragment_message, container, false);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        fetchTransactions("fetchConversations",user_id);
        C_M_P = new Dialog(getActivity());
        conversationClickListener = new ConversationAdapter.conversationClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                conversation_id = conversationsList.get(position).getConversation_id();
                convo_partner_email = conversationsList.get(position).getConversation_partner();
                openMessagesPopUp();
            }
        };
    }

    private void openMessagesPopUp() {
        C_M_P.setContentView(R.layout.conversationmessagepopup);
        C_M_P.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        send = C_M_P.findViewById(R.id.cardViewSend);
        message = C_M_P.findViewById(R.id.messageHere);
        recyclerViewMessages = C_M_P.findViewById(R.id.recyclerViewMessages);
        close = C_M_P.findViewById(R.id.close);
        fetchMessages(recyclerViewMessages);
        C_M_P.show();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                C_M_P.dismiss();
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messages = message.getText().toString().trim();
                if(!messages.equals("")){
                    sendMessage(messages,convo_partner_email,recyclerViewMessages);
                }
            }
        });
    }

    private void sendMessage(String messages, String email,RecyclerView recyclerView) {
        Call<Messages> call = apiInterface.sendMessage("sendMessage",messages,email,user_role,user_id+"");
        call.enqueue(new Callback<Messages>() {
            @Override
            public void onResponse(Call<Messages> call, Response<Messages> response) {
                if(response.body()!=null){
                    String value = response.body().getValue();
                    if(value.equals("1")){
                        fetchMessages(recyclerView);
                        message.setText("");
                    }
                    else{
                        Toast.makeText(getActivity(),"ERROR:"+response.body().getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
                else{
                    return;
                }
            }

            @Override
            public void onFailure(Call<Messages> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void fetchMessages(RecyclerView recyclerView) {
        Call<List<Messages>> call = apiInterface.fetchMessages("fetchMessages",conversation_id+"",user_id,user_role);
        call.enqueue(new Callback<List<Messages>>() {
            @Override
            public void onResponse(Call<List<Messages>> call, Response<List<Messages>> response) {
                messagesList = response.body();
                recyclerView.setLayoutManager(layoutManager);
                messagesAdapter = new MessagesAdapter(messagesList,getActivity(), messageClickListener);
                recyclerView.setAdapter(messagesAdapter);
                messagesAdapter.notifyDataSetChanged();
                layoutManager.scrollToPosition(messagesList.size()-1);
            }

            @Override
            public void onFailure(Call<List<Messages>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fetchTransactions(String key, int user_id) {
        Call<List<Conversations>> call = apiInterface.fetchConversations(key,user_id+"",user_role);
        call.enqueue(new Callback<List<Conversations>>() {
            @Override
            public void onResponse(Call<List<Conversations>> call, Response<List<Conversations>> response) {
                conversationsList = response.body();
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView = v.findViewById(R.id.recyclerViewMessage);
                recyclerView.setLayoutManager(layoutManager);
                conversationAdapter = new ConversationAdapter(conversationsList,getActivity(), conversationClickListener);
                recyclerView.setAdapter(conversationAdapter);
                conversationAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Conversations>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "rp :"+
                                t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}
