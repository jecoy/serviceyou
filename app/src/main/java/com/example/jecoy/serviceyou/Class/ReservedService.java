package com.example.jecoy.serviceyou.Class;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import com.example.jecoy.serviceyou.Activities.HomeActivity;
import com.example.jecoy.serviceyou.Api.ApiInterface;
import com.example.jecoy.serviceyou.Api.RetrofitClient;
import com.example.jecoy.serviceyou.R;
import com.example.jecoy.serviceyou.Storage.SharedPrefManager;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReservedService extends Service {
    LoginUser user;
    public static final long NOTIFY_INTERVAL = 10 * 1000; // 10 seconds

    ApiInterface apiInterface = RetrofitClient.getRetrofit().create(ApiInterface.class);
    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();
    // timer handling
    private Timer mTimer = null;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        user = SharedPrefManager.getInstance(getApplicationContext()).getUser();

        // cancel if already existed
        if (mTimer != null) {
            mTimer.cancel();
        } else {
            // recreate new
            mTimer = new Timer();
        }
        // schedule task
        mTimer.scheduleAtFixedRate(new ReservedService.TimeDisplayTimerTask(), 0, NOTIFY_INTERVAL);
    }

    class TimeDisplayTimerTask extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    Call<List<TransactionInfo>> call = apiInterface.getAllRescheduledService("getReserved",user.getUser_id(),user.getUser_role());
                    call.enqueue(new Callback<List<TransactionInfo>>() {
                        @Override
                        public void onResponse(Call<List<TransactionInfo>> call, Response<List<TransactionInfo>> response) {
                            if(response.body()!=null) {
                                int notifyId = 0;
                                for (TransactionInfo a : response.body()) {

                                    Intent mainIntent = new Intent(getApplicationContext(), HomeActivity.class);
                                    int ti_id = a.getTi_id();
                                    mainIntent.putExtra("ti_id",ti_id+"");

                                    PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(),notifyId,mainIntent,PendingIntent.FLAG_UPDATE_CURRENT);

                                    Notification.BigTextStyle bigTextStyle = new Notification.BigTextStyle();
                                    bigTextStyle.bigText(a.getMessage() );

                                    bigTextStyle.setBigContentTitle("RESERVED SERVICE");

                                    Notification.Builder builder = new Notification.Builder(getApplicationContext());
                                    builder.setSmallIcon(R.drawable.serviceyouicon)
                                            .setContentTitle("RESERVED SERVICE")
                                            .setContentText(a.getTask_instruction())
                                            .setWhen(System.currentTimeMillis())
                                            .setAutoCancel(false)
                                            .setContentIntent(contentIntent)
                                            .setPriority(Notification.PRIORITY_MAX)
                                            .setStyle(bigTextStyle)
                                            .setDefaults(Notification.DEFAULT_ALL);

                                    NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                    mNotificationManager.notify(notifyId++, builder.build());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<List<TransactionInfo>> call, Throwable t) {

                        }
                    });
                }

            });
        }
    }
}
